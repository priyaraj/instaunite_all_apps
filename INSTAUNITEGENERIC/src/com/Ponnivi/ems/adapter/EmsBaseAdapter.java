package com.Ponnivi.ems.adapter;

import java.util.ArrayList;

import org.json.JSONArray;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.Ponnivi.ems.R;
import com.Ponnivi.ems.activity.EmsSiteVistRoutineItemsActivity;
import com.Ponnivi.ems.helper.CustomDialogClass;
import com.Ponnivi.ems.helper.Utilities;

//public class EmsBaseAdapter extends BaseAdapter implements OnClickListener {
public class EmsBaseAdapter extends ArrayAdapter<ArrayList<String>> implements OnClickListener {

	public static int positionCount = 0;
	View v;
	private int selectedPos = -1;
	static class ViewHolder {
		RelativeLayout relativelayout;
		// TextView itemTitleView;
		TextView itemPriceView;
		// ImageView itemImageView;
		ImageView routineButton1;
		ImageView routineButton2;
	}

	private final Activity activity;

	private final ArrayList<String> itemTitle;
	private final ArrayList<String> itemPrice;

	private static LayoutInflater inflater = null;

	/** constructor */
	public EmsBaseAdapter(Activity activity, ArrayList<String> itemTitle,
			ArrayList<String> itemPrice) {
		super(activity, R.layout.list_row_layout);
		this.activity = activity;
		this.itemTitle = itemTitle;
		this.itemPrice = itemPrice;

		inflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

	}
	
    public void setSelectedPosition(int pos){
        selectedPos = pos;
        notifyDataSetChanged();
    }

	public int getCount() {
		return itemTitle.size();
	}

/*	public ArrayList<String> getItem(int position) {
		return position;
	}*/

	public long getItemId(int id) {
		return id;
	}

	public View getView(final int position, View convertView,
			final ViewGroup parent) {
		ViewHolder viewHolder = null;
		v = convertView;

		if (convertView == null) {
			//v = inflater.inflate(R.layout.list_row_layout, parent, false);
			LayoutInflater inflator = activity.getLayoutInflater();
			v = inflator.inflate(R.layout.list_row_layout, null);

			viewHolder = new ViewHolder();

			assert v != null;
			viewHolder.relativelayout = (RelativeLayout) v
					.findViewById(R.id.relativelayout);
			// viewHolder.itemTitleView = (TextView)
			// v.findViewById(R.id.item_title_view);
			viewHolder.itemPriceView = (TextView) v
					.findViewById(R.id.item_price_view);
			viewHolder.routineButton1 = (ImageView) v
					.findViewById(R.id.routineButton1);
			viewHolder.routineButton1.setBackgroundColor(Color.TRANSPARENT);
			viewHolder.routineButton2 = (ImageView) v
					.findViewById(R.id.routineButton2);
			viewHolder.itemPriceView.setTextColor(v.getContext().getResources()
					.getColor(R.color.black_color));
			viewHolder.routineButton1.setOnClickListener(this);
			viewHolder.routineButton2.setOnClickListener(this);
			viewHolder.itemPriceView.setTextColor(v.getContext().getResources().getColor(R.color.black_color));


			v.setTag(viewHolder);
			v.setTag(R.id.item_price_view, viewHolder.itemPriceView);
			v.setTag(R.id.routineButton1, viewHolder.routineButton1);
			v.setTag(R.id.routineButton2, viewHolder.routineButton2);

		} else {
			viewHolder = (ViewHolder) v.getTag();
		}
		if (position == positionCount) {
			viewHolder.routineButton1.setVisibility(View.VISIBLE);
			viewHolder.routineButton2.setVisibility(View.VISIBLE);
			viewHolder.itemPriceView.setTypeface(null,Typeface.BOLD);
		} 
		else {
			viewHolder.routineButton1.setVisibility(View.INVISIBLE);
			viewHolder.routineButton2.setVisibility(View.INVISIBLE);
			viewHolder.itemPriceView.setTypeface(null,Typeface.NORMAL);
		}
	     if (selectedPos == position) {
	            v.setBackgroundResource(R.drawable.blueband);
	        } else {
	            v.setBackgroundDrawable(null);
	        }
		if (position % 2 == 0) {
			if (position < positionCount) {
			viewHolder.itemPriceView.setTextColor(v.getContext()
					.getResources().getColor(R.color.white_color));
			viewHolder.relativelayout.setBackgroundColor(v.getContext()
					.getResources().getColor(R.color.grey_color));
			}
			else {
				viewHolder.itemPriceView.setTextColor(v.getContext()
						.getResources().getColor(R.color.dark_grey_color));
				viewHolder.relativelayout.setBackgroundColor(v.getContext()
						.getResources().getColor(R.color.grey_color));
			}
			
		} else {
			if (position < positionCount) {
				viewHolder.itemPriceView.setTextColor(v.getContext()
						.getResources().getColor(R.color.white_color));
				viewHolder.relativelayout.setBackgroundColor(v.getContext()
						.getResources().getColor(R.color.grey_color2));
			}
			else {
				viewHolder.itemPriceView.setTextColor(v.getContext()
						.getResources().getColor(R.color.dark_grey_color));
				viewHolder.relativelayout.setBackgroundColor(v.getContext()
						.getResources().getColor(R.color.grey_color2));
			}			

		}

		viewHolder.routineButton1.setTag(position); // This line is important.
		viewHolder.routineButton2.setTag(position); // This line is important.
		viewHolder.itemPriceView.setText(itemPrice.get(position));
		 //viewHolder.itemPriceView.setText( list.get(position).getName());

		Log.d("getView", "called");

		return v;
	}

	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.routineButton2:
			CustomDialogClass cdd = new CustomDialogClass(activity);
			cdd.show();
			v.setSelected(true);
		//	v.setBackgroundColor(v.getContext().getResources().getColor(R.color.cred_color));
			// Utilities.createAndShowAlertDialogForSiteVisitRoutine(v.getContext());

			//positionCount += 1;
			break;
		case R.id.routineButton1:
			v.setSelected(true);
		//	v.setBackgroundColor(v.getContext().getResources().getColor(R.color.cred_color));
			JSONArray itemsArray = Utilities.getItemsArray(v.getContext());
			Utilities.saveSiteVisitDetails(v.getContext(), itemsArray);
			positionCount += 1;
			Intent intent = new Intent(v.getContext(),EmsSiteVistRoutineItemsActivity.class);
			v.getContext().startActivity(intent);
			break;
		}

	}
	

	


	/**
	 * @Override public boolean isEnabled(int position) { return true; }
	 * @Override public boolean areAllItemsEnabled() { return true; }
	 */
}
