package com.Ponnivi.ems.adapter;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.Ponnivi.ems.R;
import com.Ponnivi.ems.activity.EmsCatalogueToolBoxActivity;
import com.Ponnivi.ems.activity.EmsGroupsActivity;
import com.Ponnivi.ems.activity.EmsGroupsSplitActivity;
import com.Ponnivi.ems.activity.EmsMenuActivity;
import com.Ponnivi.ems.activity.EmsServiceNotes;
import com.Ponnivi.ems.helper.Utilities;
import com.Ponnivi.ems.widget.CenterLockHorizontalScrollview;

public class EmsQuestionaireListAdapter extends ArrayAdapter<JSONObject>
		implements OnClickListener {
	Context context;
	int layoutId;

	private ArrayList<JSONObject> objects;

	private int currentNewsPosition = 0;
	String newsValue = "";

	TextView QuestionName;
	// ImageView productimage;
	LinearLayout answerLayout, questionLayout;
	ScrollView scrollView;
	Button next, prev, repair;
	JSONArray questionBankDetails;
	int pageNewsCount;
	CenterLockHorizontalScrollview centerLockHorizontalScrollview;
	Fragment groupheaderfragment;
	int imageExist = 0;
	private String categoryChosen, wholeJson;
	JSONArray questionaireDetailsJson = new JSONArray();
	String QuestionerType;
	String Answer1, Answer2, Answer3, Answer4;

	/*
	 * here we must override the constructor for ArrayAdapter the only variable
	 * we care about now is ArrayList<Item> objects, because it is the list of
	 * objects we want to display.
	 */
	public EmsQuestionaireListAdapter(Context context, int textViewResourceId,
			ArrayList<JSONObject> objects,
			CenterLockHorizontalScrollview centerLockHorizontalScrollview,
			Fragment groupheaderfragment) {
		super(context, textViewResourceId, objects);
		this.context = context;
		this.objects = objects;
		this.centerLockHorizontalScrollview = centerLockHorizontalScrollview;
		this.groupheaderfragment = groupheaderfragment;
		categoryChosen = Utilities.loadPreferences(context,
				Utilities.TAG_CHOSEN_CATEGORIES);

		/*
		 * try { JSONObject obj = new
		 * JSONObject(Utilities.loadFromAsset(context,
		 * "cataloguedetails.json")); catalogueDetails = new
		 * JSONArray(Utilities.TAG_CATALOGUE); } catch (JSONException e) { //
		 * TODO Auto-generated catch block e.printStackTrace(); }
		 */

	}

	@Override
	public int getCount() {
		return objects.size();
	}

	@Override
	public JSONObject getItem(int position) {
		return objects.get(position);
	}

	/*
	 * we are overriding the getView method here - this is what defines how each
	 * list item will look.
	 */
	@SuppressLint("NewApi")
	public View getView(final int position, View convertView, ViewGroup parent) {

		// assign the view we are converting to a local variable

		View v = convertView;
		JSONObject jsonObject = objects.get(position);

		try {
			if (jsonObject != null) {

				if (v == null) {
					LayoutInflater inflater = (LayoutInflater) getContext()
							.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
					v = inflater.inflate(R.layout.ems_questionaire_items, null);

					questionLayout = (LinearLayout) v
							.findViewById(R.id.questionLayout);
					answerLayout = (LinearLayout) v
							.findViewById(R.id.answerLayout);
					scrollView = (ScrollView) v.findViewById(R.id.scrollview);
					next = (Button) v.findViewById(R.id.next);
					next.setOnClickListener(this);

					prev = (Button) v.findViewById(R.id.prev);
					repair = (Button) v.findViewById(R.id.repair);
					prev.setOnClickListener(this);
					repair.setOnClickListener(this);
					currentNewsPosition = position;

					// QuestionName.setText(jsonObject
					// .getString(Utilities.TAG_QuestionName));
					QuestionerType = jsonObject
							.getString(Utilities.TAG_QuestionerType);
					if (QuestionerType.equalsIgnoreCase("Textboxnumber")
							|| QuestionerType.equalsIgnoreCase("Textboxstring")) {
						Utilities.addEditTextDynamically(context,
								questionLayout, answerLayout, QuestionerType,
								jsonObject);
					}
					if (QuestionerType.equalsIgnoreCase("Radio")) {
						Utilities.addRadiosDynamically(context, questionLayout,
								scrollView, jsonObject);

					} // radio
					if (QuestionerType.equalsIgnoreCase("Checkbox")) {
						Utilities.addCheckboxDynamically(context, questionLayout,
								answerLayout, jsonObject);

					} 					
					

					((TextView) groupheaderfragment.getView().findViewById(
							R.id.cataloguerecordno)).setText(String
							.valueOf(position + 1) + "/" + getCount());
					int jsonIndex = 0;
					Utilities.savePreferences(
							Utilities.TAG_CATALOGUEJSONRECORD,
							jsonObject.toString(), context);
					Utilities.savePreferences(Utilities.TAG_CATALOGUERECORDNO,
							String.valueOf(position), context);

				}
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// the view must be returned to our activity
		return v;
	}

	public int getCurrentPosition() {
		return currentNewsPosition;
	}

	public void onClick(View v) {
		// TODO Auto-generated method stub
		Intent nextFlow = new Intent();
		boolean nextFlowPresent = true;
		switch (v.getId()) {
		case R.id.prev:
			nextFlowPresent = setPrevFlow();
			break;

		case R.id.repair:
			nextFlow = new Intent(context, EmsServiceNotes.class);
			break;
		case R.id.next:
			nextFlowPresent = setNextFlow();
			break;
		/*
		 * case R.id.productimage: if (imageExist == 0) {
		 * Utilities.SavePreferences(Utilities.TAG_CATALOGUETOCAMERA,
		 * Utilities.TAG_CATALOGUETOCAMERA, context);
		 * 
		 * nextFlow = new Intent(context, EmsUploadImage.class); //
		 * nextFlow.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); } else {
		 * nextFlowPresent = setNextFlow(); } break;
		 */
		} // switch
		if (nextFlowPresent) {
			context.startActivity(nextFlow);
		}

	}

	private boolean setNextFlow() {
		if (getCurrentPosition() < getCount() - 1) {
			
			pageNewsCount = getCurrentPosition();
			pageNewsCount = (pageNewsCount + 1) % getCount();
			Utilities.prepareJson(context,"");
			centerLockHorizontalScrollview.setQuestionAdapter(this,
					pageNewsCount, "next");
		} else if (Integer.parseInt(categoryChosen) < 5) {
			Utilities.prepareQuestionaireJson(context);
			Intent nextFlow = new Intent(context,
					EmsGroupsActivity.class);
			context.startActivity(nextFlow);
		} else {
			Utilities.writeDataJsonToCloud(context, "questionaireDetails.json");
			Utilities.alertDialogDisplay(context, "status", "File is sent");
			Intent nextFlow = new Intent(context, EmsMenuActivity.class);
			context.startActivity(nextFlow);
		}
		return false;
	}

	private boolean setPrevFlow() {
		/*
		 * if (getCurrentPosition() > getCount() - 1) { pageNewsCount =
		 * getCurrentPosition(); pageNewsCount = (pageNewsCount - 1) %
		 * getCount(); Utilities.prepareJson(context);
		 * centerLockHorizontalScrollview.setQuestionAdapter(this,
		 * pageNewsCount, "next"); } else if (Integer.parseInt(categoryChosen) <
		 * 5 ) { Utilities.prepareJson(context); Intent nextFlow = new
		 * Intent(context, EmsCatalogueToolBoxActivity.class);
		 * context.startActivity(nextFlow); } else {
		 * Utilities.writeToCloud(context);
		 * Utilities.alertDialogDisplay(context, "status", "File is sent");
		 * Intent nextFlow = new Intent(context, EmsMenuActivity.class);
		 * context.startActivity(nextFlow); }
		 */
		pageNewsCount = getCurrentPosition();
		if (pageNewsCount > 0) {
			pageNewsCount = (pageNewsCount - 1) % getCount();
			Utilities.prepareJson(context,"");
			centerLockHorizontalScrollview.setQuestionAdapter(this,
					pageNewsCount, "prev");
		} else {
			prev.setVisibility(View.INVISIBLE);
		}
		return false;
	}

}