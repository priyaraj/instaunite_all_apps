package com.Ponnivi.ems.adapter;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.Ponnivi.ems.R;
import com.Ponnivi.ems.activity.EmsSummaryActivity;
import com.Ponnivi.ems.activity.EmsGroupsActivity;
import com.Ponnivi.ems.activity.EmsMissingServiceNotes;
import com.Ponnivi.ems.activity.EmsServiceNotes;
import com.Ponnivi.ems.activity.EmsUploadImage;
import com.Ponnivi.ems.helper.MJSONObject;
import com.Ponnivi.ems.helper.Utilities;
import com.Ponnivi.ems.widget.CenterLockHorizontalScrollview;

public class EmsCatalogueListAdapter extends ArrayAdapter<JSONObject> implements
		OnClickListener {
	Context context;
	int layoutId;

	private ArrayList<JSONObject> objects;

	private int currentNewsPosition = 0;
	String newsValue = "";

	TextView articleno, articlename, recordno, price, incharge;
	ImageView productimage;
	Button ok, notok, repair;
	JSONArray catalogueDetails;
	int pageNewsCount;
	CenterLockHorizontalScrollview centerLockHorizontalScrollview;
	Fragment catalogueheaderfragment;
	int imageExist = 0;
	private String categoryChosen,wholeJson;
	JSONArray catalogueDetailsJson = new JSONArray();
	static int okCount = 0;
	static int missingCount = 0;
	static int serviceCount = 0;
	static int noImageCount = 0;
	
	
	/*
	 * here we must override the constructor for ArrayAdapter the only variable
	 * we care about now is ArrayList<Item> objects, because it is the list of
	 * objects we want to display.
	 */
	public EmsCatalogueListAdapter(Context context, int textViewResourceId,
			ArrayList<JSONObject> objects,
			CenterLockHorizontalScrollview centerLockHorizontalScrollview,
			Fragment catalogueheaderfragment) {
		super(context, textViewResourceId, objects);
		this.context = context;
		this.objects = objects;
		this.centerLockHorizontalScrollview = centerLockHorizontalScrollview;
		this.catalogueheaderfragment = catalogueheaderfragment;
		categoryChosen = Utilities.loadPreferences(context
				, Utilities.TAG_CHOSEN_CATEGORIES);


	}

	@Override
	public int getCount() {
		return objects.size();
	}

	@Override
	public JSONObject getItem(int position) {
		return objects.get(position);
	}

	/*
	 * we are overriding the getView method here - this is what defines how each
	 * list item will look.
	 */
	@SuppressLint("NewApi")
	public View getView(final int position, View convertView, ViewGroup parent) {

		// assign the view we are converting to a local variable

		View v = convertView;
		JSONObject jsonObject = objects.get(position);

		try {
			if (jsonObject != null) {

				if (v == null) {
					LayoutInflater inflater = (LayoutInflater) getContext()
							.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
					v = inflater.inflate(R.layout.ems_catalogue_items, null);

					articleno = (TextView) v.findViewById(R.id.articleno);
					articlename = (TextView) v.findViewById(R.id.articlename);
				//	price = (TextView) v.findViewById(R.id.price);
					incharge = (TextView) v.findViewById(R.id.incharge);
					// recordno = (TextView) v.findViewById(R.id.recordno);
					ok = (Button) v.findViewById(R.id.ok);
					ok.setOnClickListener(this);

					notok = (Button) v.findViewById(R.id.notok);
					repair = (Button) v.findViewById(R.id.repair);
					notok.setOnClickListener(this);
					repair.setOnClickListener(this);
					currentNewsPosition = position;
					productimage = (ImageView) v
							.findViewById(R.id.productimage);
					articleno.setText("Article Nr. :" + jsonObject
							.getString(Utilities.TAG_ARTICLENO));
					articlename.setText("Article :" + 
							jsonObject.getString(Utilities.TAG_ARTICLENAME)
							);
				//	price.setText("Price in Euro : " + jsonObject.getString(Utilities.TAG_PRICE));
					incharge.setText("Owner : " + jsonObject
							.getString(Utilities.TAG_INCHARGE));
					imageExist = Utilities.getDynamicImage(context, "pic_"
							+ jsonObject.getString(Utilities.TAG_ARTICLENO),
							productimage, null);
					if (imageExist == 0) { // image is not there and hence we
											// need to do click event to go to
											// camera

						productimage.setOnClickListener(this);
					}
					/*
					 * ok.setText(jsonObject .getString(Utilities.TAG_STATUS1));
					 * notok.setText(jsonObject
					 * .getString(Utilities.TAG_STATUS2));
					 * repair.setText(jsonObject
					 * .getString(Utilities.TAG_STATUS3));
					 */
					
					((TextView) catalogueheaderfragment.getView().findViewById(
							R.id.cataloguerecordno)).setText(String
							.valueOf(position + 1) + "/" + getCount());
					/*Utilities.savePreferences(Utilities.TAG_ARTICLENO,
							jsonObject.getString(Utilities.TAG_ARTICLENO),
							context);
					Utilities.savePreferences(Utilities.TAG_ARTICLENAME,
							jsonObject.getString(Utilities.TAG_ARTICLENAME),
							context);
					Utilities.savePreferences(Utilities.TAG_PRICE,
							jsonObject.getString(Utilities.TAG_PRICE),
							context);
					Utilities.savePreferences(Utilities.TAG_INCHARGE,
							jsonObject.getString(Utilities.TAG_INCHARGE),
							context);*/
				//	Utilities.saveCatalogueDetails(jsonObject,context, null);
					int jsonIndex = 0;					
					Utilities.savePreferences(Utilities.TAG_CATALOGUEJSONRECORD, jsonObject.toString(), context);
					Bundle userObject = new Bundle();
					MJSONObject  mObject = new MJSONObject(jsonObject);
/*					userObject.putSerializable("Object", mObject);
					userObject.putString("method_name", "ObjectIntent");*/
				//	Utilities.SavePreferences(Utilities.TAG_CATALOGUEJSONRECORD, mObject.toString(), context);
					
					Utilities.savePreferences(Utilities.TAG_CATALOGUERECORDNO,
							String.valueOf(position), context);

				}
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// the view must be returned to our activity
		return v;
	}

	public int getCurrentPosition() {
		return currentNewsPosition;
	}

	public void onClick(View v) {
		// TODO Auto-generated method stub
		Intent nextFlow = new Intent();
		boolean nextFlowPresent = true;
		switch (v.getId()) {
		case R.id.notok:
			missingCount += 1;
			nextFlow = new Intent(context, EmsMissingServiceNotes.class);
			break;

		case R.id.repair:
			serviceCount += 1;
			nextFlow = new Intent(context, EmsServiceNotes.class);
			break;
		case R.id.ok:
			okCount += 1;
			nextFlowPresent = setNextFlow();
			break;
		case R.id.productimage:
			if (imageExist == 0) {
				Utilities.savePreferences(Utilities.TAG_CATALOGUETOCAMERA,
						Utilities.TAG_CATALOGUETOCAMERA, context);
				noImageCount += 1;
				nextFlow = new Intent(context, EmsUploadImage.class);
			//	nextFlow.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			}
			else {				 
				nextFlowPresent = setNextFlow();			
			}
			break;
		} //switch
		Utilities.savePreferences("ok", String.valueOf(okCount), context);
		Utilities.savePreferences("missing", String.valueOf(missingCount), context);
		Utilities.savePreferences("service", String.valueOf(serviceCount), context);
		Utilities.savePreferences("noimage", String.valueOf(noImageCount), context);
		if (nextFlowPresent) {
			context.startActivity(nextFlow);
		}

	}
	
	private boolean setNextFlow() {
		if (getCurrentPosition() < getCount() - 1) {
			pageNewsCount = getCurrentPosition();
			pageNewsCount = (pageNewsCount + 1) % getCount();
			Utilities.prepareJson(context,"");
			centerLockHorizontalScrollview.setCatalogueAdapter(this, pageNewsCount,
					"next");
		} else if (Integer.parseInt(categoryChosen) < 4 ) {
			Utilities.prepareJson(context,"");
			/*Intent nextFlow = new Intent(context,
					EmsCatalogueToolBoxActivity.class);*/
			Intent nextFlow = new Intent(context,
					EmsGroupsActivity.class);
			context.startActivity(nextFlow);
		}
		else {
//			Utilities.writeToCloud(context, "cataloguedetails.json");
			Utilities.alertDialogDisplay(context, "status",
					  "File is sent");
			Intent nextFlow = new Intent(context,
					EmsSummaryActivity.class);
			context.startActivity(nextFlow);
		}
		return false;
	}

}