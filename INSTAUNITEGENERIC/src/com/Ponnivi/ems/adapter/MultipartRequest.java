package com.Ponnivi.ems.adapter;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.poi.ss.formula.functions.T;

import android.content.Context;
import android.util.Log;

import com.Ponnivi.ems.helper.Utilities;
import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
 
public class MultipartRequest extends Request {
    private static final String KEY_PICTURE = "picture";
    public static final String KEY_PICTURE_NAME = "filename";
    private static final String TAG = "MutlipartRequest";
    public static final String KEY_ROUTE_ID = "route_id";
 
    private HttpEntity mHttpEntity;
    private final Class mClass;
    private Response.Listener mListener;
    private Map<String, String> mHeaders;
    private final Gson gson;
    private Context mContext;
 
    public MultipartRequest(Context context,String url, File file,
                            Class clazz,
                            Map<String, String> headers,
                            Response.Listener listener,
                            Response.ErrorListener errorListener) {
        super(Request.Method.POST, url, errorListener);
        mContext = context;
        mHeaders = headers;
        mClass = clazz;
        mListener = listener;
        gson = new Gson();
        mHttpEntity = buildMultipartEntity(file);
    }
 
    public MultipartRequest(String url, String path,
                            Class clazz,
                            Map<String, String> headers,
                            Response.Listener listener,
                            Response.ErrorListener errorListener) {
        super(Request.Method.POST, url, errorListener);
        mHeaders = headers;
        mClass = clazz;
        mListener = listener;
        gson = new Gson();
        mHttpEntity = buildMultipartEntity(path);
    }
 
    private HttpEntity buildMultipartEntity(String path) {
        File file = new File(path);
        return buildMultipartEntity(file);
    }
 
    private HttpEntity buildMultipartEntity(File file) {
        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        String fileName = file.getName();
        
        FileBody fileBody = new FileBody(file);
        if (Utilities.loadPreferences(mContext, "imageUpload").equalsIgnoreCase("yes")) {
        	builder.addBinaryBody(KEY_PICTURE, file, ContentType.create("image/jpeg"), fileName);//for picture
        }
/*        else if (Utilities.loadPreferences(mContext, "zipfileUpload").equalsIgnoreCase("yes")) {
        	builder.addBinaryBody(KEY_PICTURE, file, ContentType.create("application/zip"), fileName);//for zip
        } */
        else {
        	builder.addBinaryBody(KEY_PICTURE, file, ContentType.TEXT_PLAIN, fileName); //for json, but it is coming with content-type
        }
        return builder.build();
    }
    
 
    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        return mHeaders != null ? mHeaders : super.getHeaders();
    }
 
    @Override
    public String getBodyContentType() {
        return mHttpEntity.getContentType().getValue();
    }
 
    @Override
    public byte[] getBody() throws AuthFailureError {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            mHttpEntity.writeTo(bos);
        } catch (IOException e) {
            VolleyLog.e("IOException writing to ByteArrayOutputStream");
        }
        return bos.toByteArray();
    }
 
    @Override
    protected Response parseNetworkResponse(NetworkResponse response) {
        String json;
        try {
            json = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
        } catch (UnsupportedEncodingException e) {
            Log.e(TAG, String.format("Encoding problem parsing API response. NetworkResponse:%s", response.toString()), e);
            return Response.error(new ParseError(e));
        }
        try {
            return Response.success(gson.fromJson(json, mClass), HttpHeaderParser.parseCacheHeaders(response));
        } catch (JsonSyntaxException e) {
            Log.e(TAG, String.format("Couldn't API parse JSON response. NetworkResponse:%s", response.toString()), e);
            Log.e(TAG, String.format("Couldn't API parse JSON response. Json dump: %s", json));
            return Response.error(new ParseError(e));
        }
    }
 
    protected void deliverResponse(T response) {
        mListener.onResponse(response);
    }

	public int compareTo(Object arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected void deliverResponse(Object arg0) {
		// TODO Auto-generated method stub
		
	}
}
