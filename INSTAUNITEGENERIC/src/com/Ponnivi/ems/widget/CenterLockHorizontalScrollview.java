package com.Ponnivi.ems.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.ScrollView;

import com.Ponnivi.ems.adapter.EmsCatalogueListAdapter;
import com.Ponnivi.ems.adapter.EmsCatalogueMultiImageListAdapter;
import com.Ponnivi.ems.adapter.EmsQuestionaireListAdapter;
import com.Ponnivi.ems.helper.ZeroChildException;

@SuppressLint("NewApi")
public class CenterLockHorizontalScrollview extends HorizontalScrollView {
	Context context;
	int prevIndex = 0;
	int childCount = 0;
	//private int scrollMax;
	//private int scrollPos =	0;
	private boolean inSwipeMode;
	private int currentPageIndex;
	private long lastSwipeTime;
	
	public CenterLockHorizontalScrollview(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;
		this.setSmoothScrollingEnabled(true);

	}

	public void setAdapter(Context context, EmsCatalogueListAdapter mAdapter) {

		try {
			fillViewWithAdapter(mAdapter);
		} catch (ZeroChildException e) {

			e.printStackTrace();
		}
	}
	
	public synchronized void setCatalogueAdapter(EmsCatalogueListAdapter mAdapter,int seekerDetailIndex,String swipeDirection) {

		try {
			fillViewWithCatalogueAdapter(mAdapter,seekerDetailIndex,swipeDirection);
		} catch (ZeroChildException e) {

			e.printStackTrace();
		}
	}
	public synchronized void setCatalogueMultiImageAdapter(EmsCatalogueMultiImageListAdapter mAdapter,int seekerDetailIndex,String swipeDirection) {

		try {
			fillViewWithCatalogueMultiItemAdapter(mAdapter,seekerDetailIndex,swipeDirection);
		} catch (ZeroChildException e) {

			e.printStackTrace();
		}
	}
	
	public synchronized void setQuestionAdapter(EmsQuestionaireListAdapter mAdapter,int seekerDetailIndex,String swipeDirection) {

		try {
			fillViewWithQuestionaireAdapter(mAdapter,seekerDetailIndex,swipeDirection);
		} catch (ZeroChildException e) {

			e.printStackTrace();
		}
	}
	


	private void fillViewWithAdapter(EmsCatalogueListAdapter mAdapter)
			throws ZeroChildException {
		if (getChildCount() == 0) {
			throw new ZeroChildException(
					"CenterLockHorizontalScrollView must have one child");
		}
		if (getChildCount() == 0 || mAdapter == null)
			return;

		ViewGroup parent = (ViewGroup) getChildAt(0);

		parent.removeAllViews();
		childCount = mAdapter.getCount();
		for (int i = 0; i < mAdapter.getCount(); i++) {
			parent.addView(mAdapter.getView(i, null, parent));
		}
	}
	
	private void fillViewWithCatalogueAdapter(EmsCatalogueListAdapter mAdapter,int seekerDetailIndex,String swipeDirection)
			throws ZeroChildException {
		Log.v("Activity1",seekerDetailIndex+":"+swipeDirection+":"+System.currentTimeMillis());
		inSwipeMode = !swipeDirection.equals("");
		if (getChildCount() == 0) {
			throw new ZeroChildException(
					"CenterLockHorizontalScrollView must have one child");
		}
		if (getChildCount() == 0 || mAdapter == null)
			return;

		ViewGroup parent = (ViewGroup) getChildAt(0);

		parent.removeAllViews();
		childCount = mAdapter.getCount();
		View view = mAdapter.getView(seekerDetailIndex, null, parent);
		parent.addView(view);
		setCurrentPageIndex(seekerDetailIndex);
		if(isInSwipeMode())
		{
			lastSwipeTime = System.currentTimeMillis();
		}
	}
	
	private void fillViewWithCatalogueMultiItemAdapter(EmsCatalogueMultiImageListAdapter mAdapter,int seekerDetailIndex,String swipeDirection)
			throws ZeroChildException {
		Log.v("Activity1",seekerDetailIndex+":"+swipeDirection+":"+System.currentTimeMillis());
		inSwipeMode = !swipeDirection.equals("");
		if (getChildCount() == 0) {
			throw new ZeroChildException(
					"CenterLockHorizontalScrollView must have one child");
		}
		if (getChildCount() == 0 || mAdapter == null)
			return;

		ViewGroup parent = (ViewGroup) getChildAt(0);

		parent.removeAllViews();
		childCount = mAdapter.getCount();
		View view = mAdapter.getView(seekerDetailIndex, null, parent);
		parent.addView(view);
		setCurrentPageIndex(seekerDetailIndex);
		if(isInSwipeMode())
		{
			lastSwipeTime = System.currentTimeMillis();
		}
	}
	
	private void fillViewWithQuestionaireAdapter(EmsQuestionaireListAdapter mAdapter,int seekerDetailIndex,String swipeDirection)
			throws ZeroChildException {
		Log.v("Activity1",seekerDetailIndex+":"+swipeDirection+":"+System.currentTimeMillis());
		inSwipeMode = !swipeDirection.equals("");
		if (getChildCount() == 0) {
			throw new ZeroChildException(
					"CenterLockHorizontalScrollView must have one child");
		}
		if (getChildCount() == 0 || mAdapter == null)
			return;

		ViewGroup parent = (ViewGroup) getChildAt(0);

		parent.removeAllViews();
		childCount = mAdapter.getCount();
		View view = mAdapter.getView(seekerDetailIndex, null, parent);
		parent.addView(view);
		setCurrentPageIndex(seekerDetailIndex);
		if(isInSwipeMode())
		{
			lastSwipeTime = System.currentTimeMillis();
		}
	}

	public boolean isInSwipeMode() {
		return inSwipeMode;
	}

	public void setSwipeMode(boolean b) {
		this.inSwipeMode = b;
	}

	public int getCurrentPageIndex()
	{
		return currentPageIndex;
	}

	public synchronized void setCurrentPageIndex(int currentPageIndex) {
		this.currentPageIndex = currentPageIndex;
	}
	
	public long getLastSwipeTime()
	{
		return lastSwipeTime;
	}
}