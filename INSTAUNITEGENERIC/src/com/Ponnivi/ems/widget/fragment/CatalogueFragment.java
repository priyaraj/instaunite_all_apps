package com.Ponnivi.ems.widget.fragment;

import android.annotation.SuppressLint;
import android.app.ListFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.Ponnivi.ems.R;

@SuppressLint("NewApi")
public class CatalogueFragment extends  ListFragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		return inflater.inflate(R.layout.list_fragment, null, false);
	}


	@Override
	public void onActivityCreated(Bundle savedInstanceState) {   

		super.onActivityCreated(savedInstanceState);


	}


}