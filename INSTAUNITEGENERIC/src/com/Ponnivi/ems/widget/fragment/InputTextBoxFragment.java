package com.Ponnivi.ems.widget.fragment;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.Ponnivi.ems.R;
import com.Ponnivi.ems.activity.EmsCatalogueInsertItemActivity;
import com.Ponnivi.ems.activity.EmsGroupsSplitActivity;
import com.Ponnivi.ems.helper.Utilities;

@SuppressLint("NewApi")
public class InputTextBoxFragment extends Fragment implements OnClickListener {

	EditText editText1, editText2;
	TextView textView2,textView1;
	Button imgBtn;
	
	String userName, password, menuSelection,subMenuOption;

	int jsonGroupCount;
	ArrayList jsonFileNames = new ArrayList();



	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.textboxscreen, null);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		editText1 = (EditText) view.findViewById(R.id.editText1);
		editText2 = (EditText) view.findViewById(R.id.editText2);
		textView1 = (TextView) view.findViewById(R.id.textView1);
		textView2 = (TextView) view.findViewById(R.id.textView2);
		imgBtn = (Button) view.findViewById(R.id.imgBtn);
		
		
		userName = Utilities.loadPreferences(getActivity(), getResources()
				.getString(R.string.email));
		password = Utilities.loadPreferences(getActivity(),
				Utilities.TAG_PASSWORD);
		menuSelection = Utilities.loadPreferences(getActivity(),
				Utilities.TAG_MENUSELECTION);

		
/*		switch (getId()) {
		case R.id.textboxgroups:
			if ((userName.equalsIgnoreCase("AC1@g.com") && password
					.equalsIgnoreCase("ddd222"))
					|| menuSelection.equalsIgnoreCase("4")) {
				jsonGroupCount = 5;
				jsonFileNames.add("inventory.json");
			}

	
		}*/
		subMenuOption = Utilities.loadPreferences(getActivity(), "submenu");
		if (subMenuOption.equalsIgnoreCase("create toolbox")) {
			textView1.setText("Toolbox");
		}
		else {
			textView1.setText("Equipment");
		}
		textView2.setText("Recorder");

		view.findViewById(R.id.imgBtn).setOnClickListener(this);

	}

	public void onClick(View view) {		
		if ((userName.equalsIgnoreCase("AC1@g.com") && password
				.equalsIgnoreCase("ddd222"))
				|| menuSelection.equalsIgnoreCase("4")) {

			validateDetails();

		}
	}
	
	private void validateDetails() {
		Intent nextScreen = new Intent();
		String entry1 = editText1.getText().toString();
		String entry2 = editText2.getText().toString();
		boolean isValueExists = false;
		String fieldName = "",errorMessage = "";
		if (subMenuOption.equalsIgnoreCase("create toolbox")) {
			fieldName = "ToolBox Name";
			errorMessage = "Missing ToolBox Name";
		}
		else {
			fieldName = "Equipment Name";
			errorMessage = "Missing Equipment Name";
		}
		isValueExists = Utilities.mandatoryChecks(fieldName, entry1,
				getActivity(), errorMessage, errorMessage);
		if (isValueExists) {
			isValueExists = Utilities.mandatoryChecks("Recorder Name", entry2,
					getActivity(), "Missing Recorder Name", "Missing Recorder Name");
			if (isValueExists) {		
				 String volleyResponse = Utilities.loadPreferences(getActivity(),"volleyresponse");
				 if (!volleyResponse.equalsIgnoreCase("")) {
		            try {
						JSONArray volleyJsonArray = new JSONArray(volleyResponse);
						for (int i = 0; i < volleyJsonArray.length(); i++) {					
							if (volleyJsonArray.getString(i).equalsIgnoreCase(entry1)) {
								Utilities.alertDialogDisplay(getActivity(), fieldName + " already exists", fieldName + " already exists. Input some other values.");
								isValueExists = false;
								break;
							}
						}
		            } catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			}
		            if (isValueExists) {
				Utilities.savePreferences(Utilities.TAG_EVIDENCE_IDENTIFIER, entry1, getActivity());
				Utilities.savePreferences(Utilities.TAG_SE_ID, entry2, getActivity());
				
				nextScreen = new Intent(getActivity(), EmsGroupsSplitActivity.class);
				startActivity(nextScreen);
		            }
				
			}
		}
	}
	
	

}
