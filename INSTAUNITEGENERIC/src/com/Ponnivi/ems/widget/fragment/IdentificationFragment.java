package com.Ponnivi.ems.widget.fragment;

import java.util.ArrayList;

import org.apache.poi.ss.usermodel.Textbox;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.Ponnivi.ems.R;
import com.Ponnivi.ems.activity.EmsGenericFormMgmtActivity;
import com.Ponnivi.ems.helper.Utilities;

@SuppressLint("NewApi")
public class IdentificationFragment extends Fragment implements OnClickListener, OnItemSelectedListener {

    private Spinner spinner1;
    private Spinner spinner2;
    private Spinner spinner3;
    TextView textView1,textView2,textView3;
    String userName, password, jsonArraySplitter,menuSelection,subMenuSelection,itemCollectionName,customerDbName,evidenceIdentifierName,volleyResponse;
    String relevantJsonFileName = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.identification, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ArrayList<String> identificationList = new ArrayList<String>();
		userName = Utilities.loadPreferences(getActivity(),
				getResources().getString(R.string.email));
		password = Utilities.loadPreferences(getActivity(), Utilities.TAG_PASSWORD);
		menuSelection = Utilities.loadPreferences(getActivity(), Utilities.TAG_MENUSELECTION);
		subMenuSelection = Utilities.loadPreferences(getActivity(), "submenu");
		
        spinner1 = (Spinner) view.findViewById(R.id.spinner1);
        spinner2 = (Spinner) view.findViewById(R.id.spinner2);
        spinner3 = (Spinner) view.findViewById(R.id.spinner3);
        textView1 = (TextView) view.findViewById(R.id.textView1);
        textView2 = (TextView) view.findViewById(R.id.textView2);
        textView3 = (TextView) view.findViewById(R.id.textView3);
        
        if (menuSelection.equalsIgnoreCase("4") || menuSelection.equalsIgnoreCase("1")) {
        	spinner2.setVisibility(View.INVISIBLE);
            spinner3.setVisibility(View.INVISIBLE);
            if (Utilities.loadPreferences(getActivity(), "itemCollectionName").equalsIgnoreCase("items_collection")) {
            	textView1.setText("Toolbox");	
            }
            else {
            	textView1.setText("Equipmentbox");	
            }
            textView2.setVisibility(view.INVISIBLE);
            textView3.setVisibility(view.INVISIBLE);
            spinner1.setOnItemSelectedListener(this);   
            volleyResponse = Utilities.loadPreferences(getActivity(),"volleyresponse");
            if (!volleyResponse.equalsIgnoreCase("No evidences are seen") ) {
            try {
				JSONArray volleyJsonArray = new JSONArray(volleyResponse);
				for (int i = 0; i < volleyJsonArray.length(); i++) {					
					identificationList.add(volleyJsonArray.getString(i));
				}
				ArrayAdapter<String> identificationAdapter = new ArrayAdapter<String>(getActivity().getApplicationContext(), android.R.layout.simple_dropdown_item_1line, identificationList);
				identificationAdapter.setDropDownViewResource(android.R.layout.simple_list_item_checked);
				spinner1.setAdapter(identificationAdapter);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				Utilities.alertDialogDisplay(getActivity(), "No evidences are seen", "No evidences are seen");
			}
            
            String selectedEvidenceIdentifier = Utilities.loadPreferences(getActivity(), "evidenceIdentifierValueNumber");
    		if (selectedEvidenceIdentifier.equalsIgnoreCase("")) {
    			spinner1.setSelection(0);
    		}
    		else {
    			spinner1.setSelection(Integer.parseInt(selectedEvidenceIdentifier));
    		}
    		Utilities.savePreferences("evidenceIdentifierValue", String.valueOf(spinner1.getItemAtPosition(0)), getActivity());        	
    		Utilities.savePreferences(Utilities.TAG_EVIDENCE_IDENTIFIER, String.valueOf(spinner1.getItemAtPosition(0)), getActivity());
        }
        }
        

        else {

		
		JSONObject obj;
		JSONArray m_jArry;
		for (int index = 0; index < Utilities.TAG_SPINNERCOUNT; index++) {
			switch(index) {
			case 0 :
				relevantJsonFileName = "remoteunit.json";
				jsonArraySplitter = Utilities.TAG_REMOTEUNIT;
			break;
			case 1 :
				relevantJsonFileName = "machine.json";
				jsonArraySplitter = Utilities.TAG_MACHINE;
			break;
			case 2 :
				relevantJsonFileName = "supervisor.json";
				jsonArraySplitter = Utilities.TAG_SUPERVISOR;
			break;
			}
			identificationList = new ArrayList<String>();
		try {
			obj = new JSONObject(Utilities.loadFromAsset(getActivity(),
					relevantJsonFileName));
			if (obj != null) {
				m_jArry = obj.getJSONArray(jsonArraySplitter);
				for (int i = 0; i < m_jArry.length(); i++) {
					JSONObject json = m_jArry.getJSONObject(i);
					identificationList.add(json.getString("Option"));
				}
				Utilities.savePreferences(
						Utilities.TAG_CATALOGUESPECIFICTOTALRECORDS,
						String.valueOf(m_jArry.length()), getActivity());
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			Utilities.alertDialogDisplay(getActivity(), "jsonexception",
					e.getMessage());
		}
		ArrayAdapter<String> identificationAdapter = new ArrayAdapter<String>(getActivity().getApplicationContext(), android.R.layout.simple_dropdown_item_1line, identificationList);
		identificationAdapter.setDropDownViewResource(android.R.layout.simple_list_item_checked);
		switch(index) {
		case 0 :
			spinner1.setAdapter(identificationAdapter);
		break;
		case 1 :
			spinner2.setAdapter(identificationAdapter);
		break;
		case 2 :
			spinner3.setAdapter(identificationAdapter);
			break;
		}
		} //for
		spinner1.setOnItemSelectedListener(this);
		spinner2.setOnItemSelectedListener(this);
		spinner3.setOnItemSelectedListener(this);
        

		
		String selectedRemoteUnit = Utilities.loadPreferences(getActivity(), Utilities.TAG_IDENTIFIED_REMOTE_UNIT_NUMBER);
		if (selectedRemoteUnit.equalsIgnoreCase("")) {
			spinner1.setSelection(0);
		}
		else {
			spinner1.setSelection(Integer.parseInt(selectedRemoteUnit));
		}
		
		String selectedMachineUnit = Utilities.loadPreferences(getActivity(), Utilities.TAG_IDENTIFIED_MACHINE_UNIT_NUMBER);
		if (selectedMachineUnit.equalsIgnoreCase("")) {
			spinner2.setSelection(0);
		}
		else {
			spinner2.setSelection(Integer.parseInt(selectedMachineUnit));
		}
		
		String selectedSuperviserUnit = Utilities.loadPreferences(getActivity(), Utilities.TAG_IDENTIFIED_SUPERVISER_UNIT_NUMBER);
		if (selectedSuperviserUnit.equalsIgnoreCase("")) {
			spinner3.setSelection(0);
		}
		else {
			spinner3.setSelection(Integer.parseInt(selectedSuperviserUnit));
		}
		
		Utilities.savePreferences(Utilities.TAG_IDENTIFIED_REMOTE_UNIT, String.valueOf(spinner1.getItemAtPosition(0)), getActivity());
		Utilities.savePreferences(Utilities.TAG_IDENTIFIED_MACHINE_UNIT, String.valueOf(spinner2.getItemAtPosition(0)), getActivity());
		Utilities.savePreferences(Utilities.TAG_IDENTIFIED_SUPERVISER_UNIT, String.valueOf(spinner3.getItemAtPosition(0)), getActivity());
        } //else of menuselection 4
        view.findViewById(R.id.btnSubmit).setOnClickListener(this);
        view.findViewById(R.id.prev).setOnClickListener(this);
    }

    
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnSubmit:
            	if (menuSelection.equalsIgnoreCase("4") || menuSelection.equalsIgnoreCase("1")) {
            		customerDbName = Utilities.loadPreferences(getActivity(),Utilities.TAG_DBNAME);
            		itemCollectionName = Utilities.loadPreferences(getActivity(),"itemCollectionName");
            		evidenceIdentifierName = Utilities.loadPreferences(getActivity(),"evidenceIdentifierValue");
            		String url = Utilities.appendParametersInUrl(Utilities.getSelectiveItemsUrl(getActivity()), customerDbName, itemCollectionName, evidenceIdentifierName);
            		Utilities.makeJsonArryReq(url, getActivity(), "2");
            		
            	}
            	else {
            	Utilities.savePreferences(Utilities.TAG_FLOW, "Reasons", getActivity());
            	Intent i = new Intent(getActivity(), EmsGenericFormMgmtActivity.class);
            	startActivity(i);
            	}
                break;
            case R.id.prev:
            	Utilities.setBackKeyValues(getActivity());
                break;
        }
    }


	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub
		
	}

	public void onItemSelected(AdapterView<?> view, View arg1, int position,
			long arg3) {
		switch (view.getId()) {
        	case R.id.spinner1:
        		if (menuSelection.equalsIgnoreCase("4") || menuSelection.equalsIgnoreCase("1")) {
        			Utilities.savePreferences("evidenceIdentifierValue", String.valueOf(view.getItemAtPosition(position)), getActivity());
        			Utilities.savePreferences("evidenceIdentifierValueNumber", String.valueOf(position), getActivity());
        			Utilities.savePreferences(Utilities.TAG_EVIDENCE_IDENTIFIER, String.valueOf(view.getItemAtPosition(position)), getActivity());
        		}
        		else {
        		Utilities.savePreferences(Utilities.TAG_IDENTIFIED_REMOTE_UNIT, String.valueOf(view.getItemAtPosition(position)), getActivity());
        		Utilities.savePreferences(Utilities.TAG_IDENTIFIED_REMOTE_UNIT_NUMBER, String.valueOf(position), getActivity());
        		}
        		break;
        	case R.id.spinner2:
        		Utilities.savePreferences(Utilities.TAG_IDENTIFIED_MACHINE_UNIT, String.valueOf(view.getItemAtPosition(position)), getActivity());
        		Utilities.savePreferences(Utilities.TAG_IDENTIFIED_MACHINE_UNIT_NUMBER, String.valueOf(position), getActivity());
        		break;
        	case R.id.spinner3:
        		Utilities.savePreferences(Utilities.TAG_IDENTIFIED_SUPERVISER_UNIT, String.valueOf(view.getItemAtPosition(position)), getActivity());
        		Utilities.savePreferences(Utilities.TAG_IDENTIFIED_SUPERVISER_UNIT_NUMBER, String.valueOf(position), getActivity());
        		break;
		}
		
	}
	
	/*public void onResume(){
	    super.onResume();
    	VolleyCallback volleyCallBack = new VolleyCallback() {
			
			public void onSuccess(String result) {
				System.out.println("result.." + result);				
			}
		};
	}*/
}