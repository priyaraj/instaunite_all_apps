package com.Ponnivi.ems.widget.fragment;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.Ponnivi.ems.R;
import com.Ponnivi.ems.activity.EmsGroupsActivity;
import com.Ponnivi.ems.activity.EmsIdentificationFormMgmtActivity;
import com.Ponnivi.ems.activity.EmsSiteVisitItemsActivity;
import com.Ponnivi.ems.helper.Utilities;

@SuppressLint("NewApi")
public class TopHeaderFragment extends Fragment implements OnClickListener {

	private TextView screenName;
	private Button BtnSlide,round;
	private ImageButton emsiconButton;
	String userName,password,menuSelection,subMenuOption;
	String screenNameStr = "";

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.topheader1, null);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		screenName = (TextView) view.findViewById(R.id.screenName);
		BtnSlide = (Button) view.findViewById(R.id.BtnSlide);
		round = (Button) view.findViewById(R.id.round);
//		emsiconButton = (ImageButton)view.findViewById(R.id.emsiconButton);
		//emsiconButton.setBackgroundColor(Color.TRANSPARENT);
	//	emsiconButton.getBackground().setAlpha(0);
		
		/* Made BtnSlide to be false enabled -- June 23 */
		//BtnSlide.setOnClickListener(this);
		BtnSlide.setEnabled(false);
		//
		userName = Utilities.loadPreferences(getActivity(), getResources().getString(R.string.email));
		password = Utilities.loadPreferences(getActivity(), Utilities.TAG_PASSWORD);
		menuSelection = Utilities.loadPreferences(getActivity(), Utilities.TAG_MENUSELECTION);
		switch (getId()) {
		case R.id.textboxheaderfragment:
			subMenuOption = Utilities.loadPreferences(getActivity(), "submenu");
			if (subMenuOption.equalsIgnoreCase("create toolbox")) {
				screenName.setText("Input toolbox entries");
			}
			else {
				screenName.setText("Input equipment entries");
			}
			break;
		case R.id.loginheaderfragment: 
			//screenName.setText(Html.fromHtml("<u>"+ getResources().getString(R.string.loginscreen) +"</u>"));
			screenName.setText(getResources().getString(R.string.loginscreen));
			break;
		case R.id.scancardfragment:
			if (Utilities.loadPreferences(getActivity(), Utilities.TAG_IDENTIFIED_REMOTE_UNIT).equalsIgnoreCase("")) {
				screenName.setText("Identify Machine");
			}
			else { 
				screenName.setText("Identify Supervisor");
			}
			break;
		case R.id.logoutheaderfragment:
			screenName.setText("Logout");
			break;
		case R.id.scanheaderfragment: 
			//screenName.setText(Html.fromHtml("<u>"+ getResources().getString(R.string.scan_barcode) +"</u>"));	
			screenName.setText(getResources().getString(R.string.scan_barcode));
			break;
		case R.id.serviceheaderfragment: 
			String nameOfScreen = Utilities.loadPreferences(getActivity(), getResources().getString(R.string.notestitle));
			if (nameOfScreen.equalsIgnoreCase("Missing Service"))
				//screenName.setText(Html.fromHtml("<u>"+ getResources().getString(R.string.missing_notes) +"</u>"));
				screenName.setText(getResources().getString(R.string.missing_notes));
			else
				//screenName.setText(Html.fromHtml("<u>"+ getResources().getString(R.string.service_notes) +"</u>"));
				screenName.setText(getResources().getString(R.string.service_notes));
			break;
		case R.id.missingserviceheaderfragment: 
			//screenName.setText(Html.fromHtml("<u>"+ getResources().getString(R.string.missing_notes) +"</u>"));
			screenName.setText(getResources().getString(R.string.missing_notes));
			break;
		case R.id.imageuploadheaderfragment: 
			//screenName.setText(Html.fromHtml("<u>"+ getResources().getString(R.string.upload_image) +"</u>"));
			screenName.setText(getResources().getString(R.string.upload_image));
			break;
/*		case R.id.feedbackheaderfragment: 
			//screenName.setText(Html.fromHtml("<u>"+ getResources().getString(R.string.feedback_heading) +"</u>"));
			screenName.setText(getResources().getString(R.string.feedback_heading));
			break;	*/		
		case R.id.menuheaderfragment:
			//screenName.setText(Html.fromHtml("<u>"+ getResources().getString(R.string.menu_items) +"</u>"));
			screenName.setText(getResources().getString(R.string.menu_items));
			screenNameStr = "menuItems";
			break;
		case R.id.toolboxheaderfragment:
			//screenName.setText(Html.fromHtml("<u>"+ getResources().getString(R.string.toolbox) +"</u>"));
			screenName.setText(getResources().getString(R.string.toolbox));
			break;	
		case R.id.catalogueheaderfragment:
			String screen = Utilities.loadPreferences(getActivity(), getResources().getString(R.string.toolbox));			
			//screenName.setText(Html.fromHtml("<u>"+ screen +"</u>"));
			screenName.setText(screen);
			break;
		case R.id.questionerheaderfragment:
			if (menuSelection.equalsIgnoreCase("3")) {
				//screenName.setText(Html.fromHtml("<u>"+ "Question Groups" +"</u>"));
				screenName.setText("Question Groups");
			}
			if (menuSelection.equalsIgnoreCase("4") ||  menuSelection.equalsIgnoreCase("1") || menuSelection.equalsIgnoreCase("2")) {
				//screenName.setText(Html.fromHtml("<u>"+ getResources().getString(R.string.toolbox) +"</u>"));
				screenName.setText("Menu"); //May be this will be modified -- Apr 23
			}
			break;
		case R.id.groupheaderfragment:
			if (userName.equalsIgnoreCase("FS1@g.com")
					&& password.equalsIgnoreCase("Ccc333") || menuSelection.equalsIgnoreCase("3")) {
				//screenName.setText(Html.fromHtml("<u>"+ "Question Groups" +"</u>"));
				screenName.setText("Question Groups");
			}
			break;
		case R.id.genericheaderfragment:
			String flow = Utilities.loadPreferences(getActivity(), Utilities.TAG_FLOW);
			if (userName.equalsIgnoreCase("MC1@g.com")
					&& password.equalsIgnoreCase("bbb444") || menuSelection.equalsIgnoreCase("2")) {
				
				if (flow.equalsIgnoreCase("Reasons")) {
					//screenName.setText(Html.fromHtml("<u>"+ "Reasons Screen" +"</u>"));
					screenName.setText("Reasons Screen");
				}
				if (flow.equalsIgnoreCase("Machine_downtime")) {
					//screenName.setText(Html.fromHtml("<u>"+ "Machine Down Time" +"</u>"));
					screenName.setText("Machine Down Time");
				}
				if (flow.equalsIgnoreCase("action")) {
					//screenName.setText(Html.fromHtml("<u>"+ "Actions Taken" +"</u>"));
					screenName.setText("Actions Taken");
				}
			}
			if (userName.equalsIgnoreCase("TT1@g.com")
					&& password.equalsIgnoreCase("Aaa555") || menuSelection.equalsIgnoreCase("1")) {
			if (flow.equalsIgnoreCase("Parts")) {
				//screenName.setText(Html.fromHtml("<u>"+ "Choose Part" +"</u>"));
				screenName.setText("Choose Part");
			}
			}
			break;
		case R.id.identificationheaderfragment:
			if (menuSelection.equalsIgnoreCase("2")) {
				//screenName.setText(Html.fromHtml("<u>"+ "Identification" +"</u>"));
				screenName.setText("Identification");
			}
			if (menuSelection.equalsIgnoreCase("4") && Utilities.loadPreferences(getActivity(), "submenu").equalsIgnoreCase("toolbox")) {
				screenName.setText("Check Inventory");
			}
			if (menuSelection.equalsIgnoreCase("4") && Utilities.loadPreferences(getActivity(), "submenu").equalsIgnoreCase("equipmentbox")) {
				screenName.setText("List equipment");
			}
			break;	
		case R.id.summaryheaderfragment:
			//screenName.setText(Html.fromHtml("<u>"+ "Summary" +"</u>"));
			screenName.setText("Summary");
			screenNameStr = "menuItems";
			break;	
		case R.id.multiimageheaderfragment :
			String groupName = Utilities.loadPreferences(getActivity(), getResources()
					.getString(R.string.toolbox));
			//screenName.setText(Html.fromHtml("<u>"+ groupName +"</u>"));
			screenName.setText(groupName);
			break;
		case R.id.sitevisitheaderfragment :
			//screenName.setText(Html.fromHtml("<u>"+ "Your workorders and messages" +"</u>"));
			//screenName.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15.0f);
			screenName.setText("Work Orders");
			break;
		case R.id.sitevisitroutineheaderfragment :
			//screenName.setText(Html.fromHtml("<u>"+ "Site visit routine" +"</u>"));
			screenName.setText("Site visit routine");
			break;			
			
			
		}
		
/*		
 * As feedbackheaderfragment right now not referred, commented out on June 11
 * switch(getId()) {
		case R.id.feedbackheaderfragment:
		
			RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)screenName.getLayoutParams();
			String leftMargin = String.valueOf(getResources().getDimension(R.dimen.screennameleft));
			leftMargin = leftMargin.substring(0,leftMargin.indexOf("."));
			params.leftMargin = Integer.parseInt(leftMargin);
			//params.leftMargin = 10;
			screenName.setLayoutParams(params);
			break;
		
		}*/

	}

	public void onClick(View v) {
		switch(v.getId()) {
		case R.id.BtnSlide :
			Intent nextFlow = new Intent();
			if (!screenNameStr.equalsIgnoreCase("menuItems")) { 
				
				Utilities.redirectMenuFlows(menuSelection, getActivity());
			}

		}
		
	}
}
				
/*			//	menuSelection = "4"; //Right now it is hardcoded
			if (menuSelection.equalsIgnoreCase("2")) {
				nextFlow = new Intent(getActivity().getApplicationContext(),
						EmsIdentificationFormMgmtActivity.class);
				nextFlow = new Intent(getActivity(),
						EmsGroupsActivity.class);
			}
			//else if (menuSelection.equalsIgnoreCase("4") || menuSelection.equalsIgnoreCase("3")){
			if (menuSelection.equalsIgnoreCase("4")){
				nextFlow = new Intent(getActivity(),
						EmsGroupsActivity.class);	
			}
			if (menuSelection.equalsIgnoreCase("1")){
				nextFlow = new Intent(getActivity(),
						EmsSiteVisitItemsActivity.class);	
				nextFlow = new Intent(getActivity(),
						EmsGroupsActivity.class);
				//if (Utilities.loadPreferences(getActivity(), "submenu").equalsIgnoreCase(")) if required we need to check condition
				Utilities.savePreferences("submenu", "", getActivity());
			}
			else {
			nextFlow = new Intent(getActivity().getApplicationContext(),
					EmsGroupsActivity.class);
			}

			if (menuSelection.equalsIgnoreCase("2") || menuSelection.equalsIgnoreCase("4") || menuSelection.equalsIgnoreCase("1")){
		//	if (menuSelection.equalsIgnoreCase("1")){
				//nextFlow.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
				nextFlow.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
				getActivity().getApplicationContext().startActivity(nextFlow);
			}*/
			