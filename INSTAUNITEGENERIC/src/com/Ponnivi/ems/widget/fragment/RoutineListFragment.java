package com.Ponnivi.ems.widget.fragment;


import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.ListFragment;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.Ponnivi.ems.R;
import com.Ponnivi.ems.adapter.EmsBaseAdapter;
import com.Ponnivi.ems.helper.Utilities;

@SuppressLint("NewApi")
public class RoutineListFragment extends ListFragment {

    private ArrayList<String> itemTitle;
    private ArrayList<String> itemRoutine;


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        /**update data used by the adapter*/
        updateData();

        /**instantiate adapter*/
        final EmsBaseAdapter mAdapter = new EmsBaseAdapter(getActivity(), itemTitle, itemRoutine);
        

        /**get listView reference*/
        final ListView mListView = (ListView) getActivity().findViewById(R.id.routinelist);
        mListView.setItemsCanFocus(true);
        mListView.setAdapter(mAdapter);
        mListView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        
        mListView.setItemChecked(EmsBaseAdapter.positionCount, true);
     //   mListView.setSelection(EmsBaseAdapter.positionCount);
        mListView.setSelector(R.drawable.orangeband);
        

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

                Log.d("item " + Integer.toString(position), "clicked");

            }
        });
        
        mListView.post(new Runnable(){
        	  public void run() {
        	    mListView.setSelection(EmsBaseAdapter.positionCount);
        	  }});
        
    

    }

    private void updateData(){

        itemTitle = new ArrayList<String>();
        itemRoutine = new ArrayList<String>();
        
        try {
        	JSONObject obj = new JSONObject(Utilities.loadFromAsset(getActivity(),
					"sitevisitroutine.json"));
			JSONArray mjArry = obj.getJSONArray("sitevisitroutine");
			for (int i = 0; i < mjArry.length(); i++) {

				JSONObject jo_inside = mjArry.getJSONObject(i);
				String option = jo_inside.getString("Option");
				//itemTitle.add("Item " + Integer.toString(i));
				itemTitle.add(" ");
				itemRoutine.add(option);
			}
		  Utilities.savePreferences(Utilities.TAG_CATALOGUETOTALRECORDS, String.valueOf(mjArry.length()), getActivity());
        }catch(JSONException ex) {
        	ex.printStackTrace();
        }


    }
    

        

}
