package com.Ponnivi.ems.widget.fragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.Ponnivi.ems.R;
import com.Ponnivi.ems.activity.EmsCatalogueMultimageItemsActivity;
import com.Ponnivi.ems.activity.EmsGroupsActivity;
import com.Ponnivi.ems.activity.EmsGroupsSplitActivity;
import com.Ponnivi.ems.activity.EmsSiteVistRoutineItemsActivity;
import com.Ponnivi.ems.activity.EmsSummaryActivity;
import com.Ponnivi.ems.adapter.EmsBaseAdapter;
import com.Ponnivi.ems.helper.Utilities;

@SuppressLint("NewApi")
public class NotesFragment extends Fragment implements OnClickListener {

	private TextView servicenotes;
	private TextView chooseservicenotesstatus, report;
	private Button waytogo;
	private String buttonName, notesColor = "GREEN";
	private String emailIdPrefernence, barcodePreference,
			userCategoryPrefernence, serviceNotesPreference,
			serviceNotesColorPrefernence, imagePathPrefernence;

	private EditText editnotes;
	private RadioGroup notesstatus;
	private RadioButton radioNotesButton;
	com.Ponnivi.ems.helper.GPSTracker gps;
	private String currentRecordPosition, totalRecords, userName, password,
			specificCatalogueTotalRecords, categoryChosen;
	private View v;
	String miss_or_service = "";

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.ems_notes, null);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		v = view;
		servicenotes = (TextView) view.findViewById(R.id.servicenotes);
		editnotes = (EditText) view.findViewById(R.id.editnotes);
		chooseservicenotesstatus = (TextView) view
				.findViewById(R.id.chooseservicenotesstatus);
		report = (TextView) view.findViewById(R.id.report);
		waytogo = (Button) view.findViewById(R.id.waytogo);
		notesstatus = (RadioGroup) view.findViewById(R.id.notesstatus);

		String prevJsonObject = Utilities.loadPreferences(getActivity()
				.getApplicationContext(), Utilities.TAG_CATALOGUEJSONRECORD);
		String multiImageRequirement = Utilities.loadPreferences(
				v.getContext(), Utilities.TAG_IMAGES_REQUIREMENT);
		if (!multiImageRequirement.equalsIgnoreCase("MultiplePerScreen")) {
			currentRecordPosition = Utilities.loadPreferences(getActivity()
					.getApplicationContext(), Utilities.TAG_CATALOGUERECORDNO);
			totalRecords = Utilities.loadPreferences(getActivity()
					.getApplicationContext(),
					Utilities.TAG_CATALOGUETOTALRECORDS);
			specificCatalogueTotalRecords = Utilities.loadPreferences(
					getActivity().getApplicationContext(),
					Utilities.TAG_CATALOGUESPECIFICTOTALRECORDS);
			categoryChosen = Utilities.loadPreferences(getActivity()
					.getApplicationContext(), Utilities.TAG_CHOSEN_CATEGORIES);
			JSONObject prevJson = null;

			try {
				prevJson = new JSONObject(prevJsonObject);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			currentRecordPosition = "";
		}

		userName = Utilities.loadPreferences(getActivity()
				.getApplicationContext(),
				getResources().getString(R.string.email));
		password = Utilities.loadPreferences(getActivity()
				.getApplicationContext(), Utilities.TAG_PASSWORD);

		waytogo.setOnClickListener(this);
		notesstatus
				.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

					public void onCheckedChanged(RadioGroup group, int checkedId) {
						RadioButton checkedRadioButton = (RadioButton) v
								.findViewById(checkedId);
						notesColor = checkedRadioButton.getText().toString();
					}
				});
		// notesstatus.setOnClickListener(this);
		serviceNotesPreference = getResources()
				.getString(R.string.servicenotes);
		serviceNotesColorPrefernence = getResources().getString(
				R.string.servicenotescolor);
		Utilities.removePreference(serviceNotesPreference, getActivity());
		Utilities.removePreference(serviceNotesColorPrefernence, getActivity());

		switch (getId()) {
		case R.id.servicenotesheaderfragment:
			servicenotes
					.setText(Html.fromHtml("<u>"
							+ getResources().getString(R.string.servicenotes)
							+ "</u>"));
			chooseservicenotesstatus.setText(Html.fromHtml("<u>"
					+ getResources().getString(R.string.servicenotesstatus)
					+ "</u>"));
			miss_or_service = getResources().getString(R.string.service_notes);
			if (!multiImageRequirement.equalsIgnoreCase("MultiplePerScreen")) {
				setNextOrSend();
			} else {
				buttonName = getResources().getString(R.string.next);
			}

			break;
		case R.id.missingnotesheaderfragment:
			servicenotes
					.setText(Html.fromHtml("<u>"
							+ getResources().getString(R.string.missingnotes)
							+ "</u>"));
			chooseservicenotesstatus.setText(Html.fromHtml("<u>"
					+ getResources().getString(R.string.missingnotesstatus)
					+ "</u>"));
			miss_or_service = getResources().getString(R.string.missing_notes);
			if (!multiImageRequirement.equalsIgnoreCase("MultiplePerScreen")) {
				setNextOrSend();
			} else {
				buttonName = getResources().getString(R.string.next);
			}
			break;
		/*
		 * case R.id.serviceheaderfragment:
		 * screenName.setText(Html.fromHtml("<u>"+
		 * getResources().getString(R.string.service_notes) +"</u>")); break;
		 * case R.id.missingserviceheaderfragment:
		 * screenName.setText(Html.fromHtml("<u>"+
		 * getResources().getString(R.string.missing_notes) +"</u>")); break;
		 */

		}
	}

	public void onClick(View v) {
		emailIdPrefernence = getResources().getString(R.string.email);
		barcodePreference = getResources().getString(R.string.barcoderesult);
		userCategoryPrefernence = getResources().getString(
				R.string.userCategory);
		imagePathPrefernence = getResources().getString(R.string.imagePath);
		barcodePreference = getResources().getString(R.string.barcoderesult);
		String menuSelection = Utilities.loadPreferences(getActivity(),
				Utilities.TAG_MENUSELECTION);
		switch (v.getId()) {
		/*
		 * case R.id.notesstatus: // if //
		 * (buttonName.equalsIgnoreCase(getResources
		 * ().getString(R.string.next))) // { // get selected radio button from
		 * radioGroup int selectedId = notesstatus.getCheckedRadioButtonId();
		 * 
		 * // find the radiobutton by returned id radioNotesButton =
		 * (RadioButton) v.findViewById(selectedId);
		 * Utilities.SavePreferences(serviceNotesColorPrefernence,
		 * radioNotesButton.getText().toString(), getActivity()
		 * .getApplicationContext()); // } break;
		 */

		case R.id.waytogo:
			if (buttonName.equalsIgnoreCase(getResources().getString(
					R.string.next))) {
				String Notes = editnotes.getText().toString();
				Utilities.savePreferences(serviceNotesPreference, Notes,
						getActivity().getApplicationContext());
				if (notesColor.equalsIgnoreCase("red")) {
					notesColor = "High_Severity";
				}
				if (notesColor.equalsIgnoreCase("green")) {
					notesColor = "Low_Severity";
				}
				Utilities.savePreferences(serviceNotesColorPrefernence,
						notesColor, getActivity().getApplicationContext());
				Utilities.savePreferences("miss_or_service",
						miss_or_service, v.getContext());
				Utilities.savePreferences(Utilities.TAG_OPTION_RESULT,
						"", v.getContext());
				if (menuSelection.equalsIgnoreCase("4") ||
						(menuSelection.equalsIgnoreCase("1") && Utilities.loadPreferences(v.getContext(), "submenu").equalsIgnoreCase("toolbox")) ) 							
						 {
					EmsCatalogueMultimageItemsActivity multiImage = new EmsCatalogueMultimageItemsActivity();

					String multiImageRequirement = Utilities.loadPreferences(
							v.getContext(), Utilities.TAG_IMAGES_REQUIREMENT);
					if (multiImageRequirement
							.equalsIgnoreCase("MultiplePerScreen")) {
						// We just need to go back to existing previous screen
						// after capturing details from here -- Mar 26
						JSONObject jsonObject;
						try {
							jsonObject = new JSONObject(
									Utilities.loadPreferences(v.getContext(),
											"jsonobject"));
							int recordIndex = Integer.parseInt(Utilities
									.loadPreferences(v.getContext(),
											"recordIndex"));
							multiImage.setFlows(v.getContext(), jsonObject,
									recordIndex);
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						getActivity().finish();
					} else {
						// Need to go back to powertools screen
						int nextRecord = (Integer
								.parseInt(currentRecordPosition) + 1)
								% Integer
										.parseInt(specificCatalogueTotalRecords);

						// Utilities.prepareJson(context);
						if (Integer.parseInt(categoryChosen) < 4
								&& nextRecord == 0) { // went to all records and
														// hence need to go to
														// toolboxactivity
							Utilities.prepareJson(getActivity()
									.getApplicationContext(), servicenotes
									.getText().toString());
							/*
							 * Intent i = new Intent(getActivity(),
							 * EmsCatalogueToolBoxActivity.class);
							 */
							Intent i = new Intent(getActivity(),
									EmsGroupsActivity.class);
							startActivity(i);
						} else {
							if (nextRecord < Integer
									.parseInt(specificCatalogueTotalRecords)) { // go
																				// to
																				// next
																				// record
																				// of
																				// the
																				// series
								Utilities.savePreferences(
										Utilities.TAG_CATALOGUERECORDNO,
										String.valueOf(nextRecord),
										getActivity().getApplicationContext());
								Utilities.prepareJson(getActivity()
										.getApplicationContext(), servicenotes
										.getText().toString());
								Intent i = new Intent(getActivity(),
										EmsGroupsSplitActivity.class);
								startActivity(i);
							} else { // go to toolbox activity to check for
										// other items
								/*
								 * Intent i = new Intent(getActivity(),
								 * EmsCatalogueToolBoxActivity.class);
								 */
								Intent i = new Intent(getActivity(),
										EmsGroupsActivity.class);
								startActivity(i);
							}
						}
					} // else i.e. single image
				
				}// menuselection "4"
				if (menuSelection.equalsIgnoreCase("1") 
						&& Utilities.loadPreferences(v.getContext(), "submenu").equalsIgnoreCase("btnMenu1")) {  //This is the normal flow
					
					Utilities.navigateToListView(v.getContext());
				}
			}
			if (buttonName.equalsIgnoreCase(getResources().getString(
					R.string.send))) {
				/*
				 * if (menuSelection.equalsIgnoreCase("3"))
				 * Utilities.writeToCloud(getActivity().getApplicationContext(),
				 * "questionaireDetails.json"); else
				 * Utilities.writeToCloud(getActivity().getApplicationContext(),
				 * "cataloguedetails.json");
				 */
				/*
				 * Utilities.alertDialogDisplay(getActivity().getApplicationContext
				 * (), "status", "File is sent");
				 */
				Intent nextFlow = new Intent(getActivity()
						.getApplicationContext(), EmsSummaryActivity.class);
				nextFlow.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
						| Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
				getActivity().getApplicationContext().startActivity(nextFlow);

			} // send
		}
	}

	private void setNextOrSend() {
		// For catalogue Management we are doing this. Hence checking the
		// credentials here

		// if (userName.equalsIgnoreCase("AC1@g.com") &&
		// password.equalsIgnoreCase("ddd222")) {
		if (Integer.parseInt(currentRecordPosition) < Integer
				.parseInt(specificCatalogueTotalRecords) - 1) {
			waytogo.setBackgroundResource(R.drawable.next_24);
			buttonName = getResources().getString(R.string.next);
		} else if (Integer.parseInt(categoryChosen) == 4) {// finished all
															// categories
			report.setVisibility(View.VISIBLE);
			report.setText(Html.fromHtml("<u>"
					+ getResources().getString(R.string.send_report) + "</u>"));
			waytogo.setBackgroundResource(R.drawable.sent);
			buttonName = getResources().getString(R.string.send);
		} else { // it need to chose other categories. so only next button
			waytogo.setBackgroundResource(R.drawable.next_24);
			buttonName = getResources().getString(R.string.next);
		}
		// }
	}

}
