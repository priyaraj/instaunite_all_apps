package com.Ponnivi.ems.widget.fragment;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.Ponnivi.ems.R;
import com.Ponnivi.ems.activity.EmsGroupsActivity;
import com.Ponnivi.ems.activity.EmsNfcReaderActivity;
import com.Ponnivi.ems.activity.EmsSiteVisitItemsActivity;
import com.Ponnivi.ems.activity.EmsTextboxActivity;
import com.Ponnivi.ems.helper.Utilities;

@SuppressLint("NewApi")
public class GroupsscreenFragment extends Fragment implements OnClickListener {

	Button btnMenu1, btnMenu2, btnMenu3, btnMenu4, btnMenu5, btnMenu6;
	// btnMenu4,btnMenu5;
	TextView owner, ownerResult, department, departmentResult;
	String userName, password, menuSelection;
	String groupBoxArray;
	ArrayList<String> groupsArray = new ArrayList<String>();
	String groupName = "";
	int jsonGroupCount;
	ArrayList jsonFileNames = new ArrayList();
	String toolBoxArray;
	ArrayList<String> toolsArray = new ArrayList<String>();
	String itemCollectionName,customerDbName,url;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.groupsscreen1, null);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		btnMenu1 = (Button) view.findViewById(R.id.btnMenu1);
		btnMenu2 = (Button) view.findViewById(R.id.btnMenu2);
		btnMenu3 = (Button) view.findViewById(R.id.btnMenu3);
		btnMenu4 = (Button) view.findViewById(R.id.btnMenu4);
		btnMenu5 = (Button) view.findViewById(R.id.btnMenu5);
		btnMenu6 = (Button) view.findViewById(R.id.btnMenu6);
		owner = (TextView) view.findViewById(R.id.owner);
		ownerResult = (TextView) view.findViewById(R.id.ownerResult);
		department = (TextView) view.findViewById(R.id.department);
		departmentResult = (TextView) view.findViewById(R.id.departmentResult);
		userName = Utilities.loadPreferences(getActivity(), getResources()
				.getString(R.string.email));
		password = Utilities.loadPreferences(getActivity(),
				Utilities.TAG_PASSWORD);
		menuSelection = Utilities.loadPreferences(getActivity(),
				Utilities.TAG_MENUSELECTION);

		
		switch (getId()) {
		case R.id.questionergroups:
			if ((userName.equalsIgnoreCase("FS1@g.com") && password
					.equalsIgnoreCase("Ccc333"))
					|| menuSelection.equalsIgnoreCase("3")) {
				btnMenu1.setText("Complaints");
				/*
				 * btnMenu2.setText("Occupancy"); btnMenu3.setText("Others");
				 * btnMenu4.setText("Safeguarding");
				 * btnMenu5.setText("Staffing");
				 */
				jsonGroupCount = 5;
				jsonFileNames.add("complaints.json");
				jsonFileNames.add("occupancy_utilisation.json");
				jsonFileNames.add("others.json");
				jsonFileNames.add("safeguarding.json");
				jsonFileNames.add("staffing.json");
			}

			// telecom service engineer app related checks begin
			if (userName.equalsIgnoreCase("TT1@g.com")
					&& password.equalsIgnoreCase("Aaa555")
					|| menuSelection.equalsIgnoreCase("1")) {
				if (Utilities.loadPreferences(getActivity(), "submenu")
						.equalsIgnoreCase("")) {
					btnMenu1.setText("Workorders");
					btnMenu2.setText("Training and Assessment");
					btnMenu3.setText("Machine Diagnostic Tools");
					btnMenu4.setText("My Account");
					btnMenu5.setVisibility(View.INVISIBLE);
					btnMenu6.setVisibility(View.INVISIBLE);
				}
				// training submenu check
				if (Utilities.loadPreferences(getActivity(), "submenu")
						.equalsIgnoreCase("btnMenu2")) {
					btnMenu1.setText("Training");
					btnMenu2.setText("Coaching");
					btnMenu3.setText("Assessment");
					btnMenu4.setText("Knowledge Center");
					btnMenu5.setVisibility(View.INVISIBLE);
					btnMenu6.setVisibility(View.INVISIBLE);
					btnMenu1.setEnabled(false); // For timebeing this will be
												// inactive -- Apr 23
					btnMenu2.setEnabled(false); // For timebeing this will be
												// inactive -- Apr 23
					btnMenu3.setEnabled(false); // For timebeing this will be
												// inactive -- Apr 23
					btnMenu4.setEnabled(false); // For timebeing this will be
												// inactive -- Apr 23
				}
				// Machine Diagnostic submenu check
				if (Utilities.loadPreferences(getActivity(), "submenu")
						.equalsIgnoreCase("btnMenu3")) {

					btnMenu1.setText("ToolBox");
					btnMenu2.setText("Other Diagnostic Tools");
					btnMenu3.setVisibility(View.INVISIBLE);
					btnMenu4.setVisibility(View.INVISIBLE);
					btnMenu5.setVisibility(View.INVISIBLE);
					btnMenu6.setVisibility(View.INVISIBLE);
					btnMenu2.setEnabled(false); // For timebeing this will be
												// inactive -- Apr 23
				}
				// My Accound submenu
				if (Utilities.loadPreferences(getActivity(), "submenu")
						.equalsIgnoreCase("btnMenu4")) {
					btnMenu1.setText("Time Sheet & Attendance");
					btnMenu2.setText("Payments & Bonuses");
					btnMenu3.setText("Trainings & Scores");
					btnMenu4.setText("Performance Evaluation");
					btnMenu5.setText("Favourites");
					btnMenu6.setText("Profile");
					// For timebeing this will be inactive -- Apr 24
					btnMenu1.setEnabled(false);
					btnMenu2.setEnabled(false);
					btnMenu3.setEnabled(false);
					btnMenu4.setEnabled(false);
					btnMenu5.setEnabled(false);
					btnMenu6.setEnabled(false);

				}
			} // telecom service engineer app related checks end

			// machine superviser app begin
			if (userName.equalsIgnoreCase("MC1@g.com")
					&& password.equalsIgnoreCase("bbb444")
					|| menuSelection.equalsIgnoreCase("2")) {
				if (Utilities.loadPreferences(getActivity(), "submenu")
						.equalsIgnoreCase("") || 
						Utilities.loadPreferences(getActivity(), "submenu")
						.equalsIgnoreCase("btnMenu1")) {
					btnMenu1.setText("Machine mgmt");
					btnMenu2.setText("Supervisor Performance Mgmt");
					btnMenu3.setText("Machine Performance Mgmt");
					btnMenu4.setText("Diagnostic Tools");
					btnMenu5.setText("Custom Applications");
					btnMenu2.setEnabled(false);
					btnMenu3.setEnabled(false);
					btnMenu4.setEnabled(false);
					btnMenu5.setEnabled(false);
					btnMenu6.setVisibility(View.INVISIBLE);
				}

			}// machine superviser app end

			if ((userName.equalsIgnoreCase("AC1@g.com") && password
					.equalsIgnoreCase("ddd222"))
					|| menuSelection.equalsIgnoreCase("4")) {
				btnMenu1.setText("Check Inventory");
				btnMenu2.setText("Create Toolbox");
				btnMenu3.setText("List Equipment");
				btnMenu4.setText("Create Equipment");
				btnMenu5.setVisibility(View.INVISIBLE);
				btnMenu6.setVisibility(View.INVISIBLE);
				//btnMenu3.setEnabled(false);
			}

			break;
		}

		view.findViewById(R.id.btnMenu1).setOnClickListener(this);
		view.findViewById(R.id.btnMenu2).setOnClickListener(this);
		view.findViewById(R.id.btnMenu3).setOnClickListener(this);
		view.findViewById(R.id.btnMenu4).setOnClickListener(this);
		view.findViewById(R.id.btnMenu5).setOnClickListener(this);
		view.findViewById(R.id.btnMenu6).setOnClickListener(this);
	}

	public void onClick(View view) {
		Intent nextScreen = new Intent();
		switch (view.getId()) {
		case R.id.btnMenu1:
			groupName = btnMenu1.getText().toString();
			break;
		case R.id.btnMenu2:
			groupName = btnMenu2.getText().toString();
			break;
		case R.id.btnMenu3:
			groupName = btnMenu3.getText().toString();
			break;
		case R.id.btnMenu4:
			groupName = btnMenu4.getText().toString();
			break;
		case R.id.btnMenu5:
			groupName = btnMenu5.getText().toString();
			break;
		case R.id.btnMenu6:
			groupName = btnMenu6.getText().toString();
			break;

		}

		if (menuSelection.equalsIgnoreCase("4")) {
			url = "";
			switch (view.getId()) {
				case R.id.btnMenu1:
				case R.id.btnMenu2:
					itemCollectionName = "items_collection";
					break;
				case R.id.btnMenu3:
				case R.id.btnMenu4:
					itemCollectionName = "items_equipments_collection";
					break;
				
			}
			customerDbName = Utilities.loadPreferences(getActivity(),Utilities.TAG_DBNAME);
			Utilities.savePreferences("itemCollectionName", itemCollectionName, getActivity());
        	url = Utilities.appendParametersInUrl(Utilities.getDistinctToolOrEquipmentBoxes(getActivity()),customerDbName,itemCollectionName,"");		        	

			switch (view.getId()) {		
				
			case R.id.btnMenu1:
				Utilities.savePreferences("submenu", "toolbox",
						view.getContext());
				//checking to remove items, so that we can freshly take the modified items alone
				Utilities.removePreference(Utilities.TAG_ITEMS, view.getContext());
				Utilities.makeJsonArryReq(url,getActivity().getApplicationContext(), "1");
				
	        		
			break;
			case R.id.btnMenu2:
				Utilities.savePreferences("submenu", "create toolbox",
						view.getContext());
				//Utilities.makeJsonArryReq(url,getActivity().getApplicationContext(), "createtoolbox");	
			setCataLogueDetails("inventory.json");
			nextScreen = new Intent(getActivity(), EmsTextboxActivity.class);
			startActivity(nextScreen);			
			break;
			case R.id.btnMenu3:
				Utilities.savePreferences("submenu", "equipmentbox",
						view.getContext());			
				//checking to remove items, so that we can freshly take the modified items alone
				Utilities.removePreference(Utilities.TAG_ITEMS, view.getContext());
	        	Utilities.makeJsonArryReq(url,getActivity().getApplicationContext(), "1");	
	        break;
			case R.id.btnMenu4:
				Utilities.savePreferences("submenu", "create equipment",
						view.getContext());
				//Utilities.makeJsonArryReq(url,getActivity().getApplicationContext(), "createequipmentbox");	
			setCataLogueDetails("inventory_equipment.json");
			nextScreen = new Intent(getActivity(), EmsTextboxActivity.class);
			startActivity(nextScreen);			
			break;
			}
		}
		if (userName.equalsIgnoreCase("MC1@g.com")
				&& password.equalsIgnoreCase("bbb444")
				|| menuSelection.equalsIgnoreCase("2")) {
			switch (view.getId()) {
			case R.id.btnMenu1:
				Utilities.savePreferences("submenu", "btnMenu1",
						view.getContext());
				//Transfering to EmsScanNFCCard as per discussion on July 8. If the device is NFC enabled it will progress there or else progress for EmsIdentificationFormMgmtActivity 
/*				nextScreen = new Intent(getActivity(),
						EmsIdentificationFormMgmtActivity.class);
*/				nextScreen = new Intent(getActivity(),
		EmsNfcReaderActivity.class);
				startActivity(nextScreen);
				break;
			// Todo for rest of buttons
			}
		}
		if (userName.equalsIgnoreCase("TT1@g.com")
				&& password.equalsIgnoreCase("Aaa555")
				|| menuSelection.equalsIgnoreCase("1")) {

			switch (view.getId()) {
			case R.id.btnMenu1:
				if (Utilities.loadPreferences(getActivity(), "submenu")
						.equalsIgnoreCase("btnMenu3")) {
					Utilities.savePreferences("submenu", "toolbox",
							view.getContext());
					/* Commented on 3rd July to accomodate the same result of check inventory
					 * setCataLogueDetails("inventory.json");
					nextScreen = new Intent(getActivity(),
							EmsGroupsSplitActivity.class);*/
					customerDbName = Utilities.loadPreferences(getActivity(),Utilities.TAG_DBNAME);
					itemCollectionName = "items_collection";
					Utilities.savePreferences("itemCollectionName", itemCollectionName, getActivity());
		        	url = Utilities.appendParametersInUrl(Utilities.getDistinctToolOrEquipmentBoxes,customerDbName,itemCollectionName,"");
		        	Utilities.makeJsonArryReq(url,getActivity().getApplicationContext(), "1");
				} else if (Utilities.loadPreferences(getActivity(), "submenu")
						.equalsIgnoreCase("btnMenu2")) {
					// Todo
				} else {
					Utilities.savePreferences("submenu", "btnMenu1",
							view.getContext());
					nextScreen = new Intent(getActivity(),
							EmsSiteVisitItemsActivity.class);
					startActivity(nextScreen);  //Moved here on 3rd july
				}
				break;
			case R.id.btnMenu2:
				Utilities.savePreferences("submenu", "btnMenu2",
						view.getContext());
				nextScreen = new Intent(getActivity(), EmsGroupsActivity.class);
				startActivity(nextScreen);  //Moved here on 3rd july
				break;
			case R.id.btnMenu3:
				Utilities.savePreferences("submenu", "btnMenu3",
						view.getContext());
				nextScreen = new Intent(getActivity(), EmsGroupsActivity.class);
				startActivity(nextScreen);  //Moved here on 3rd july
				break;
			case R.id.btnMenu4:
				Utilities.savePreferences("submenu", "btnMenu4",
						view.getContext());
				nextScreen = new Intent(getActivity(), EmsGroupsActivity.class);
				startActivity(nextScreen);  //Moved here on 3rd july
				break;
			}
			
		}
		

	}

	private void setCataLogueDetails(String jsonFileName) {
		// Need to change here
		jsonGroupCount = 1;
		jsonFileNames.add(jsonFileName); //Changed on June 22 to support both toolbox/equipment
		int totalRecords = Utilities.getTotalNumberOfRecordsFromAssetJson(
				getActivity(), jsonGroupCount, jsonFileNames);
		Utilities.savePreferences(Utilities.TAG_CATALOGUETOTALRECORDS,
				String.valueOf(totalRecords), getActivity());
	}
	



}
