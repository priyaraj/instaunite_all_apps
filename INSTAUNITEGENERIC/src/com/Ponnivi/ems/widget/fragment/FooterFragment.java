package com.Ponnivi.ems.widget.fragment;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.Ponnivi.ems.R;
import com.Ponnivi.ems.activity.EmsCatalogueToolBoxActivity;
import com.Ponnivi.ems.activity.EmsMissingServiceNotes;
import com.Ponnivi.ems.activity.EmsServiceNotes;
import com.Ponnivi.ems.helper.SetNextFlow;
import com.Ponnivi.ems.helper.Utilities;

@SuppressLint("NewApi")
public class FooterFragment extends Fragment implements OnClickListener {

	Button ok, notok, repair;
//	String currentRecNo,specificTotalRecord,categoryChosen;
	SetNextFlow setNextFlow = null;
	View vw = null;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.footer, null);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		vw = view;
		ok = (Button) view.findViewById(R.id.ok);
		ok.setOnClickListener(this);
		notok = (Button) view.findViewById(R.id.notok);
		repair = (Button) view.findViewById(R.id.repair);
		notok.setOnClickListener(this);
		repair.setOnClickListener(this);
		/*currentRecNo = Utilities.LoadPreferences(getActivity().getApplicationContext(),Utilities.TAG_CATALOGUERECORDNO);
		
		specificTotalRecord = Utilities.LoadPreferences(getActivity().getApplicationContext(), Utilities.TAG_CATALOGUESPECIFICTOTALRECORDS);
		categoryChosen = Utilities.LoadPreferences(getActivity().getApplicationContext()
				, Utilities.TAG_CHOSEN_CATEGORIES);*/
	}

	public void onClick(View v) {
		
		setNextFlow = new SetNextFlow(getActivity().getApplicationContext());
		setNextFlow.getSpecificFlow(v.getId());
		/*Intent nextFlow = new Intent();
		boolean nextFlowPresent = true;
		switch (v.getId()) {
		case R.id.notok:
			nextFlow = new Intent(getActivity(), EmsMissingServiceNotes.class);
			break;

		case R.id.repair:
			nextFlow = new Intent(getActivity(), EmsServiceNotes.class);
			break;
		case R.id.ok:
			nextFlowPresent = false;
			setNextFlow = new SetNextFlow(v.getContext());
			//Utilities.setNextFlow(getActivity().getApplicationContext());
			break;
		}
		if (nextFlowPresent) {
			nextFlow.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
			getActivity().getApplicationContext().startActivity(nextFlow);
		}*/
		
	}
		
/*		private boolean setNextFlow() {
			if (currentRecNo < specificTotalRecord) {				
				currentRecNo = (currentRecNo + 1) % specificTotalRecord;
				Utilities.setNextFlow(currentRecNo,specificTotalRecord,getActivity().getApplicationContext());
			} else if (Integer.parseInt(categoryChosen) < 4 ) {
				Utilities.prepareJson(context);
				Intent nextFlow = new Intent(context,
						EmsCatalogueToolBoxActivity.class);
				context.startActivity(nextFlow);
			}
			else {
				Utilities.writeToCloud(context);
				Utilities.alertDialogDisplay(context, "status",
						  "File is sent");
			}
			return false;
		}*/		
}