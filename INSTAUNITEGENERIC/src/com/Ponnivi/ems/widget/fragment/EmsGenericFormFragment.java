package com.Ponnivi.ems.widget.fragment;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.ScrollView;

import com.Ponnivi.ems.R;
import com.Ponnivi.ems.activity.EmsGenericFormMgmtActivity;
import com.Ponnivi.ems.activity.EmsIdentificationFormMgmtActivity;
import com.Ponnivi.ems.activity.EmsMenuActivity;
import com.Ponnivi.ems.activity.EmsNfcReaderActivity;
import com.Ponnivi.ems.activity.EmsSiteVistRoutineItemsActivity;
import com.Ponnivi.ems.activity.EmsSummaryActivity;
import com.Ponnivi.ems.activity.EmsUploadImage;
import com.Ponnivi.ems.adapter.EmsBaseAdapter;
import com.Ponnivi.ems.helper.Utilities;

@SuppressLint("NewApi")
public class EmsGenericFormFragment extends Fragment implements OnClickListener, OnCheckedChangeListener {

    private Fragment genericheaderfragment;
    private LinearLayout genericLayout;
    private ScrollView scrollview;
    private RadioGroup status;
    JSONObject obj;
	JSONArray mjArry;
	ArrayList radioList = new ArrayList();
	String flow;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.ems_generic_form, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        genericheaderfragment = getFragmentManager().findFragmentById(R.id.genericheaderfragment);
        genericLayout = (LinearLayout) view.findViewById(R.id.genericLayout);
        scrollview = (ScrollView) view.findViewById(R.id.scrollview);
        status = (RadioGroup) view.findViewById(R.id.status);
        status.setOnCheckedChangeListener(this);
        flow = Utilities.loadPreferences(getActivity(), Utilities.TAG_FLOW);
		String relevantJsonFileName = "";
		String jsonSplitter = "";
		if (flow.equalsIgnoreCase("Parts")) {
			relevantJsonFileName = "assetspart.json";
			jsonSplitter = Utilities.TAG_ASSETSPART_JSON;
		}
		if (flow.equalsIgnoreCase("Reasons")) {
			relevantJsonFileName = "Reasons.json";
			jsonSplitter = Utilities.TAG_REASONS;
		}
		if (flow.equalsIgnoreCase("Machine_downtime")) {
			relevantJsonFileName = "Machine_DownTime.json";
			jsonSplitter = Utilities.TAG_MACHINE_DOWNTIME;
		}
		if (flow.equalsIgnoreCase("action")) {
			relevantJsonFileName = "action.json";
			jsonSplitter = Utilities.TAG_ACTION;
		}

		try {
			obj = new JSONObject(Utilities.loadFromAsset(getActivity(),
					relevantJsonFileName));
			mjArry = obj.getJSONArray(jsonSplitter);
			for (int i = 0; i < mjArry.length(); i++) {

				JSONObject jo_inside = mjArry.getJSONObject(i);
				String option = jo_inside.getString(Utilities.TAG_OPTION);
				radioList.add(option);
			}
		if (flow.equalsIgnoreCase("Reasons") || flow.equalsIgnoreCase("Machine_downtime") || flow.equalsIgnoreCase("Parts")) {
			if (flow.equalsIgnoreCase("Parts")) {
				view.findViewById(R.id.next).setBackgroundResource(R.drawable.ok);
				view.findViewById(R.id.prev).setBackgroundResource(R.drawable.prev);
			}
			Utilities.addRadiosDynamically(getActivity().getParent(),getActivity(), genericLayout,
				scrollview,status,radioList,flow);
		}
		else {
			view.findViewById(R.id.next).setBackgroundResource(R.drawable.ok);
			Utilities.addCheckboxDynamically(getActivity(), genericLayout,
					radioList);
		}
		}catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        view.findViewById(R.id.next).setOnClickListener(this);
        view.findViewById(R.id.prev).setOnClickListener(this);
    }

    public void onClick(View view) {
    	Intent nextFlow = new Intent();
        switch (view.getId()) {        	
            case R.id.next:
            	if (flow.equalsIgnoreCase("Reasons")) {
        			Utilities.savePreferences(Utilities.TAG_FLOW,
        					"Machine_downtime", getActivity());
        			nextFlow = new Intent(getActivity(), EmsGenericFormMgmtActivity.class);
            	}
            	if (flow.equalsIgnoreCase("Machine_downtime")) {
        			Utilities.savePreferences(Utilities.TAG_FLOW, "action",
        					getActivity());
        			nextFlow = new Intent(getActivity(), EmsGenericFormMgmtActivity.class);
            	}
            	if (flow.equalsIgnoreCase("action")) {
        			/*Utilities.SavePreferences(Utilities.TAG_FLOW, "action",
        					getActivity());*/
        			nextFlow = new Intent(getActivity(), EmsSummaryActivity.class);
            	}
            	if (flow.equalsIgnoreCase("Parts")) {
            		JSONArray itemsArray = Utilities.getItemsArray(getActivity());
            		Utilities.savePreferences("miss_or_service",
    						"", getActivity());
					Utilities.saveSiteVisitDetails(getActivity(), itemsArray);
					EmsBaseAdapter.positionCount += 1;
					Intent intent = new Intent(getActivity(),EmsSiteVistRoutineItemsActivity.class);
			        startActivity(intent);
            	}
            	
                break;
            case R.id.prev:
            	if (flow.equalsIgnoreCase("Reasons")) {
            		Utilities.savePreferences("from camera back", "",
        					getActivity());//As back button is pressed, we should have "" for  this
            		Utilities.savePreferences(Utilities.TAG_REASONS_NUMBER,
            				"", getActivity());//As back button is pressed, we should have "" for  this
            		//Here we need to see from which screen we came, i.e. if nfc enabled, we should go to emsnfcadapter, will look that on monday
            		if (Utilities.loadPreferences(getActivity(), "nfc_feature").equalsIgnoreCase("false")) {
            			nextFlow = new Intent(getActivity(), EmsIdentificationFormMgmtActivity.class);
            		}
            		else {
            			Utilities.savePreferences(Utilities.TAG_IDENTIFIED_REMOTE_UNIT,"",getActivity());
            			Utilities.savePreferences(Utilities. TAG_IDENTIFIED_SUPERVISER_UNIT, "",
	    						getActivity());
            			nextFlow = new Intent(getActivity(), EmsNfcReaderActivity.class);
            		}
            	}
            	if (flow.equalsIgnoreCase("Machine_downtime")) {
        			Utilities.savePreferences(Utilities.TAG_FLOW, "Reasons",
        					getActivity());
        			nextFlow = new Intent(getActivity(), EmsGenericFormMgmtActivity.class);
            	}
            	if (flow.equalsIgnoreCase("action")) {
        			Utilities.savePreferences(Utilities.TAG_FLOW, "Machine_downtime",
        					getActivity());
        			nextFlow = new Intent(getActivity(), EmsGenericFormMgmtActivity.class);
            	}
            	if (flow.equalsIgnoreCase("Parts")) {
            		getActivity().finish();
            	}
                break;
        }
        if (!flow.equalsIgnoreCase("Parts")) {
        startActivity(nextFlow); 
        }
    }

	public void onCheckedChanged(RadioGroup arg0, int arg1) {
	//	Utilities.alertDialogDisplay(getActivity(), "selected", String.valueOf(arg1) + radioList.get(arg1));
		for (int i = 0; i < arg0.getChildCount(); i++) {
			RadioButton temp = (RadioButton)arg0.getChildAt(i);
			temp.setButtonDrawable(R.drawable.white_radio);
		}
		RadioButton a = (RadioButton)arg0.getChildAt(arg1);
		if (a != null) {
		a.setButtonDrawable(R.drawable.orange_radio);
		}
		if (flow.equalsIgnoreCase("Reasons")) {
			if (!Utilities.loadPreferences(getActivity(), "from camera back").equalsIgnoreCase("from camera back")) {
		Utilities.savePreferences(Utilities.TAG_REASONS,
				radioList.get(arg1).toString(), getActivity());
		Utilities.savePreferences(Utilities.TAG_REASONS_NUMBER,
				String.valueOf(arg1), getActivity());
		if (radioList.get(arg1).toString().equalsIgnoreCase("Unknown Machine Issue") || arg1 == 6) {
			Utilities.savePreferences(Utilities.TAG_CATALOGUETOCAMERA,
					Utilities.TAG_CATALOGUETOCAMERA, getActivity());
			Intent i = new Intent(getActivity(), EmsUploadImage.class);
			i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
			startActivity(i);
		}
		}
		}
		else {
			Utilities.savePreferences(Utilities.TAG_MACHINE_DOWNTIME,
					radioList.get(arg1).toString(), getActivity());
			Utilities.savePreferences(Utilities.TAG_MACHINE_DOWNTIME_NUMBER,
					String.valueOf(arg1), getActivity());
		}
		}
	
	

		
	}
