package com.Ponnivi.ems.widget.fragment;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.Ponnivi.ems.R;
import com.Ponnivi.ems.helper.Utilities;

@SuppressLint("NewApi")
public class EMSTopHeaderFragment extends Fragment {

	private TextView screenName;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.topheader, null);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		screenName = (TextView) view.findViewById(R.id.screenName);

		switch (getId()) {
		case R.id.loginheaderfragment: 
			screenName.setText(Html.fromHtml("<u>"+ getResources().getString(R.string.loginscreen) +"</u>"));
			break;
/*		case R.id.scanheaderfragment: 
			screenName.setText(Html.fromHtml("<u>"+ getResources().getString(R.string.scan_barcode) +"</u>"));					
			break;*/
		case R.id.serviceheaderfragment: 
			String nameOfScreen = Utilities.loadPreferences(getActivity(), "Title");
			if (nameOfScreen.equalsIgnoreCase("Missing Service"))
				screenName.setText(Html.fromHtml("<u>"+ getResources().getString(R.string.missing_notes) +"</u>"));
			else
				screenName.setText(Html.fromHtml("<u>"+ getResources().getString(R.string.service_notes) +"</u>"));
			break;
		case R.id.missingserviceheaderfragment: 
			screenName.setText(Html.fromHtml("<u>"+ getResources().getString(R.string.missing_notes) +"</u>"));
			break;
		case R.id.imageuploadheaderfragment: 
			screenName.setText(Html.fromHtml("<u>"+ getResources().getString(R.string.upload_image) +"</u>"));
			break;

		}
	}
}