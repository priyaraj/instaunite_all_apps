package com.Ponnivi.ems.widget.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
//import android.app.Fragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.TranslateAnimation;
import android.widget.ImageButton;

import com.Ponnivi.ems.R;
import com.Ponnivi.ems.helper.Utilities;


public class HeaderFragment1 extends Fragment  {


    private ImageButton emsLogoButton;
    String animationPreference = null;
    TranslateAnimation tAnimation;
    boolean animated = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.header, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        emsLogoButton = (ImageButton) view.findViewById(R.id.emsLogoButton);

        animationPreference = view.getContext().getResources()
				.getString(R.string.animationPreference);
        String animationPreferenceStr = Utilities.loadPreferences(getActivity(),
        		animationPreference);
      //  if (animationPreferenceStr != null && !animationPreferenceStr.equals("")) {
        tAnimation = new TranslateAnimation(0, 0, 250, -100);   
        tAnimation.setDuration(5000);
        tAnimation.setRepeatCount(0);
        tAnimation.setInterpolator(new AccelerateDecelerateInterpolator());
        tAnimation.setFillAfter(true);
        tAnimation.setAnimationListener(new AnimationListener() {

            
            public void onAnimationStart(Animation animation) {
                // TODO Auto-generated method stub

            }

            
            public void onAnimationRepeat(Animation animation) {
                // TODO Auto-generated method stub

            }

            
            public void onAnimationEnd(Animation animation) {
                // TODO Auto-generated method stub
/*                button2.setVisibility(View.VISIBLE);
                tAnimation = new TranslateAnimation(0, 0, 0, -(int)(getScreenHeight()/2)); 
                tAnimation.setDuration(5000);
                tAnimation.setRepeatCount(0);
                tAnimation.setInterpolator(new AccelerateDecelerateInterpolator());
                tAnimation.setFillAfter(true);
                if (!animated) {
                button2.startAnimation(tAnimation);
                animated = true;
                }*/
                
             //   startNewAct();
            }
        });

        emsLogoButton.startAnimation(tAnimation);
        //}
        
    }
/*    private float getScreenHeight() {

        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        return (float) displaymetrics.heightPixels;

    } */   

}