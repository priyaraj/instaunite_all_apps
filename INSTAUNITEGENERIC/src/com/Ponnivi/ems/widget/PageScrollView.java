package com.Ponnivi.ems.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ScrollView;

public class PageScrollView extends ScrollView {
    public PageScrollView(Context context) {
        super(context);
    }

    public PageScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PageScrollView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }
    
    public interface OnScrollViewListener {
        void onScrollChanged( PageScrollView v, int l, int t, int oldl, int oldt );
    }
    private OnScrollViewListener mOnScrollViewListener;

    public void setOnScrollViewListener(OnScrollViewListener l) {
        this.mOnScrollViewListener = l;
    }
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        mOnScrollViewListener.onScrollChanged( this, l, t, oldl, oldt );
        super.onScrollChanged( l, t, oldl, oldt );
    }
}

