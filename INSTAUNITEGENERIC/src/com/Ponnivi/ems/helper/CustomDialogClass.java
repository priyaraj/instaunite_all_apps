package com.Ponnivi.ems.helper;


import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import com.Ponnivi.ems.R;
import com.Ponnivi.ems.activity.EmsGenericFormMgmtActivity;
import com.Ponnivi.ems.activity.EmsMissingServiceNotes;
import com.Ponnivi.ems.activity.EmsServiceNotes;
import com.Ponnivi.ems.activity.EmsUploadImage;

public class CustomDialogClass extends Dialog implements
		android.view.View.OnClickListener {

	public Activity activity;
	public Dialog d;
	public Button take_picture, service_note, choose_part,missing_note;
	public static int missingCount = 0;
	public static int serviceCount = 0;
	public static int imageCount = 0;
	public CustomDialogClass(Activity a) {
		super(a);
		// TODO Auto-generated constructor stub
		this.activity = a;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.customdialogue);
		take_picture = (Button) findViewById(R.id.take_picture);
		service_note = (Button) findViewById(R.id.service_note);
		choose_part = (Button) findViewById(R.id.choose_part);
		missing_note = (Button) findViewById(R.id.missing_note);
		take_picture.setOnClickListener(this);
		service_note.setOnClickListener(this);
		choose_part.setOnClickListener(this);
		missing_note.setOnClickListener(this);
	}

	
	public void onClick(View v) {
		Intent nextScreen = new Intent();
		switch (v.getId()) {
		case R.id.take_picture:			
			imageCount += 1;
			Utilities
			.savePreferences("noimage", String.valueOf(imageCount), v.getContext()); //As we need to use in summary page, reusing noimage itself
			nextScreen = new Intent(v.getContext(),
					EmsUploadImage.class);			
			break;
		case R.id.service_note:
			serviceCount += 1;
			Utilities.savePreferences("service", String.valueOf(serviceCount), v.getContext());
			nextScreen = new Intent(v.getContext(),
					EmsServiceNotes.class);
			break;
		case R.id.choose_part:
			Utilities.savePreferences(Utilities.TAG_FLOW, "Parts", v.getContext());
        	nextScreen = new Intent(v.getContext(), EmsGenericFormMgmtActivity.class);
			break;
		case R.id.missing_note:
			missingCount += 1;
			Utilities
			.savePreferences("missing", String.valueOf(missingCount), v.getContext());
			nextScreen = new Intent(v.getContext(),
					EmsMissingServiceNotes.class);
			break;
		}
		v.getContext().startActivity(nextScreen);
	}

}