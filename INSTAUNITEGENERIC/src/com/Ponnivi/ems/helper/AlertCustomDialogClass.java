package com.Ponnivi.ems.helper;


import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.Window;
import android.widget.TextView;

import com.Ponnivi.ems.R;

public class AlertCustomDialogClass extends Dialog  {

	public Activity activity;
	
	public TextView errorMsg,headerMsg;
	
	public AlertCustomDialogClass(Activity a) {
		super(a);
		// TODO Auto-generated constructor stub
		this.activity = a;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.alertcustomdialogue);
		errorMsg = (TextView) findViewById(R.id.errorMsg);
		headerMsg = (TextView) findViewById(R.id.headerMsg);
		headerMsg.setText(Utilities.loadPreferences(activity, "restHeaderValue"));
		errorMsg.setText(Utilities.loadPreferences(activity, "restValue"));

	}
}