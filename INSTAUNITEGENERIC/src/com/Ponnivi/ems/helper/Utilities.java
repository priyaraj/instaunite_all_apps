/**
 * Author: Priyaraj
 * */
package com.Ponnivi.ems.helper;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.Settings.Secure;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TextView;

import com.Ponnivi.ems.R;
import com.Ponnivi.ems.activity.EmsCatalogueMultimageItemsActivity;
import com.Ponnivi.ems.activity.EmsCatalogueMultimageItemsActivityCreateToolBox;
import com.Ponnivi.ems.activity.EmsGenericFormMgmtActivity;
import com.Ponnivi.ems.activity.EmsGroupsActivity;
import com.Ponnivi.ems.activity.EmsGroupsSplitActivity;
import com.Ponnivi.ems.activity.EmsIdentificationFormMgmtActivity;
import com.Ponnivi.ems.activity.EmsMenuActivity;
import com.Ponnivi.ems.activity.EmsNfcReaderActivity;
import com.Ponnivi.ems.activity.EmsServiceNotes;
import com.Ponnivi.ems.activity.EmsSiteVisitItemsActivity;
import com.Ponnivi.ems.activity.EmsSiteVistRoutineItemsActivity;
import com.Ponnivi.ems.activity.EmsTextboxActivity;
import com.Ponnivi.ems.activity.EmsUploadImage;
import com.Ponnivi.ems.adapter.EmsBaseAdapter;
import com.Ponnivi.ems.adapter.MultipartRequest;
import com.android.volley.AuthFailureError;
import com.android.volley.ClientError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

public class Utilities {

	static int passwordMinLength = 6;
	static String notificationPreference = null;
	ProgressDialog pDialog;
	private static String tag_json_obj = "jobj_req", tag_json_arry = "jarray_req";
	static Context ctx;
	public static final String getAdditionalInstructionsUrl = "http://52.11.147.41/instatest/readadditionalinstructions.jsp";
	//public static final String uploadDataJsonToServerUrl = "http://192.168.137.1:8080/formbasedreadandwriteroutineexamples/UploadToServer";
	//public static final String uploadImageToServerUrl = "http://192.168.137.1:8080/formbasedreadandwriteroutineexamples/UploadImageToServer";
	public static String uploadImageToServerUrl = "";
	//public static final String uploadDataJsonToServerUrl = "http://52.11.147.41/instacustomers/UploadToServer";
	public static String uploadDataJsonToServerUrl = "";
	//public static final String getDistinctToolOrEquipmentBoxes = "http://52.11.147.41/instatest2/getDistinctBoxes.jsp";
	//public static final String getSelectiveItemsUrl = "http://52.11.147.41/instatest2/getSelectedItemsForToolsOrEquipments.jsp";
	public static String getDistinctToolOrEquipmentBoxes = "";
	public static String getSelectiveItemsUrl = "";
	public static final String TAG_LOGINID = "LoginID";
	public static final String TAG_USERNAME = "UserName";
	public static final String TAG_PASSWORD = "Password";
	public static final String TAG_Login = "Login";
	public static final String TAG_UserCategory = "UserCategory";
	public static final String TAG_DBNAME = "CustomerDbName";
	public static final String TAG_SERVERURL = "ServerUrl";
	public static final String TAG_CUSTOMERSPECIFIC = "CustomerSpecific";
	public static final String TAG_CATALOGUERECORDNO = "RecordNo";
	public static final String TAG_CATALOGUESCANNEDITEMCOUNT = "scannedItemCount";
	public static final String TAG_CATALOGUESWIPEDCOUNT = "swipedCount";
	public static final String TAG_CATALOGUETOTALRECORDS = "RecordCount";
	public static final String TAG_CATALOGUESPECIFICTOTALRECORDS = "SpecificRecordCount";
	public static final String TAG_CATALOGUESPECIFICRECORDS = "specificRecords";
	public static final String TAG_EVIDENCE_RECORD = "Evidence_Record";
	public static final String TAG_CATALOGUESPECIFICRECORDSArrayList = "specificRecordsList";
	public static final String TAG_CATALOGUESPECIFICRECORDSArrayListSize = "specificRecordsListSize";
	public static final String TAG_CHOSEN_CATEGORIES = "ChosenCatalogue";
	public static final String TAG_WHOLE_JSON = "CatalogueJson";
	public static final String TAG_ASSETSPART_JSON = "assetspart";
	public static final String TAG_WHOLE_JSONArray = "CatalogueJsonArray";
	public static final String TAG_NeedAttention = "NeedAttention";
	public static final String TAG_ImagePath = "ImagePath";
	public static final String TAG_SERVICE_USER = "ServiceUser";
	public static final String TAG_SERVICE_PROVIDER = "ServiceProvider";
	public static final String TAG_BarCodeResult = "BarCodeResult";
	public static final String TAG_ServiceNotes = "ServiceNotes";
	public static final String TAG_Catalogue_Details = "CatalogueDetails";
	public static final String TAG_ServiceNotesColorCodes = "ServiceNotesColor";
	public static final String TAG_GPS = "GPS coordinates";
	public static final String TAG_MissingNotes = "MissingNotes";
	public static final String TAG_MissingNotesColorCodes = "MissingNotesColor";
	public static final String TAG_LOGINTIME = "LoginTime";
	public static final String TAG_LOGOUTTIME = "LogoutTime";
	public static final String TAG_CATALOGUE = "Catalogue";
	public static final String TAG_CATALOGUETOCAMERA = "CatalogueToCamera";
	public static final String TAG_ASSETID = "AssetId";
	public static final String TAG_ARTICLENO = "Art.nr";
	public static final String TAG_ARTICLENAME = "Produkt";
	public static final String TAG_PRICE = "price";
	public static final String TAG_INCHARGE = "Incharge";
	public static final String TAG_TOOLBOX = "toolbox";
	public static final String TAG_SCREENX = "screenx";
	public static final String TAG_POSITIONY = "positiony";	
	public static final String TAG_SCANNEDSTATUS = "Scanned_Status";
	public static final String TAG_CATEGORY = "category";
	public static final String TAG_DELETEDSTATUS = "Deleted_Item";
	public static final String TAG_ADDEDSTATUS = "Added_Item";
	public static final String TAG_ADDEDCOUNT = "itemsAddedCount";
	public static final String TAG_CATALOGUEJSONRECORD = "category";
	public static final String TAG_QUESTIONBANK = "QuestionBank";
	public static final String TAG_QUESTIONNAME = "QuestionName";
	public static final String TAG_MENUSELECTION = "MenuSelection";
	public static final String TAG_QuestionerType = "QuestionerType";
	public static final String TAG_REMOTEUNIT = "remoteunit";
	public static final String TAG_MACHINE = "machine";
	public static final String TAG_SUPERVISOR = "supervisor";
	public static final int TAG_SPINNERCOUNT = 3;
	public static final String TAG_IDENTIFIED_REMOTE_UNIT = "identifiedremoteunit";
	public static final String TAG_IDENTIFIED_REMOTE_UNIT_NUMBER = "identifiedremoteunitNumber";
	public static final String TAG_IDENTIFIED_MACHINE_UNIT = "identifiedmachineunit";
	public static final String TAG_IDENTIFIED_MACHINE_UNIT_NUMBER = "identifiedmachineunitNumber";
	public static final String TAG_IDENTIFIED_SUPERVISER_UNIT = "identifiedsuperviserunit";
	public static final String TAG_IDENTIFIED_SUPERVISER_UNIT_NUMBER = "identifiedsuperviserunitNumber";
	public static final String TAG_FLOW = "tag_flow";
	public static final String TAG_REASONS = "reasons";
	public static final String TAG_REASONS_NUMBER = "reasonsNumber";
	public static final String TAG_MACHINE_DOWNTIME = "machine_downtime";
	public static final String TAG_MACHINE_DOWNTIME_NUMBER = "machine_downtime_number";
	public static final String TAG_ACTION = "action";
	public static final String TAG_ACTION_COUNT = "actionCount";
	public static final String TAG_OPTION = "Option";
	public static final String TAG_OPTION_RESULT = "OptionResult";
	public static final String TAG_IMAGES_REQUIREMENT = "imagesRequirement";
	public static final String NO_OF_IMAGES_PER_SCREEN = "NoofImagesPerScreen";
	
	public static final String TAG_CUSTOMER_ID = "CustomerId";

	// Entries added on March 16
	public static final String TAG_EVIDENCE_IDENTIFIER = "Evidence_Identifier";
	public static final String TAG_LOCATION = "Evidence_Location";
	public static final String TAG_ENTRY1 = "Entry1";
	public static final String TAG_ENTRY2 = "Entry2";
	public static final String TAG_ENTRY3 = "Entry3";
	public static final String TAG_ENTRY4 = "Entry4";
	public static final String TAG_ENTRY5 = "Entry5";
	public static final String TAG_LOCATION_ENTRIES = "Evidence_Location_Entries";
	public static final String TAG_TIME_LOCATION_DETAILS = "Time_Location_Details"; 
	public static final String TAG_ASSET_XXX = "Asset_Xxx";
	public static final String TAG_ASSET_YYY = "Asset_Yyy";
	public static final String TAG_ASSET_ZZZ = "Asset_Zzz";

	public static final String TAG_ASSET_ID = "Asset_Id";
	public static final String TAG_SE_ID = "SE_Id";
	public static final String TAG_EVIDENCE_ID = "Evidence_Id";
	public static final String TAG_CONTEXT_TYPE = "Context_Type";
	public static final String TAG_ITEMS = "Items";
	public static final String TAG_CAPTURING_DEVICE_DETAILS = "Capturing_Device_Details";
	public static final String TAG_EVIDENCES = "Evidences";

	public static final String TAG_EVIDENCE_1 = "Evidence_1";
	public static final String TAG_EVIDENCE_2 = "Evidence_2";
	public static final String TAG_EVIDENCE_3 = "Evidence_3";
	public static final String TAG_EVIDENCE_4 = "Evidence_4";
	public static final String TAG_EVIDENCE_5 = "Evidence_5";
	public static final String TAG_EVIDENCE_6 = "Evidence_6";

	public static final String TAG_EVIDENCE_7 = "Evidence_7";
	public static final String TAG_EVIDENCE_8 = "Evidence_8";
	public static final String TAG_EVIDENCE_9 = "Evidence_9";
	public static final String TAG_EVIDENCE_10 = "Evidence_10";

	public static final String TAG_EVIDENCE_QUALIFIERS = "Evidence_Qualifiers";
	public static final String TAG_QUALIFIER_1 = "Qualifier_1";

	public static final String TAG_QUALIFIER_2 = "Qualifier_2";
	public static final String TAG_QUALIFIER_3 = "Qualifier_3";
	public static final String TAG_QUALIFIER_4 = "Qualifier_4";
	public static final String TAG_QUALIFIER_5 = "Qualifier_5";
	public static final String TAG_QUALIFIER_6 = "Qualifier_6";
	public static final String TAG_QUALIFIER_7 = "Qualifier_7";

	public static final String TAG_QUALIFIER_8 = "Qualifier_8";
	public static final String TAG_QUALIFIER_9 = "Qualifier_9";
	public static final String TAG_QUALIFIER_10 = "Qualifier_10";
	public static final String TAG_SPECIFIC_DETAILS = "Specific_Details";

	public static final String TAG_A = "A";
	public static final String TAG_B = "B";
	public static final String TAG_C = "C";
	public static final String TAG_D = "D";
	public static final String TAG_E = "E";

	public static final String TAG_F = "F";
	public static final String TAG_G = "G";
	
	 public static final int TAB_SCREEN_SIZE = 3;
	 public static final int GALAXY_S3_SCREEN_SIZE = 2;
	 
	 //telecom tower assets related -- March 30
	 public static final String TAG_SITE_VISIT_NAME = "SiteName";

	static final SimpleDateFormat monthDayYearformatter = new SimpleDateFormat(
			"MMM dd, yyyy");

	static final String monthNames[] = { "January", "February", "March",
			"April", "May", "June", "July", "August", "September", "October",
			"November", "December" };

	static String TAG_DEVICETYPE = "DeviceType";
	static String deviceTypeValue = "ANDROID";
	static String TAG_APIKEY = "APIKey";
	static String TAG_DEVICEID = "DeviceId";

	static String PUSH_KEY_ALERT = "alert";
	static String TAG = "IntentReceiver";

	public static boolean task1Finished = false;

	public static ArrayList addedActionsList = new ArrayList();
	
	public static String volleyResult = "";
	
	static int bufferWhileZipping = 80000;
	
	public static String getDataJsonServerUrl(Context context) {
		uploadDataJsonToServerUrl = "http://" + Utilities.loadPreferences(context, Utilities.TAG_SERVERURL)
				+ "/" + Utilities.loadPreferences(context, Utilities.TAG_CUSTOMERSPECIFIC) + "/UploadToServer";
		return uploadDataJsonToServerUrl;
	}
	public static String getImageUploadToServerUrl(Context context) {
		uploadImageToServerUrl = "http://" + Utilities.loadPreferences(context, Utilities.TAG_SERVERURL)
				+ "/" + Utilities.loadPreferences(context, Utilities.TAG_CUSTOMERSPECIFIC) + "/uploadimg.jsp";
		return uploadImageToServerUrl;
	}
	public static String getDistinctToolOrEquipmentBoxes(Context context) {
		getDistinctToolOrEquipmentBoxes = "http://" + Utilities.loadPreferences(context, Utilities.TAG_SERVERURL)
				+ "/" + Utilities.loadPreferences(context, Utilities.TAG_CUSTOMERSPECIFIC) + "/getDistinctBoxes.jsp";
		return getDistinctToolOrEquipmentBoxes;
	}
	
	public static String getSelectiveItemsUrl(Context context) {
		getSelectiveItemsUrl = "http://" + Utilities.loadPreferences(context, Utilities.TAG_SERVERURL)
				+ "/" + Utilities.loadPreferences(context, Utilities.TAG_CUSTOMERSPECIFIC) + "/getSelectedItemsForToolsOrEquipments.jsp";
		return getSelectiveItemsUrl;
	}

	public static File getDir(String customerId, String evidenceIdentifierId) {
		File sdDir = Environment
				.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
		return new File(sdDir, "EMS/"+ customerId + "/" + evidenceIdentifierId);
	}

	public static GPSTracker GPSdetails(Context context) {
		GPSTracker gps = new GPSTracker(context);
		// check if GPS enabled
		if (gps.canGetLocation()) {

		} else {
			// can't get location
			// GPS or Network is not enabled
			// Ask user to enable GPS/network in settings
			gps.showSettingsAlert();
		}

		return gps;
	}

	public static ArrayList<JSONObject> loadJsonValues(Context context,
			String jsonArraySplitter, String relevantJsonFileName) {
		JSONObject obj;
		JSONArray m_jArry;
		ArrayList<JSONObject> groupList = new ArrayList<JSONObject>();
		try {
			obj = new JSONObject(Utilities.loadFromAsset(context,
					relevantJsonFileName));
			if (obj != null) {
				m_jArry = obj.getJSONArray(jsonArraySplitter);
				for (int i = 0; i < m_jArry.length(); i++) {
					groupList.add(m_jArry.getJSONObject(i));
				}
				Utilities.savePreferences(
						Utilities.TAG_CATALOGUESPECIFICTOTALRECORDS,
						String.valueOf(m_jArry.length()), context);
				Utilities.savePreferences(
						Utilities.TAG_CATALOGUESPECIFICRECORDS,
						m_jArry.toString(), context);



			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			/*
			 * Utilities.alertDialogDisplay(context, "jsonexception",
			 * e.getMessage());
			 */
		}
		return groupList;
	}

	public static ArrayList getMemberValuesForCatalogueMgmt(Context context,
			String jsonArraySplitter, String relevantJsonFileName) {
		JSONObject obj;
		JSONArray m_jArry;
		ArrayList memberDetails = new ArrayList();
		try {
			obj = new JSONObject(Utilities.loadFromAsset(context,
					relevantJsonFileName));
			if (obj != null) {
				m_jArry = obj.getJSONArray(jsonArraySplitter);
				for (int i = 0; i < m_jArry.length(); i++) {
					JSONObject innerJson = m_jArry.getJSONObject(i);
					memberDetails.add(innerJson
							.getString(Utilities.TAG_INCHARGE));
					Utilities.savePreferences(Utilities.TAG_INCHARGE,
							innerJson.getString(Utilities.TAG_INCHARGE),
							context);
					memberDetails.add(innerJson.getString("department"));
					break;
				}
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			/*
			 * Utilities.alertDialogDisplay(context, "jsonexception",
			 * e.getMessage());
			 */
		}
		return memberDetails;
	}

	public static String loadFromAsset(Context context, String assetJson) {
		String json = null;
		try {

			InputStream is = context.getAssets().open(assetJson);

			int size = is.available();

			byte[] buffer = new byte[size];

			is.read(buffer);

			is.close();

			json = new String(buffer, "UTF-8");

		} catch (IOException ex) {
			ex.printStackTrace();
			return null;
		}
		return json;

	}

	static String convertToGivenFont(String inputField) {
		String arialText = "<font  face='Calibri'>" + inputField + "</font>";
		return arialText;
	}

	static String convertToMixFont1(String inputField1, String inputField2,
			String inputColor) {
		StringBuilder convertedText = new StringBuilder();
		convertedText
				.append("<font  face='Georgia, Arial' color='@color/black_color'>")
				.append(inputField1)
				.append("</font> <font face='Georgia, Arial' color='"
						+ inputColor + "'>").append(" " + inputField2)
				.append("</font>");
		return convertedText.toString();
	}

	static String convertToMixFont(String inputField1, String inputField2,
			String inputColor) {
		StringBuilder convertedText = new StringBuilder();
		convertedText
				.append("<font  face='Georgia, Arial' color='" + inputColor
						+ "'>")
				.append(inputField1)
				.append("</font> <font face='Georgia, Arial' color='"
						+ inputColor + "'>").append(" " + inputField2)
				.append("</font>");
		return convertedText.toString();
	}

	public static boolean isEmailValid(CharSequence email, Context context,
			String titleName, String errorMessage) {
		boolean validEmail = android.util.Patterns.EMAIL_ADDRESS.matcher(email)
				.matches();
		if (!validEmail) {
			// txtView.setText("Invalid email pattern");
			Utilities.alertDialogDisplay(context, titleName, errorMessage);
			return false;
		} else {
			return true;
		}

	}

	public static void savePreferences(String key, String value, Context context) {

		SharedPreferences SharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(context);
		SharedPreferences.Editor editor = SharedPreferences.edit();
		editor.putString(key, value);
		editor.commit();
	}

	public static void removePreference(String key, Context context) {

		SharedPreferences SharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(context);
		SharedPreferences.Editor editor = SharedPreferences.edit();
		editor.remove(key);
		editor.commit();

	}

	public static void removeAllPreferences(Context context) {

		SharedPreferences SharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(context);
		SharedPreferences.Editor editor = SharedPreferences.edit();
		editor.clear();
		editor.commit();

	}

	public static String loadPreferences(Context context, String key) {
		SharedPreferences SharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(context);
		String userDetails = SharedPreferences.getString(key, "");
		return userDetails;

	}

	static SharedPreferences getPrefs(Context context, String key) {
		return context.getSharedPreferences(key, Context.MODE_PRIVATE);
	}

	static void CopyStream(InputStream is, OutputStream os) {
		final int buffer_size = 1024;
		try {
			byte[] bytes = new byte[buffer_size];
			for (;;) {
				int count = is.read(bytes, 0, buffer_size);
				if (count == -1)
					break;
				os.write(bytes, 0, count);
			}
		} catch (Exception ex) {
		}
	}

	static String ConvertDate(String text) {

		int pos = text.indexOf("T");
		text = text.substring(0, pos);

		// String MONTHS[] =
		// {"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};

		String splitText[] = text.split("-");

		String mName = splitText[1];

		// String monthName = MONTHS[Integer.parseInt(mName) - 1];

		text = mName + "-" + splitText[2] + "-" + splitText[0];

		return text;
	}

	static String BitMapToString(Bitmap bitmap) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
		byte[] b = baos.toByteArray();
		String temp = Base64.encodeToString(b, Base64.DEFAULT);
		return temp;
	}

	static Bitmap StringToBitMap(String encodedString) {
		try {
			byte[] encodeByte = Base64.decode(encodedString, Base64.DEFAULT);
			Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0,
					encodeByte.length);
			return bitmap;
		} catch (Exception e) {
			e.getMessage();
			return null;
		}
	}

	static String getpreviousYearDate(String dateString) throws ParseException {
		// Create a date formatter using your format string
		DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");

		// Parse the given date string into a Date object.
		// Note: This can throw a ParseException.
		Date myDate = dateFormat.parse(dateString);

		// Use the Calendar class to subtract one day
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(myDate);
		calendar.add(Calendar.DAY_OF_YEAR, -365);
		// calendar.add(Calendar.DAY_OF_MONTH, -1);

		// Use the date formatter to produce a formatted date string
		Date previousDate = calendar.getTime();
		String result = dateFormat.format(previousDate);

		return result;
	}

	public static boolean checkInternetConnection(Activity activity,
			Context context) {

		ConnectionDetector cd = new ConnectionDetector(context);
		AlertDialogManager alert = new AlertDialogManager();

		// Check for internet connection
		if (!cd.isConnectingToInternet()) {
			// Internet Connection is not present
			alert.showAlertDialog(activity, "Internet Connection Error",
					"Please check your internet connectivity", false);
			// stop executing code by return
			return false;
		}
		else
			return true;

	}

	static ProgressDialog progressDialogDisplay(Activity activity) {

		ProgressDialog pDialog = new ProgressDialog(activity);
		pDialog.setIndeterminate(false);
		pDialog.setCancelable(false);
		if (!activity.getClass().getSimpleName()
				.equals("ListCredPostsReceivedActivity")) {
			pDialog.setMessage("Processing. Please wait");
			pDialog.show();
		} else {
			pDialog.setMessage("");
			pDialog.hide();
		}
		return pDialog;
	}

	static void dismiss(ProgressDialog pDialog) {
		pDialog.dismiss();
	}

	public static void alertDialogDisplay(Context context, String titleMessage,
			String errorMessage) {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);

		// set title
		alertDialogBuilder.setTitle(titleMessage);

		// set dialog message
		alertDialogBuilder.setMessage(errorMessage).setCancelable(false)
				.setNegativeButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
					}
				});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}

	public static void requestFocus(EditText fieldName, InputMethodManager imm) {
		imm.showSoftInput(fieldName, InputMethodManager.SHOW_IMPLICIT);
		fieldName.setFocusableInTouchMode(true);
		fieldName.requestFocus();
	}

	public static boolean mandatoryChecks(String fieldName, String fieldValue,
			Context context, String titleName, String errorMessage) {
		boolean result = true;
		if (fieldName.equalsIgnoreCase("First Name")
				|| fieldName.equalsIgnoreCase("Last Name")) {
			if (fieldValue.length() < 2) {
				Utilities.alertDialogDisplay(context, titleName, errorMessage);
				result = false;
			}
		}
		if (fieldName.equalsIgnoreCase("Email")
				|| fieldName.equalsIgnoreCase("Password")
				|| fieldName.equalsIgnoreCase("Confirm Password")
				|| fieldName.equalsIgnoreCase("ArticleNo")
				|| fieldName.equalsIgnoreCase("ArticleName")
				|| fieldName.equalsIgnoreCase("Price")
				|| fieldName.equalsIgnoreCase("ToolBox Name")
				|| fieldName.equalsIgnoreCase("Recorder Name")
				|| fieldName.equalsIgnoreCase("Equipment Name")) {
			if (fieldValue.length() == 0) {
				Utilities.alertDialogDisplay(context, titleName, errorMessage);

				result = false;
			}
		}
		/*
		 * else { if (fieldName.equalsIgnoreCase("cred code")) { if
		 * (fieldValue.length() == 0) { txtView.setText("Please enter " +
		 * fieldName + "to search"); result = false; } } }
		 */
		/*
		 * if (fieldValue.length() > 0) { //txtView.setText(""); result = true;
		 * }
		 */
		return result;
	}

	/** Check if this device has a camera */
	public static boolean checkCameraHardware(Context context) {
		if (context.getPackageManager().hasSystemFeature(
				PackageManager.FEATURE_CAMERA)) {
			// this device has a camera
			return true;
		} else {
			// no camera on this device
			return false;
		}
	}

	public static File getOutputMediaFile() {
		File mediaStorageDir = new File(
				Environment
						.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
				"EMS");
		if (!mediaStorageDir.exists()) {
			if (!mediaStorageDir.mkdirs()) {

			}
		}
		// Create a media file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
				.format(new Date());
		File mediaFile;
		mediaFile = new File(mediaStorageDir.getPath() + File.separator
				+ "IMG_" + timeStamp + ".jpg");

		return mediaFile;
	}



	public static void prepareJson(Context context, String notesStatus) {
		JSONObject serviceNotesJson = new JSONObject();
		JSONArray catalogueDetailsJson = new JSONArray();
		int jsonIndex = 0;
		String serviceNotes = "null", serviceNotesColor = "null", missingNotes = "null", missingNotesColor = "null";
		String userEmail = loadPreferences(context, context.getResources()
				.getString(R.string.email));
		if (!notesStatus.equalsIgnoreCase("")) {
			if (notesStatus.equalsIgnoreCase(context.getResources().getString(
					R.string.servicenotes))) {
				serviceNotes = Utilities.loadPreferences(context, context
						.getResources().getString(R.string.servicenotes));
				if (serviceNotes.equalsIgnoreCase("")) {
					serviceNotes = "null";
				}
				serviceNotesColor = Utilities.loadPreferences(context, context
						.getResources().getString(R.string.servicenotescolor));
				if (serviceNotesColor.equalsIgnoreCase("")) {
					serviceNotesColor = "null";
				}
			}
			if (notesStatus.equalsIgnoreCase(context.getResources().getString(
					R.string.missingnotes))) {
				missingNotes = Utilities.loadPreferences(context, context
						.getResources().getString(R.string.missingnotes));
				if (missingNotes.equalsIgnoreCase("")) {
					missingNotes = "null";
				}
				missingNotesColor = Utilities.loadPreferences(context, context
						.getResources().getString(R.string.missingnotesColor));
				if (missingNotesColor.equalsIgnoreCase("")) {
					missingNotesColor = "null";
				}
			}
		}
		String imagePath = Utilities.loadPreferences(context, context
				.getResources().getString(R.string.imagePath));

		String articleNo = Utilities.loadPreferences(context,
				Utilities.TAG_ARTICLENO);
		String articleName = Utilities.loadPreferences(context,
				Utilities.TAG_ARTICLENAME);
		/*
		 * String barcodeResult = Utilities.LoadPreferences(context, context
		 * .getResources().getString(R.string.barcoderesult));
		 */
		String catalogueJson = Utilities.loadPreferences(context,
				Utilities.TAG_CATALOGUEJSONRECORD);
		String price = Utilities.loadPreferences(context, Utilities.TAG_PRICE);
		String inCharge = Utilities.loadPreferences(context,
				Utilities.TAG_INCHARGE);
		String menuSelection = Utilities.loadPreferences(context,
				Utilities.TAG_MENUSELECTION);

		// Get location details here
		// create class object
		GPSTracker gps = GPSdetails(context);
		SimpleDateFormat df = new SimpleDateFormat("dd-MM-yy HH:mm:ss");
		Calendar calobj = Calendar.getInstance();

		try {
			String wholeJson = Utilities.loadPreferences(context,
					Utilities.TAG_WHOLE_JSONArray);
			if (wholeJson != null && wholeJson.equalsIgnoreCase("")) {
				catalogueDetailsJson = new JSONArray();
				jsonIndex = 0;
			} else {
				try {
					catalogueDetailsJson = new JSONArray(wholeJson);
					jsonIndex = catalogueDetailsJson.length();
					Utilities.savePreferences(Utilities.TAG_WHOLE_JSONArray,
							catalogueDetailsJson.toString(), context);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			serviceNotesJson = new JSONObject();

			serviceNotesJson.put(Utilities.TAG_USERNAME, userEmail);
			// serviceNotesJson.put(Utilities.TAG_UserCategory, userCategory);
			serviceNotesJson.put(Utilities.TAG_ARTICLENO, articleNo);
			serviceNotesJson.put(Utilities.TAG_ARTICLENAME, articleName);
			serviceNotesJson.put(Utilities.TAG_PRICE, price);
			serviceNotesJson.put(Utilities.TAG_INCHARGE, inCharge);
			serviceNotesJson.put(Utilities.TAG_ServiceNotes, serviceNotes);
			// serviceNotesJson.put(Utilities.TAG_Catalogue_Details,
			// catalogueJson);
			/*
			 * if (!menuSelection.equalsIgnoreCase("4")) { serviceNotesJson
			 * .put(Utilities.TAG_BarCodeResult, barcodeResult); // This
			 * 
			 * // need // to // be // changed
			 * serviceNotesJson.put(Utilities.TAG_ImagePath, imagePath); // This
			 * // need // to // be // changed }
			 */
			serviceNotesJson.put(Utilities.TAG_ServiceNotesColorCodes,
					serviceNotesColor);
			serviceNotesJson.put(Utilities.TAG_MissingNotes, missingNotes);
			serviceNotesJson.put(Utilities.TAG_MissingNotesColorCodes,
					missingNotesColor);

			serviceNotesJson.put(
					Utilities.TAG_GPS,
					String.valueOf(gps.getLatitude()) + " , "
							+ String.valueOf(gps.getLongitude()));
			serviceNotesJson.put("LoginTime", df.format(calobj.getTime()));
			catalogueDetailsJson.put(jsonIndex, serviceNotesJson);
			Utilities.savePreferences(Utilities.TAG_WHOLE_JSONArray,
					catalogueDetailsJson.toString(), context);

		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	public static String getActionNames(Context context) {
		int actionCount = 0;
		if (!Utilities.loadPreferences(context, Utilities.TAG_ACTION_COUNT)
				.equalsIgnoreCase("")) {
			actionCount = Integer.parseInt(Utilities.loadPreferences(context,
					Utilities.TAG_ACTION_COUNT));
		}
		String actions = "";
		if (actionCount == 0) {
			actions += Utilities.loadPreferences(context, Utilities.TAG_ACTION
					+ actionCount);
		} else {
			for (int i = 0; i < actionCount; i++) {
				if (i == actionCount - 1) {
					actions += Utilities.loadPreferences(context,
							Utilities.TAG_ACTION + i);
				} else {
					actions += Utilities.loadPreferences(context,
							Utilities.TAG_ACTION + i) + ",";
				}
			}
		}
		return actions;
	}

	public static void prepareMachineUptimeJson(Context context) {
		JSONObject machineUptimeJson = new JSONObject();
		String remoteUnit = Utilities.loadPreferences(context,
				Utilities.TAG_IDENTIFIED_REMOTE_UNIT);
		String machine = Utilities.loadPreferences(context,
				Utilities.TAG_IDENTIFIED_MACHINE_UNIT);
		String superviser = Utilities.loadPreferences(context,
				Utilities.TAG_IDENTIFIED_SUPERVISER_UNIT);
		String reasons = Utilities.loadPreferences(context,
				Utilities.TAG_REASONS);
		String machineDowntime = Utilities.loadPreferences(context,
				Utilities.TAG_MACHINE_DOWNTIME);
		String imagePath = Utilities.loadPreferences(context, context
				.getResources().getString(R.string.imagePath));
		if (imagePath.equalsIgnoreCase("")) {
			imagePath = "null";
		}

		String userEmail = loadPreferences(context, context.getResources()
				.getString(R.string.email));
		if (userEmail.equalsIgnoreCase("")) {
			userEmail = "null";
		}
		// Get location details here
		// create class object
		GPSTracker gps = GPSdetails(context);
		SimpleDateFormat df = new SimpleDateFormat("dd-MM-yy HH:mm:ss");
		Calendar calobj = Calendar.getInstance();

		try {

			machineUptimeJson = new JSONObject();

			machineUptimeJson.put(Utilities.TAG_USERNAME, userEmail);
			machineUptimeJson.put(Utilities.TAG_REMOTEUNIT, remoteUnit);
			machineUptimeJson.put(Utilities.TAG_MACHINE, machine);
			machineUptimeJson.put(Utilities.TAG_SUPERVISOR, superviser);
			machineUptimeJson.put(Utilities.TAG_REASONS, reasons);
			machineUptimeJson.put(Utilities.TAG_MACHINE_DOWNTIME,
					machineDowntime);
			String actions = Utilities.getActionNames(context);
			machineUptimeJson.put(Utilities.TAG_ACTION, actions);

			machineUptimeJson.put(
					Utilities.TAG_GPS,
					String.valueOf(gps.getLatitude()) + " , "
							+ String.valueOf(gps.getLongitude()));
			machineUptimeJson.put(Utilities.TAG_LOGINTIME,
					df.format(calobj.getTime()));
			machineUptimeJson.put(Utilities.TAG_LOGOUTTIME,
					df.format(calobj.getTime()));
			machineUptimeJson.put(Utilities.TAG_ImagePath, imagePath);
			machineUptimeJson.put(Utilities.TAG_SERVICE_PROVIDER, "SP_LAGNA");
			machineUptimeJson.put(Utilities.TAG_SERVICE_USER, "SU_LAGNA");
			Utilities.savePreferences(Utilities.TAG_WHOLE_JSONArray,
					machineUptimeJson.toString(), context);

		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

	}

	public static void prepareQuestionaireJson(Context context) {
		JSONObject serviceNotesJson = new JSONObject();
		JSONArray questionaireDetailsJson = new JSONArray();
		int jsonIndex = 0;
		String userEmail = loadPreferences(context, context.getResources()
				.getString(R.string.email));
		String userCategory = Utilities.loadPreferences(context, context
				.getResources().getString(R.string.userCategory));
		String serviceNotes = Utilities.loadPreferences(context, context
				.getResources().getString(R.string.servicenotes));
		String serviceNotesColor = Utilities.loadPreferences(context, context
				.getResources().getString(R.string.servicenotescolor));
		String imagePath = Utilities.loadPreferences(context, context
				.getResources().getString(R.string.imagePath));
		String missingNotes = Utilities.loadPreferences(context, context
				.getResources().getString(R.string.missingnotes));
		String missingNotesColor = Utilities.loadPreferences(context, context
				.getResources().getString(R.string.missingnotesColor));
		String questionName = Utilities.loadPreferences(context,
				Utilities.TAG_QUESTIONNAME);
		String articleName = Utilities.loadPreferences(context,
				Utilities.TAG_ARTICLENAME);
		String barcodeResult = Utilities.loadPreferences(context, context
				.getResources().getString(R.string.barcoderesult));
		String questionarireJson = Utilities.loadPreferences(context,
				Utilities.TAG_CATALOGUEJSONRECORD);
		// Get location details here
		// create class object
		GPSTracker gps = new GPSTracker(context);
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
		Calendar calobj = Calendar.getInstance();

		// check if GPS enabled
		if (gps.canGetLocation()) {

			double latitude = gps.getLatitude();
			double longitude = gps.getLongitude();

		} else {
			// can't get location
			// GPS or Network is not enabled
			// Ask user to enable GPS/network in settings
			gps.showSettingsAlert();
		}

		try {
			String wholeJson = Utilities.loadPreferences(context,
					Utilities.TAG_WHOLE_JSONArray);
			if (wholeJson != null && wholeJson.equalsIgnoreCase("")) {
				questionaireDetailsJson = new JSONArray();
				jsonIndex = 0;
			} else {
				try {
					questionaireDetailsJson = new JSONArray(wholeJson);
					jsonIndex = questionaireDetailsJson.length();
					Utilities.savePreferences(Utilities.TAG_WHOLE_JSONArray,
							questionaireDetailsJson.toString(), context);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			serviceNotesJson = new JSONObject();

			serviceNotesJson.put(Utilities.TAG_USERNAME, userEmail);
			serviceNotesJson.put(Utilities.TAG_UserCategory, userCategory);
			serviceNotesJson.put(Utilities.TAG_QUESTIONNAME, questionName);
			// serviceNotesJson.put(Utilities.TAG_ARTICLENAME, articleName);
			serviceNotesJson.put(Utilities.TAG_ServiceNotes, serviceNotes);
			serviceNotesJson.put(Utilities.TAG_Catalogue_Details,
					questionarireJson);

			serviceNotesJson.put(Utilities.TAG_BarCodeResult, barcodeResult); // This
																				// need
																				// to
																				// be
																				// changed
			serviceNotesJson.put(Utilities.TAG_ImagePath, imagePath); // This
																		// need
																		// to
																		// be
																		// changed
			serviceNotesJson.put(Utilities.TAG_ServiceNotesColorCodes,
					serviceNotesColor);
			serviceNotesJson.put(Utilities.TAG_MissingNotes, missingNotes);
			serviceNotesJson.put(Utilities.TAG_MissingNotesColorCodes,
					missingNotesColor);

			serviceNotesJson.put(
					Utilities.TAG_GPS,
					String.valueOf(gps.getLatitude()) + " , "
							+ String.valueOf(gps.getLongitude()));
			serviceNotesJson.put(Utilities.TAG_LOGINTIME,
					df.format(calobj.getTime()));
			questionaireDetailsJson.put(jsonIndex, serviceNotesJson);
			Utilities.savePreferences(Utilities.TAG_WHOLE_JSONArray,
					questionaireDetailsJson.toString(), context);

		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
	public static JSONObject getDeviceDetails(Context context) {
		Configuration config = context.getResources().getConfiguration();
	    int screenSize = config.screenLayout & config.SCREENLAYOUT_SIZE_MASK;
	    int screenWidth = getWidth(context)  ;
	    int screenHeight = getHeight(context);
	    DisplayMetrics metrics = new DisplayMetrics();
	    int density = metrics.densityDpi;
	    float xdpi = metrics.xdpi;
	    float ydpi = metrics.ydpi;
	    String phoneModel = android.os.Build.MODEL;
	    String phoneManufacturer = android.os.Build.MANUFACTURER;
	    String brand = android.os.Build.BRAND;
	    String device = android.os.Build.DEVICE;
	    String display = android.os.Build.DISPLAY;
	    String product = android.os.Build.PRODUCT;
	    int apiVersion = android.os.Build.VERSION.SDK_INT;
	    String deviceId = Secure.getString(context.getContentResolver(),
                Secure.ANDROID_ID);
	    JSONObject capturing_device_details = new JSONObject();
	    try {
			capturing_device_details.put("Device_Id", deviceId);
			capturing_device_details.put("Screen_Height", screenHeight);
			capturing_device_details.put("Screen_Width", screenWidth);
			capturing_device_details.put("Screen_Size", screenSize);
			capturing_device_details.put("Density", density);
			capturing_device_details.put("xdpi", xdpi);
			capturing_device_details.put("ydpi", ydpi);
			capturing_device_details.put("Phonemodel", phoneModel);
			capturing_device_details.put("API", apiVersion);
			capturing_device_details.put("Phonemanufacturer", phoneManufacturer);

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    return capturing_device_details;
	}

	public static void writeDataJsonToCloud(Context context, String fileName) {
		ctx = context;
		try {
			JSONArray evidence_record = new JSONArray();
			JSONObject evidence_record_json = new JSONObject();
			String menuSelection = Utilities.loadPreferences(context, Utilities.TAG_MENUSELECTION);
			String evidenceIdentifierId = getDataJsonEvidenceIdentifier(menuSelection, context);
			String customerId = Utilities.loadPreferences(context, Utilities.TAG_CUSTOMER_ID);
			String assetName = getAssetName(context, menuSelection);
			String assetXxx = getDataJsonAssetXXX(menuSelection, context);
			String assetYyy = getDataJsonAssetYYY(menuSelection, context);
			String assetZzz = getDataJsonAssetZZZ(menuSelection, context);
			SimpleDateFormat df = new SimpleDateFormat("dd-MM-yy HH:mm:ss");
			Calendar calobj = Calendar.getInstance();
			GPSTracker gps = GPSdetails(context);
			
			JSONObject time_location_details = new JSONObject();
			
			time_location_details.put(Utilities.TAG_LOGINTIME,
					df.format(calobj.getTime()));
			time_location_details.put(Utilities.TAG_LOGOUTTIME,
					df.format(calobj.getTime()));
			time_location_details.put(Utilities.TAG_EVIDENCE_IDENTIFIER,
					evidenceIdentifierId);
			time_location_details.put(Utilities.TAG_LOCATION, getEvidenceLocation(context,menuSelection));

			JSONObject location_entries = new JSONObject();
			location_entries.put(Utilities.TAG_ENTRY1,
					String.valueOf(gps.getLatitude()));
			location_entries.put(Utilities.TAG_ENTRY2,
					String.valueOf(gps.getLongitude()));
			location_entries.put(Utilities.TAG_ENTRY3,
					"null");
			location_entries.put(Utilities.TAG_ENTRY4,
					"null");
			location_entries.put(Utilities.TAG_ENTRY5,
					"null");
			time_location_details.put(Utilities.TAG_LOCATION_ENTRIES,
					location_entries);
			evidence_record_json.put(Utilities.TAG_TIME_LOCATION_DETAILS,time_location_details);

			//
			evidence_record_json.put(Utilities.TAG_ASSET_XXX, assetXxx);
			evidence_record_json.put(Utilities.TAG_ASSET_YYY, assetYyy); 
			evidence_record_json.put(Utilities.TAG_ASSET_ZZZ, assetZzz);
			//These details need to be available from config_app.json -- June 24
			evidence_record_json.put(Utilities.TAG_CUSTOMER_ID, customerId);
			evidence_record_json.put(Utilities.TAG_ASSET_ID, "asset_id_123");
			evidence_record_json.put(Utilities.TAG_SE_ID, Utilities.loadPreferences(context, Utilities.TAG_SE_ID));
			evidence_record_json.put(Utilities.TAG_DBNAME, Utilities.loadPreferences(context, Utilities.TAG_DBNAME));
			//
			evidence_record_json.put(
					Utilities.TAG_EVIDENCE_ID,
					df.format(calobj.getTime())
							+ "_"
							+ String.valueOf(gps.getLatitude() + "_"
									+ String.valueOf(gps.getLongitude())));
			evidence_record_json.put(Utilities.TAG_CONTEXT_TYPE,
					assetName);

			String items = Utilities.loadPreferences(context,
					Utilities.TAG_ITEMS);
			evidence_record_json.put(Utilities.TAG_ITEMS, new JSONArray(items));
			
			JSONObject capturing_device_details = getDeviceDetails(context);
			evidence_record_json.put(Utilities.TAG_CAPTURING_DEVICE_DETAILS, capturing_device_details);

			evidence_record.put(evidence_record_json);
			
			JSONObject dataJson = new JSONObject();
			dataJson.put(Utilities.TAG_EVIDENCE_RECORD, evidence_record_json);

			File imagesFolder = Utilities.getDir(customerId, evidenceIdentifierId);
			if (!imagesFolder.exists())
				imagesFolder.mkdirs();
			/*
			 * String detailsJson = Utilities.loadPreferences(context,
			 * Utilities.TAG_WHOLE_JSONArray);
			 */

			File jsonFileName = new File(imagesFolder, fileName);
			if (jsonFileName.exists()) {
				try {
					FileOutputStream fOut = new FileOutputStream(jsonFileName);
					OutputStreamWriter myOutWriter = new OutputStreamWriter(
							fOut);
					//As the data json should start with Utilities.TAG_EVIDENCE_RECORD, we have modified -- Mar 26
					myOutWriter.append(dataJson.toString());
					//myOutWriter.append(evidence_record_json.toString());
					myOutWriter.close();
					fOut.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			else {
				FileWriter file = new FileWriter(jsonFileName);
				file.write(dataJson.toString());
				file.flush();
				file.close();
			}

	//		uploadFileToServer(jsonFileName);
			//jsonFileName = new File(imagesFolder, "pic_newImage.jpg");
			Utilities.savePreferences("imageUpload", "no", context);
			Map headerData = new HashMap();
			headerData.put("Content-type", "multipart/form-data");
			
			//MultipartRequest request = new MultipartRequest(uploadUrl, jsonFileName, Response.class, headerData, new Response.Listener(), null);
			uploadDataJsonToServerUrl = getDataJsonServerUrl(context);
			
			MultipartRequest request = new MultipartRequest(context,uploadDataJsonToServerUrl, jsonFileName, Response.class, headerData, new Response.Listener<String>() {
				
				public void onResponse(String response) {
					System.out.println("response.." + response);
					Toast.makeText(ctx, response,
							Toast.LENGTH_LONG).show();
					Utilities.savePreferences("datajsonupload", "success", ctx);
					/*Intent nextFlow = new Intent(ctx, EmsMenuActivity.class);
					ctx.startActivity(nextFlow);*/
				}
				
			}, new Response.ErrorListener(){


				public void onErrorResponse(VolleyError error) {
					System.out.println("errorResponse.." + error);
					if (error instanceof NetworkError
							|| error instanceof ClientError
							|| error instanceof ServerError
							|| error instanceof AuthFailureError
							|| error instanceof ParseError) {
						System.out.println("errorResponse123.." + error);
						Toast.makeText(
								ctx,
								"Error occured. Please contact System administrator"
										, Toast.LENGTH_LONG)
								.show();
						Utilities.savePreferences("datajsonupload", "fail", ctx);
					}
					
				}
			}
		        );
			
			request.setRetryPolicy(new DefaultRetryPolicy(20 * 10000, 5,
					DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
			request.setTag("multipart upload");
			
			AppController.getInstance().addToRequestQueue(request,
					"multipart upload");

		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
	public static String getAssetName(Context context, String menuSelection) {
		String assetName = "";
		if (menuSelection.equalsIgnoreCase("4")) {
			assetName = "Rotating_assets";
		}
		if (menuSelection.equalsIgnoreCase("2")) {
			assetName = "machine_uptime";
		}
		if (menuSelection.equalsIgnoreCase("1")) {
			assetName = "Telecom_Tower_Assets";
		}
		return assetName;
	}
	
	public static String getContextType(Context context, String menuSelection) {
		String type = "";
		if ( (menuSelection.equalsIgnoreCase("4")) || (menuSelection.equalsIgnoreCase("2") || (menuSelection.equalsIgnoreCase("1")) ) ) {
			type = "existing context";
		}
		return type;
	}
	public static String getMutipleItemFunctionality(Context context, String menuSelection) {
		String type = "";
		if (menuSelection.equalsIgnoreCase("4") || menuSelection.equalsIgnoreCase("1")) {
			type = "yes";
		}
		if (menuSelection.equalsIgnoreCase("2")) {
			type = "no";
		}
		return type;
	}
	public static String getPartialEntriesSelection(Context context, String menuSelection) {
		String type = "";
		if (menuSelection.equalsIgnoreCase("4") || menuSelection.equalsIgnoreCase("1")) {
			type = "no";
		}
		if (menuSelection.equalsIgnoreCase("2")) {
			type = "no";
		}
		return type;
	}
	
	public static String getNumberOfEvidences(Context context, String menuSelection) {
		String type = "";
		if (menuSelection.equalsIgnoreCase("4")) {
			type = "6";
		}
		if (menuSelection.equalsIgnoreCase("2") || menuSelection.equalsIgnoreCase("1")) {
			type = "4";
		}
		return type;
	}
	
	public static String getEvidenceIdentifier(Context context, String menuSelection) {
		String type = "";
		if (menuSelection.equalsIgnoreCase("4") || menuSelection.equalsIgnoreCase("1")) {
			type = "manual_entry";
		}
		if (menuSelection.equalsIgnoreCase("2")) {
			type = "manual_entry";
		}
		return type;
	}
	public static String getEvidenceLocation(Context context, String menuSelection) {
		String type = "";
		if (menuSelection.equalsIgnoreCase("4") || menuSelection.equalsIgnoreCase("1")) {
			type = "gps";
		}
		if (menuSelection.equalsIgnoreCase("2")) {
			type = "gps";
		}
		return type;
	}
	public static String getNumberOfItems(Context context, String menuSelection) {
		String type = "";
		if (menuSelection.equalsIgnoreCase("4") || menuSelection.equalsIgnoreCase("1")) {
			type = Utilities.loadPreferences(context, Utilities.TAG_CATALOGUETOTALRECORDS);
		}
		if (menuSelection.equalsIgnoreCase("2")) {
			type = "1";
		}
		return type;
	}
	
	public static String getDataJsonEvidenceIdentifier(String menuSelection, Context context) {
		if (menuSelection.equalsIgnoreCase("2")) {
			return Utilities.loadPreferences(context, Utilities.TAG_IDENTIFIED_REMOTE_UNIT) + "_" +
					Utilities.loadPreferences(context, Utilities.TAG_IDENTIFIED_MACHINE_UNIT) + "_" +
					Utilities.loadPreferences(context, Utilities.TAG_IDENTIFIED_SUPERVISER_UNIT);			
		}
		if (menuSelection.equalsIgnoreCase("4")) {			
			return Utilities.loadPreferences(context, Utilities.TAG_EVIDENCE_IDENTIFIER);
		}
		if (menuSelection.equalsIgnoreCase("1")) {
			return Utilities.loadPreferences(context, Utilities.TAG_SITE_VISIT_NAME);
		}
		return "";
	}
	
	public static String getDataJsonAssetXXX(String menuSelection, Context context) {
		if (menuSelection.equalsIgnoreCase("2")) {
			return Utilities.loadPreferences(context, Utilities.TAG_IDENTIFIED_MACHINE_UNIT);			
		}
		else {
			return "";
		}		
	}
	
	public static String getDataJsonAssetYYY(String menuSelection, Context context) {
		if (menuSelection.equalsIgnoreCase("2")) {
			return Utilities.loadPreferences(context, Utilities.TAG_IDENTIFIED_SUPERVISER_UNIT);			
		}
		else {
			return "";
		}		
	}
	public static String getDataJsonAssetZZZ(String menuSelection, Context context) {
		if (menuSelection.equalsIgnoreCase("2")) {
			return Utilities.loadPreferences(context, Utilities.TAG_IDENTIFIED_REMOTE_UNIT);			
		}
		else {
			return "";
		}		
	}
	
	public static JSONObject getMetaJsonEvidences(String menuSelection) {
		JSONObject evidences = new JSONObject();
		try {
		if (menuSelection.equalsIgnoreCase("2")) {


	evidences.put(Utilities.TAG_EVIDENCE_1,
			"Image_Directory_Location");
evidences.put(Utilities.TAG_EVIDENCE_2,
		"Reasons");
evidences.put(Utilities.TAG_EVIDENCE_3,
		"Machine_DownTime");
evidences.put(Utilities.TAG_EVIDENCE_4,
		"Action");
evidences.put(Utilities.TAG_EVIDENCE_5,
		null);
evidences.put(Utilities.TAG_EVIDENCE_6,
		null);
		}	
		
		if (menuSelection.equalsIgnoreCase("1")) {


	evidences.put(Utilities.TAG_EVIDENCE_1,
			"Image_Directory_Location");
evidences.put(Utilities.TAG_EVIDENCE_2,
		"Service_Note");
evidences.put(Utilities.TAG_EVIDENCE_3,
		"Missing_Note");
evidences.put(Utilities.TAG_EVIDENCE_4,
		"Choose_Part");
evidences.put(Utilities.TAG_EVIDENCE_5,
		null);
evidences.put(Utilities.TAG_EVIDENCE_6,
		null);
		}

		if (menuSelection.equalsIgnoreCase("4")) {
			evidences.put(Utilities.TAG_EVIDENCE_1,
					"Image_Directory_Location");
			evidences.put(Utilities.TAG_EVIDENCE_2,
					"Service_Note");
			evidences.put(Utilities.TAG_EVIDENCE_3,
					"Missing_Note");
			evidences.put(Utilities.TAG_EVIDENCE_4,
					Utilities.TAG_SCANNEDSTATUS);
			evidences.put(Utilities.TAG_EVIDENCE_5,
					Utilities.TAG_ADDEDSTATUS);
			evidences.put(Utilities.TAG_EVIDENCE_6,
					Utilities.TAG_DELETEDSTATUS);

		}
		evidences.put(Utilities.TAG_EVIDENCE_7,
				"null");
		evidences.put(Utilities.TAG_EVIDENCE_8,
				"null");
		evidences.put(Utilities.TAG_EVIDENCE_9,
				"null");
		evidences.put(Utilities.TAG_EVIDENCE_10,
				"null");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return evidences;
	}
	
	public static JSONObject getMetaJsonSpecificDetails(String menuSelection) {
		JSONObject specific_details = new JSONObject();
		try {
		if (menuSelection.equalsIgnoreCase("4")) {
			specific_details.put(Utilities.TAG_A,
					Utilities.TAG_ARTICLENO);
			specific_details.put(Utilities.TAG_B,
					Utilities.TAG_ARTICLENAME);
			specific_details.put(Utilities.TAG_C,
					Utilities.TAG_PRICE);
			specific_details.put(Utilities.TAG_D,
					Utilities.TAG_INCHARGE);
		}
		if (menuSelection.equalsIgnoreCase("2")) {
			specific_details.put(Utilities.TAG_A,
					"null");
		}
		if (menuSelection.equalsIgnoreCase("1")) {
			specific_details.put(Utilities.TAG_A,
					null);
		}
		if (menuSelection.equalsIgnoreCase("1") || menuSelection.equalsIgnoreCase("2")) {
			specific_details.put(Utilities.TAG_B,
					"null");
			specific_details.put(Utilities.TAG_C,
					"null");
			specific_details.put(Utilities.TAG_D,
					"null");
		}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return specific_details;
	}
	
	public static JSONObject getMetaJsonEvidenceQualifiers(String menuSelection) {
		JSONObject evidence_qualifiers = new JSONObject();

		try {
			if (menuSelection.equalsIgnoreCase("4") || menuSelection.equalsIgnoreCase("1")) {
				//As per the discussion had on May 14, following changes are done
			evidence_qualifiers.put(Utilities.TAG_QUALIFIER_1,
					"To_Service_Item");
		
		evidence_qualifiers.put(Utilities.TAG_QUALIFIER_2,
				"Missing_Item");

		evidence_qualifiers.put(Utilities.TAG_QUALIFIER_8,
				"Category");
		
			}
		if (menuSelection.equalsIgnoreCase("2")) {
			evidence_qualifiers.put(Utilities.TAG_QUALIFIER_1,
					"unknown machine issue");
		
		evidence_qualifiers.put(Utilities.TAG_QUALIFIER_2,
				"null");
		//There may be changes in this -- May 27
		evidence_qualifiers.put(Utilities.TAG_QUALIFIER_8,
				"null");
		}
		if (menuSelection.equalsIgnoreCase("1")) {
			evidence_qualifiers.put(Utilities.TAG_QUALIFIER_3,
					"partname");
			evidence_qualifiers.put(Utilities.TAG_QUALIFIER_8,
					"null");
		}
		else {
		evidence_qualifiers.put(Utilities.TAG_QUALIFIER_3,
				"null");
		}
		evidence_qualifiers.put(Utilities.TAG_QUALIFIER_4,
				"null");
			
		evidence_qualifiers.put(Utilities.TAG_QUALIFIER_5,
				"null");
		evidence_qualifiers.put(Utilities.TAG_QUALIFIER_6,
				"null");
		evidence_qualifiers.put(Utilities.TAG_QUALIFIER_7,
				"null");

		evidence_qualifiers.put(Utilities.TAG_QUALIFIER_9,
				"null");
		evidence_qualifiers.put(Utilities.TAG_QUALIFIER_10,
				"null");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return evidence_qualifiers;
		
	}
	
	public static void writeMetaJsonToCloud(Context context, String fileName) {
		try {
			JSONArray evidence_report = new JSONArray();
			String menuSelection = Utilities.loadPreferences(context, Utilities.TAG_MENUSELECTION);
			String assetName = getAssetName(context, menuSelection);
			String contextType = getContextType(context, menuSelection);
			String multiItemFunctionality = getMutipleItemFunctionality(context,menuSelection);
			String numberOfItems = getNumberOfItems(context, menuSelection);
			String evidenceIdentifierId = getDataJsonEvidenceIdentifier(menuSelection, context);
			String customerId = Utilities.loadPreferences(context, Utilities.TAG_CUSTOMER_ID);
			JSONObject evidence_record_json = new JSONObject();
			SimpleDateFormat df = new SimpleDateFormat("dd-MM-yy HH:mm:ss");
			Calendar calobj = Calendar.getInstance();
			GPSTracker gps = GPSdetails(context);

			evidence_record_json.put("Context_Reference_Example",
					assetName);
			evidence_record_json.put(Utilities.TAG_CONTEXT_TYPE,
					contextType);
			evidence_record_json.put("Multuple_Items",
					multiItemFunctionality);
			evidence_record_json.put("Number_Of_Items", numberOfItems);
			evidence_record_json.put("Partial_Entries_Allowed",
					getPartialEntriesSelection(context,menuSelection));
			evidence_record_json.put("Number_Of_Evidences_In_Each_Item",
					getNumberOfEvidences(context,menuSelection));
			evidence_record_json.put(Utilities.TAG_EVIDENCE_IDENTIFIER,
					getEvidenceIdentifier(context,menuSelection));
			evidence_record_json.put(Utilities.TAG_LOCATION, getEvidenceLocation(context,menuSelection));
			
			JSONObject location_entries = new JSONObject();
			location_entries.put(Utilities.TAG_ENTRY1,"latitude");

			location_entries.put(Utilities.TAG_ENTRY2,"longtitude");
			location_entries.put(Utilities.TAG_ENTRY3,
					"null");
			location_entries.put(Utilities.TAG_ENTRY4,
					"null");
			location_entries.put(Utilities.TAG_ENTRY5,
					"null");

			evidence_record_json.put(Utilities.TAG_LOCATION_ENTRIES,
					location_entries);

			evidence_record_json.put(Utilities.TAG_ASSET_XXX, Utilities.loadPreferences(context, Utilities.TAG_USERNAME));
			evidence_record_json.put(Utilities.TAG_ASSET_YYY, "null");
			evidence_record_json.put(Utilities.TAG_ASSET_ZZZ, "null");
			//These details need to be available from config_app.json -- June 24
			evidence_record_json.put(Utilities.TAG_CUSTOMER_ID, Utilities.loadPreferences(context, Utilities.TAG_CUSTOMER_ID));
			evidence_record_json.put(Utilities.TAG_ASSET_ID, "asset_id_123");
			evidence_record_json.put(Utilities.TAG_SE_ID, Utilities.loadPreferences(context, Utilities.TAG_SE_ID));
			evidence_record_json.put(Utilities.TAG_DBNAME, Utilities.loadPreferences(context, Utilities.TAG_DBNAME));
			//
			
			JSONObject specific_details = getMetaJsonSpecificDetails(menuSelection);

			JSONObject evidences = getMetaJsonEvidences(menuSelection);
			
			JSONObject evidence_qualifiers = getMetaJsonEvidenceQualifiers(menuSelection);
			
			JSONArray items = new JSONArray();
			items.put(specific_details);
			items.put(evidences);
			items.put(evidence_qualifiers);
			evidence_record_json.put(Utilities.TAG_ITEMS, items);
			
			JSONObject capturing_device_details = getDeviceDetails(context);
			evidence_record_json.put(Utilities.TAG_CAPTURING_DEVICE_DETAILS, capturing_device_details);
			
			JSONObject metaJson = new JSONObject();
			metaJson.put(Utilities.TAG_EVIDENCE_RECORD, evidence_record_json);

			evidence_report.put(evidence_record_json);

			File imagesFolder = Utilities.getDir(customerId, evidenceIdentifierId);
			if (!imagesFolder.exists())
				imagesFolder.mkdirs();
			/*
			 * String detailsJson = Utilities.loadPreferences(context,
			 * Utilities.TAG_WHOLE_JSONArray);
			 */

			File jsonFileName = new File(imagesFolder, fileName);
			// File servicedetailsJson
			// =openFileOutput("servicenotesdetails.json",
			// Context.MODE_PRIVATE);
			if (jsonFileName.exists()) {
				try {
					FileOutputStream fOut = new FileOutputStream(jsonFileName);
					OutputStreamWriter myOutWriter = new OutputStreamWriter(
							fOut);
					//myOutWriter.append(evidence_report.toString());
					//Writing the json object output with Utilities.TAG_EVIDENCE_RECORD -- Mar 26
					myOutWriter.append(metaJson.toString());
					myOutWriter.close();
					fOut.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			else {
				FileWriter file = new FileWriter(jsonFileName);
				file.write(metaJson.toString());
				file.flush();
				file.close();
			}

		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	public static int getDynamicImage(Context context, String fName,
			ImageView imageView, String fNamePattern) {
		String imageFilename = fName; // this is image file name
		String PACKAGE_NAME = context.getPackageName();
		int imgId = context.getResources().getIdentifier(
				PACKAGE_NAME + ":drawable/" + imageFilename, null, null);
		System.out.println("IMG ID :: " + imgId);
		System.out.println("PACKAGE_NAME :: " + PACKAGE_NAME);
		// Bitmap bitmap = BitmapFactory.decodeResource(getResources(),imgId);
		if (imgId != 0) {
			imageView.setImageBitmap(BitmapFactory.decodeResource(
					context.getResources(), imgId));
		} else {
			if (fNamePattern == null) {
				imageView.setTag(R.drawable.upload);
				imageView.setImageResource(R.drawable.upload);
			} else {
				imageView.setTag(R.drawable.upload_100);
				imageView.setImageResource(R.drawable.upload_100);
			}

		}
		return imgId;
	}

	public static int getTotalNumberOfRecordsFromAssetJson(Context context) {
		int totalRecords = 0;
		JSONObject obj;
		JSONArray m_jArry;
		String fileNames[] = { "powertools.json", "othertools.json",
				"spanner.json", "multipleitems.json" };
		for (int i = 0; i < 4; i++) {
			try {
				obj = new JSONObject(Utilities.loadFromAsset(context,
						fileNames[i]));
				if (obj != null) {
					m_jArry = obj.getJSONArray(Utilities.TAG_CATALOGUE);
					totalRecords += m_jArry.length();
				}

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				// Utilities.alertDialogDisplay(context, "jsonexception",
				// e.getMessage());
			}
		}
		return totalRecords;
	}

	public static int getTotalNumberOfRecordsFromAssetJson(Context context,
			int count, ArrayList fileNames) {
		int totalRecords = 0;
		JSONObject obj;
		JSONArray m_jArry;
		String jsonArraySplitter = "";
		for (int i = 0; i < count; i++) {
			try {
				obj = new JSONObject(Utilities.loadFromAsset(context, fileNames
						.get(i).toString()));
				if (obj != null) {
					if (count == 5) {
						jsonArraySplitter = Utilities.TAG_QUESTIONBANK;
					}
					if (count == 1) {
						jsonArraySplitter = Utilities.TAG_CATALOGUE;
					}
					m_jArry = obj.getJSONArray(jsonArraySplitter);
					totalRecords += m_jArry.length();
				}

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				// Utilities.alertDialogDisplay(context, "jsonexception",
				// e.getMessage());
			}
		}
		return totalRecords;
	}

	/*
	 * public static void setNextFlow(Context context) { int currentRecNo =
	 * Integer
	 * .parseInt(Utilities.LoadPreferences(context,Utilities.TAG_CATALOGUERECORDNO
	 * )); int specificTotalRecord =
	 * Integer.parseInt(Utilities.LoadPreferences(context,
	 * Utilities.TAG_CATALOGUESPECIFICTOTALRECORDS)); int categoryChosen =
	 * Integer.parseInt(Utilities.LoadPreferences(context ,
	 * Utilities.TAG_CHOSEN_CATEGORIES));
	 * 
	 * if (currentRecNo < specificTotalRecord - 1) { //go to next record of the
	 * series currentRecNo = (currentRecNo + 1) % specificTotalRecord;
	 * Utilities.SavePreferences(Utilities.TAG_CATALOGUERECORDNO,
	 * String.valueOf(currentRecNo), context); Utilities.prepareJson(context);
	 * Intent i = new Intent(context, EmsCatalogueActivity.class);
	 * i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
	 * Intent.FLAG_ACTIVITY_MULTIPLE_TASK); context.startActivity(i); } else if
	 * (categoryChosen < 4 ) { Utilities.prepareJson(context); Intent nextFlow =
	 * new Intent(context, EmsCatalogueToolBoxActivity.class);
	 * nextFlow.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
	 * Intent.FLAG_ACTIVITY_MULTIPLE_TASK); context.startActivity(nextFlow); }
	 * else { Utilities.writeToCloud(context);
	 * Utilities.alertDialogDisplay(context, "status", "File is sent"); } }
	 */

	public static void addEditTextDynamically(Context context,
			LinearLayout questionLayout, LinearLayout answerLayout,
			String questionaireType, JSONObject jsonObject) {
		float height = getHeight(context);

		addTextView(context, questionLayout, jsonObject);

		EditText editText1 = new EditText(context);

		LayoutParams layoutParams1 = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.FILL_PARENT, (int) (height / 4));
		layoutParams1.gravity = Gravity.LEFT;
		layoutParams1.setMargins(0, 10, 10, 10); // (left, top, right, bottom)

		editText1.setLayoutParams(layoutParams1);

		editText1.setBackgroundColor(context.getResources().getColor(
				R.color.grey_color));
		editText1.setTextColor(context.getResources().getColor(
				R.color.black_color));
		// editText1.setPadding(20, 20, 300, 20);// in pixels (left,
		// top, right,
		// bottom)

		editText1.setSingleLine(false);
		editText1.setHorizontallyScrolling(false);
		editText1.setTextColor(context.getResources().getColor(
				R.color.black_color));
		answerLayout.addView(editText1);

	}

	public static void addTextView(Context context,
			LinearLayout questionLayout, JSONObject jsonObject) {
		TextView textView2 = new TextView(context);
		LayoutParams layoutParams2 = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.FILL_PARENT,
				LinearLayout.LayoutParams.WRAP_CONTENT);
		LayoutParams layoutParams = new LayoutParams(layoutParams2);
		layoutParams.gravity = Gravity.LEFT;
		layoutParams.setMargins(0, 10, 10, 10); // (left, top, right, bottom)
		textView2.setLayoutParams(layoutParams);
		try {
			textView2.setText(jsonObject.getString(Utilities.TAG_QUESTIONNAME));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		textView2.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
		textView2.setTextColor(context.getResources().getColor(
				R.color.black_color));
		// textView2.setBackgroundColor(0xffffdbdb); // hex color 0xAARRGGBB
		// textView1.setPadding(20, 20, 20, 20);// in pixels (left, top, right,
		// bottom)
		textView2.setSingleLine(false);

		questionLayout.addView(textView2);
	}

	public static void addRadiosDynamically(Context context,
			LinearLayout questionLayout, ScrollView scrollView,
			JSONObject jsonObject) {

		addTextView(context, questionLayout, jsonObject);
		RadioGroup.LayoutParams rprms;
		RadioGroup answerStatus = new RadioGroup(context);
		/*
		 * answerStatus.setBackgroundColor(context.getResources()
		 * .getColor(R.color.grey_color));
		 */
		int index = 1, count = 5;
		for (index = 1; index < count; index++) {
			RadioButton rdbtn = new RadioButton(context);
			rdbtn.setId(index);
			try {
				switch (index) {
				case 1:
					rdbtn.setText(jsonObject.getString("Answer1"));
					break;
				case 2:
					rdbtn.setText(jsonObject.getString("Answer2"));
					break;
				case 3:
					rdbtn.setText(jsonObject.getString("Answer3"));
					break;
				case 4:
					rdbtn.setText(jsonObject.getString("Answer4"));
					break;
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			rdbtn.setTextColor(context.getResources().getColor(
					R.color.black_color));

			rprms = new RadioGroup.LayoutParams(
					LinearLayout.LayoutParams.FILL_PARENT,
					LinearLayout.LayoutParams.WRAP_CONTENT);

			if (index == 1) {
				rdbtn.setChecked(true);

			}
			answerStatus.addView(rdbtn, rprms);

		}
		scrollView.addView(answerStatus);

	}

	public static void addRadiosDynamically(Activity activity, Context context,
			LinearLayout layout, ScrollView scrollView, RadioGroup status,
			ArrayList radioList, String flow) {

		RadioGroup.LayoutParams rprms;
		String alreadyCheckedState = "";
		int checkedNumber = 0;

		for (int i = 0; i < radioList.size() ; i++) {

			String option = radioList.get(i).toString();
			RadioButton rdbtn = new RadioButton(context);
			rdbtn.setId(i);
			rdbtn.setText(option);
			int screenSize = getScreenSize(context);
			if (screenSize <= GALAXY_S3_SCREEN_SIZE) {
				rdbtn.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16.0f);
				
			}
			else {
				rdbtn.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20.0f);
			}
			if(i == 0)
			{
				rdbtn.setButtonDrawable(R.drawable.orange_radio);				
			}
			else
			{
				rdbtn.setButtonDrawable(R.drawable.white_radio);				
			}
			rdbtn.setTextColor(context.getResources().getColor(
					R.color.black_color));
			//rdbtn.setBackgroundColor(context.getResources().getColor(R.color.green_color)); //set the radio button color to be in green -- June 15

			rprms = new RadioGroup.LayoutParams(
					LinearLayout.LayoutParams.FILL_PARENT,
					LinearLayout.LayoutParams.WRAP_CONTENT);
			if (flow.equalsIgnoreCase("Reasons")) {
				alreadyCheckedState = Utilities.loadPreferences(context,
						Utilities.TAG_REASONS_NUMBER);
			}
			if (flow.equalsIgnoreCase("Machine_downtime")) {
				alreadyCheckedState = Utilities.loadPreferences(context,
						Utilities.TAG_MACHINE_DOWNTIME_NUMBER);
			}
			if (alreadyCheckedState.equalsIgnoreCase("")) {
				checkedNumber = 0;
			} else {
				checkedNumber = Integer.parseInt(alreadyCheckedState);
			}
			if (i == checkedNumber) {
				rdbtn.setChecked(true);
				String result = rdbtn.getText().toString();

				Utilities.savePreferences(Utilities.TAG_OPTION_RESULT, result,
						context);
				/*
				 * Utilities.savePreferences(Utilities.TAG_REASONS, result,
				 * context);
				 */
			}
			status.addView(rdbtn, rprms);
		}

	}

	public static void addCheckboxDynamically(Context context,
			LinearLayout questionLayout, LinearLayout answerLayout,
			JSONObject jsonObject) {

		addTextView(context, questionLayout, jsonObject);
		float height = getHeight(context);
		LayoutParams layoutParams1 = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.FILL_PARENT,
				LinearLayout.LayoutParams.WRAP_CONTENT);
		int index = 1, count = 5;
		for (index = 1; index < count; index++) {
			CheckBox checkBox = new CheckBox(context);
			checkBox.setId(index);
			try {
				switch (index) {
				case 1:
					checkBox.setText(jsonObject.getString("Answer1"));
					break;
				case 2:
					checkBox.setText(jsonObject.getString("Answer2"));
					break;
				case 3:
					checkBox.setText(jsonObject.getString("Answer3"));
					break;
				case 4:
					checkBox.setText(jsonObject.getString("Answer4"));
					break;
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			checkBox.setTextColor(context.getResources().getColor(
					R.color.black_color));

			/*
			 * rprms = new RadioGroup.LayoutParams(
			 * LinearLayout.LayoutParams.FILL_PARENT
			 * ,LinearLayout.LayoutParams.WRAP_CONTENT);
			 */

			if (index == 1) {
				checkBox.setChecked(true);

			}
			checkBox.setLayoutParams(layoutParams1);

			answerLayout.addView(checkBox);

		}

	}

	public static void addCheckboxDynamically(Context context,
			LinearLayout genericLayout, ArrayList checkboxList) {

		LayoutParams layoutParams1 = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.FILL_PARENT,
				LinearLayout.LayoutParams.WRAP_CONTENT);
		for (int index = 0; index < checkboxList.size(); index++) {
			CheckBox checkBox = new CheckBox(context);
			checkBox.setId(index);
			if(index == 0)
			{
				checkBox.setButtonDrawable(R.drawable.orange_check);				
			}
			else
			{
				checkBox.setButtonDrawable(R.drawable.white_check);				
			}
			
			String option = checkboxList.get(index).toString();
			// checkBox.setLeft(100);
			checkBox.setText(option);
			int screenSize = getScreenSize(context);
			if (screenSize <= GALAXY_S3_SCREEN_SIZE)
				checkBox.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16.0f);
			else
			checkBox.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20.0f);
			checkBox.setTextColor(context.getResources().getColor(
					R.color.black_color));

			if (index == 0) {
				checkBox.setChecked(true);
				addedActionsList.add(option);
				Utilities.savePreferences(Utilities.TAG_ACTION + index,
						addedActionsList.get(index).toString(), context);
				// Utilities.savePreferences(Utilities.TAG_ACTION_COUNT,
				// String.valueOf(addedActionsList.size()), context);
			}
			// checkBox.setLayoutParams(layoutParams1);

			checkBox.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {

					if (((CheckBox) v).isChecked()) {
						// Utilities.alertDialogDisplay(v.getContext(),
						// "checkbox", ((CheckBox) v).getText().toString());
						((CheckBox) v).setButtonDrawable(R.drawable.orange_check);
						addedActionsList.add(((CheckBox) v).getText()
								.toString());
					} else {
						addedActionsList.remove(((CheckBox) v).getText()
								.toString());
						((CheckBox) v).setButtonDrawable(R.drawable.white_check);
					}
					for (int i = 0; i < addedActionsList.size(); i++) {
						Utilities.savePreferences(Utilities.TAG_ACTION + i,
								addedActionsList.get(i).toString(),
								v.getContext());
					}
					Utilities.savePreferences(Utilities.TAG_ACTION_COUNT,
							String.valueOf(addedActionsList.size()),
							v.getContext());

				}

			});

			genericLayout.addView(checkBox);

		}

	}

	private static int getHeight(Context context) {
		DisplayMetrics metrics = new DisplayMetrics();
		WindowManager wm = (WindowManager) context
				.getSystemService(Context.WINDOW_SERVICE);
		wm.getDefaultDisplay().getMetrics(metrics);
		return metrics.heightPixels;
	}
	
	private static int getWidth(Context context) {
		DisplayMetrics metrics = new DisplayMetrics();
		WindowManager wm = (WindowManager) context
				.getSystemService(Context.WINDOW_SERVICE);
		wm.getDefaultDisplay().getMetrics(metrics);
		return metrics.widthPixels;
	}
	
	private static int getScreenSize(Context context) {
		Configuration config = context.getResources().getConfiguration();
	    int screenSize = config.screenLayout & config.SCREENLAYOUT_SIZE_MASK;
	    return screenSize;
	}
	


	public static void saveCatalogueDetails(JSONObject jsonObject,
			Context context, JSONArray jsonArray, String isItemAdded, String isItemDeleted) {
		String fromFlow = Utilities.loadPreferences(context, "submenu");
		try {
			JSONObject specific_details = new JSONObject();
			JSONObject evidences = new JSONObject();
			JSONObject evidence_qualifiers = new JSONObject();
			if (fromFlow.equalsIgnoreCase("create toolbox") ||
					fromFlow.equalsIgnoreCase("create equipment")	 ||
					isItemAdded.equalsIgnoreCase("yes")) {
			specific_details.put(Utilities.TAG_A,
					jsonObject.getString(Utilities.TAG_ARTICLENO));
			specific_details.put(Utilities.TAG_B,
					jsonObject.getString(Utilities.TAG_ARTICLENAME));
			specific_details.put(Utilities.TAG_C,
					jsonObject.getString(Utilities.TAG_PRICE));
			specific_details.put(Utilities.TAG_D,
					jsonObject.getString(Utilities.TAG_INCHARGE));
			
			evidences.put(Utilities.TAG_EVIDENCE_1,
					"pic_100_" + jsonObject.getString(Utilities.TAG_ARTICLENO));  //Added picture name in evidence_1 -- June 1
			
			evidences.put(Utilities.TAG_EVIDENCE_4,
					jsonObject.getString(Utilities.TAG_SCANNEDSTATUS));
			
			evidence_qualifiers.put(Utilities.TAG_QUALIFIER_8,
					jsonObject.getString(Utilities.TAG_CATEGORY).toLowerCase());
			}
			else { //from check inventory or list equipment
				specific_details.put(Utilities.TAG_A,
						jsonObject.getString(Utilities.TAG_A));
				specific_details.put(Utilities.TAG_B,
						jsonObject.getString(Utilities.TAG_B));
				specific_details.put(Utilities.TAG_C,
						jsonObject.getString(Utilities.TAG_C));
				specific_details.put(Utilities.TAG_D,
						jsonObject.getString(Utilities.TAG_D));
				
				evidences.put(Utilities.TAG_EVIDENCE_1,
						"pic_100_" + jsonObject.getString(Utilities.TAG_A));  //Added picture name in evidence_1 -- June 1
				
				evidences.put(Utilities.TAG_EVIDENCE_4,
						jsonObject.getString(Utilities.TAG_EVIDENCE_4));
				
				evidence_qualifiers.put(Utilities.TAG_QUALIFIER_8,
						jsonObject.getString(Utilities.TAG_QUALIFIER_8).toLowerCase());
			}
			


			

			
			String evidence_choice = Utilities.loadPreferences(context, "miss_or_service");
			if (evidence_choice.equalsIgnoreCase("Service Notes")) {
				evidences.put(Utilities.TAG_EVIDENCE_2,
					Utilities.loadPreferences(context, context.getResources()
							.getString(R.string.servicenotes)));
				evidences.put(Utilities.TAG_EVIDENCE_3,
						"null");
			}
			if (evidence_choice.equalsIgnoreCase("Missing Notes")) {
				evidences.put(Utilities.TAG_EVIDENCE_2,
						"null");
				evidences.put(Utilities.TAG_EVIDENCE_3,
						Utilities.loadPreferences(context, context.getResources()
								.getString(R.string.servicenotes)));
			}
			if (evidence_choice.equalsIgnoreCase("")) {
				evidences.put(Utilities.TAG_EVIDENCE_2,
						"null");
				evidences.put(Utilities.TAG_EVIDENCE_3,
						"null");
			}
			
			if (isItemAdded.equalsIgnoreCase("yes")) {
				String addedItemCount = Utilities.loadPreferences(context, Utilities.TAG_ADDEDCOUNT);
				if (addedItemCount.equalsIgnoreCase("")) {
					addedItemCount = "1";
				}
				else {
					addedItemCount = String.valueOf(Integer.parseInt(addedItemCount) + 1);
				}
				Utilities.savePreferences(Utilities.TAG_ADDEDCOUNT, addedItemCount, context);
				evidences.put(Utilities.TAG_EVIDENCE_5,
					"Added");
			}
			else {
				evidences.put(Utilities.TAG_EVIDENCE_5,
						"null");
			}
			if (isItemDeleted.equalsIgnoreCase("yes")) {
				evidences.put(Utilities.TAG_EVIDENCE_6,
					"Deleted");
			}
			else {
				evidences.put(Utilities.TAG_EVIDENCE_6,
						"null");
			}
			
			evidences.put(Utilities.TAG_EVIDENCE_7,
					"null");
			evidences.put(Utilities.TAG_EVIDENCE_8,
					"null");
			evidences.put(Utilities.TAG_EVIDENCE_9,
					"null");
			evidences.put(Utilities.TAG_EVIDENCE_10,
					"null");
			

			

			if (evidence_choice.equalsIgnoreCase("Service Notes")) {
			evidence_qualifiers.put(Utilities.TAG_QUALIFIER_1,
					Utilities.loadPreferences(context, context.getResources()
							.getString(R.string.servicenotescolor)));
			evidence_qualifiers.put(Utilities.TAG_QUALIFIER_2,
					"null");
			}
			if (evidence_choice.equalsIgnoreCase("Missing Notes")) {
				evidence_qualifiers.put(Utilities.TAG_QUALIFIER_1,
						"null");
				evidence_qualifiers.put(Utilities.TAG_QUALIFIER_2,
						Utilities.loadPreferences(context, context.getResources()
								.getString(R.string.servicenotescolor)));
			}
			if (evidence_choice.equalsIgnoreCase("")) {
				evidence_qualifiers.put(Utilities.TAG_QUALIFIER_1,
						"null");
				evidence_qualifiers.put(Utilities.TAG_QUALIFIER_2,
						"null");
			}
			evidence_qualifiers.put(Utilities.TAG_QUALIFIER_3,
					"null");
			evidence_qualifiers.put(Utilities.TAG_QUALIFIER_4,
					"null");
			evidence_qualifiers.put(Utilities.TAG_QUALIFIER_5,
					"null");
			evidence_qualifiers.put(Utilities.TAG_QUALIFIER_6,
					"null");
			evidence_qualifiers.put(Utilities.TAG_QUALIFIER_7,
					"null");
			
			evidence_qualifiers.put(Utilities.TAG_QUALIFIER_9,
					"null");
			evidence_qualifiers.put(Utilities.TAG_QUALIFIER_10,
					"null");
			
			if (jsonArray != null) {
				JSONObject itemsJsonObject = new JSONObject();
				itemsJsonObject.put(Utilities.TAG_SPECIFIC_DETAILS, specific_details);
				itemsJsonObject.put(Utilities.TAG_EVIDENCES, evidences);
				itemsJsonObject.put(Utilities.TAG_EVIDENCE_QUALIFIERS, evidence_qualifiers);
				jsonArray.put(jsonArray.length(), itemsJsonObject);
			}
			else {
				jsonArray = new JSONArray();
				JSONObject itemsJsonObject = new JSONObject();
				itemsJsonObject.put(Utilities.TAG_SPECIFIC_DETAILS, specific_details);
				itemsJsonObject.put(Utilities.TAG_EVIDENCES, evidences);
				itemsJsonObject.put(Utilities.TAG_EVIDENCE_QUALIFIERS, evidence_qualifiers);
				jsonArray.put(0, itemsJsonObject);
			}
			Utilities.savePreferences(Utilities.TAG_ITEMS,
					jsonArray.toString(), context);
			


		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void saveSiteVisitDetails(Context context,
			JSONArray jsonArray) {
		try {
			JSONObject specific_details = new JSONObject();

			specific_details.put(Utilities.TAG_A,
					"null");
			specific_details.put(Utilities.TAG_B,
					"null");
			specific_details.put(Utilities.TAG_C,
					"null");
			specific_details.put(Utilities.TAG_D,
					"null");
			


			JSONObject evidences = new JSONObject();

			evidences.put(Utilities.TAG_EVIDENCE_1,
					"Image_Directory_Location");  
			String evidence_choice = Utilities.loadPreferences(context, "miss_or_service");
			if (evidence_choice.equalsIgnoreCase("Service Notes")) {
				evidences.put(Utilities.TAG_EVIDENCE_2,
					Utilities.loadPreferences(context, context.getResources()
							.getString(R.string.servicenotes)));
				evidences.put(Utilities.TAG_EVIDENCE_3,
						"null");
			}
			if (evidence_choice.equalsIgnoreCase("Missing Notes")) {
				evidences.put(Utilities.TAG_EVIDENCE_2,
						"null");
				evidences.put(Utilities.TAG_EVIDENCE_3,
						Utilities.loadPreferences(context, context.getResources()
								.getString(R.string.servicenotes)));
			}
			if (evidence_choice.equalsIgnoreCase("")) {
				evidences.put(Utilities.TAG_EVIDENCE_2,
						"null");
				evidences.put(Utilities.TAG_EVIDENCE_3,
						"null");
			}
			if (!Utilities.loadPreferences(context, Utilities.TAG_OPTION_RESULT).equalsIgnoreCase("")) {
			evidences.put(Utilities.TAG_EVIDENCE_4,
					Utilities.loadPreferences(context, Utilities.TAG_OPTION_RESULT));
			}
			else {
				evidences.put(Utilities.TAG_EVIDENCE_4,
						"null");
			}
			evidences.put(Utilities.TAG_EVIDENCE_5,
					"null");
			evidences.put(Utilities.TAG_EVIDENCE_6,
					"null");
			evidences.put(Utilities.TAG_EVIDENCE_7,
					"null");
			evidences.put(Utilities.TAG_EVIDENCE_8,
					"null");
			evidences.put(Utilities.TAG_EVIDENCE_9,
					"null");
			evidences.put(Utilities.TAG_EVIDENCE_10,
					"null");
			

			JSONObject evidence_qualifiers = new JSONObject();

			if (evidence_choice.equalsIgnoreCase("Service Notes")) {
			evidence_qualifiers.put(Utilities.TAG_QUALIFIER_1,
					Utilities.loadPreferences(context, context.getResources()
							.getString(R.string.servicenotescolor)));
			evidence_qualifiers.put(Utilities.TAG_QUALIFIER_2,
					"null");
			}
			if (evidence_choice.equalsIgnoreCase("Missing Notes")) {
				evidence_qualifiers.put(Utilities.TAG_QUALIFIER_1,
						"null");
				evidence_qualifiers.put(Utilities.TAG_QUALIFIER_2,
						Utilities.loadPreferences(context, context.getResources()
								.getString(R.string.servicenotescolor)));
			}
			if (evidence_choice.equalsIgnoreCase("")) {
				evidence_qualifiers.put(Utilities.TAG_QUALIFIER_1,
						"null");
				evidence_qualifiers.put(Utilities.TAG_QUALIFIER_2,
						"null");
			}
			evidence_qualifiers.put(Utilities.TAG_QUALIFIER_3,
					"null");
			evidence_qualifiers.put(Utilities.TAG_QUALIFIER_4,
					"null");
			evidence_qualifiers.put(Utilities.TAG_QUALIFIER_5,
					"null");
			evidence_qualifiers.put(Utilities.TAG_QUALIFIER_6,
					"null");
			evidence_qualifiers.put(Utilities.TAG_QUALIFIER_7,
					"null");
			evidence_qualifiers.put(Utilities.TAG_QUALIFIER_8,
					"null");
			evidence_qualifiers.put(Utilities.TAG_QUALIFIER_9,
					"null");
			evidence_qualifiers.put(Utilities.TAG_QUALIFIER_10,
					"null");
			
			if (jsonArray != null) {
				JSONObject itemsJsonObject = new JSONObject();
				itemsJsonObject.put(Utilities.TAG_SPECIFIC_DETAILS, specific_details);
				itemsJsonObject.put(Utilities.TAG_EVIDENCES, evidences);
				itemsJsonObject.put(Utilities.TAG_EVIDENCE_QUALIFIERS, evidence_qualifiers);
				jsonArray.put(jsonArray.length(), itemsJsonObject);
			}
			else {
				jsonArray = new JSONArray();
				JSONObject itemsJsonObject = new JSONObject();
				itemsJsonObject.put(Utilities.TAG_SPECIFIC_DETAILS, specific_details);
				itemsJsonObject.put(Utilities.TAG_EVIDENCES, evidences);
				itemsJsonObject.put(Utilities.TAG_EVIDENCE_QUALIFIERS, evidence_qualifiers);
				jsonArray.put(0, itemsJsonObject);
			}
			Utilities.savePreferences(Utilities.TAG_ITEMS,
					jsonArray.toString(), context);
			

/*	 This may not be used as we have moved things inside specific details		
 * -- Mar 26
			Utilities.savePreferences(Utilities.TAG_ARTICLENO,
					jsonObject.getString(Utilities.TAG_ARTICLENO), context);

			Utilities.savePreferences(Utilities.TAG_ARTICLENAME,
					jsonObject.getString(Utilities.TAG_ARTICLENAME), context);
			Utilities.savePreferences(Utilities.TAG_PRICE,
					jsonObject.getString(Utilities.TAG_PRICE), context);
			Utilities.savePreferences(Utilities.TAG_INCHARGE,
					jsonObject.getString(Utilities.TAG_INCHARGE), context);*/
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void saveCatalogueDetails(JSONObject jsonObject,
			 Context context, String newItem) {
		
		try {
			String addedItemCount = Utilities.loadPreferences(context, Utilities.TAG_ADDEDCOUNT);
			if (addedItemCount.equalsIgnoreCase("")) {
				addedItemCount = "1";
			}
			else {
				addedItemCount = String.valueOf(Integer.parseInt(addedItemCount) + 1);
			}
			Utilities.savePreferences(Utilities.TAG_ADDEDCOUNT, addedItemCount, context);
			JSONArray catalogueSpecificRecords = new JSONArray(
					Utilities.loadPreferences(context,
							Utilities.TAG_CATALOGUESPECIFICRECORDS));
			int screenX = (catalogueSpecificRecords.length() + 1) / 4;
			int positionY = (catalogueSpecificRecords.length() + 1) % 4;
			if (positionY == 0) {
				screenX += 1;
				positionY = 1;
			}
			jsonObject.put("Antal", "1");
			jsonObject.put("Deltotal", "");  // We need to fill value or we need to get from form -- mar 23
			jsonObject.put("Incharge", "Jula"); //If not Jula, we need to get from form
			jsonObject.put(Utilities.TAG_TOOLBOX, "1");  //If not 1, we need to get from form
			jsonObject.put(Utilities.TAG_SCREENX, String.valueOf(screenX));  //If not 1, we need to get from form
			jsonObject.put(Utilities.TAG_POSITIONY, String.valueOf(positionY));  //If not 1, we need to get from form
			jsonObject.put(Utilities.TAG_SCANNEDSTATUS, "unscanned");  //If not 1, we need to get from form
			jsonObject.put(Utilities.TAG_DELETEDSTATUS, "notdeleted");  //If not 1, we need to get from form
			jsonObject.put(Utilities.TAG_ADDEDSTATUS, "notadded");  //If not 1, we need to get from form
			catalogueSpecificRecords.put(jsonObject);
			Utilities.savePreferences(Utilities.TAG_CATALOGUESPECIFICRECORDS, catalogueSpecificRecords.toString(), context);
			
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
/*		Utilities.savePreferences(Utilities.TAG_ARTICLENO, articleNo, context);

		Utilities.savePreferences(Utilities.TAG_ARTICLENAME, articleName,
				context);
		Utilities.savePreferences(Utilities.TAG_PRICE, price, context);*/

	}
	
	//Applicable only for rotating assets
	
	public static void resetCatalogueCountValues(Context context) {
		if (Utilities.loadPreferences(context, "submenu").equalsIgnoreCase("create toolbox") ||
				Utilities.loadPreferences(context, "submenu").equalsIgnoreCase("create equipment")	) {
			EmsCatalogueMultimageItemsActivityCreateToolBox.index = 0;
			EmsCatalogueMultimageItemsActivityCreateToolBox.itemsScanned = 0;
			EmsCatalogueMultimageItemsActivityCreateToolBox.noImageCount = 0;
			EmsCatalogueMultimageItemsActivityCreateToolBox.swipedCount = 0;			
		}
		else {
		EmsCatalogueMultimageItemsActivity.index = 0;
		EmsCatalogueMultimageItemsActivity.itemsScanned = 0;
		EmsCatalogueMultimageItemsActivity.missingCount = 0;
		EmsCatalogueMultimageItemsActivity.serviceCount = 0;
		EmsCatalogueMultimageItemsActivity.noImageCount = 0;
		EmsCatalogueMultimageItemsActivity.swipedCount = 0;
		}
		Utilities.removePreference(Utilities.TAG_CHOSEN_CATEGORIES, context);
		Utilities.removePreference(Utilities.TAG_CATALOGUESCANNEDITEMCOUNT, context);
		Utilities.removePreference("missing", context);
		Utilities.removePreference("service", context);
		Utilities.removePreference("noimage", context);
		Utilities.removePreference(context.getResources().getString(R.string.toolbaxarray), context);
		Utilities.savePreferences(Utilities.TAG_ADDEDCOUNT, "", context); //As user not submitting and pressing back key, we should have the added count to be started freshly -- June 18
	}
	
	public static void resetSiteRoutineCountValues(Context context) {
		CustomDialogClass.missingCount = 0;
		CustomDialogClass.serviceCount = 0;
		CustomDialogClass.imageCount = 0;
		EmsBaseAdapter.positionCount = 0;
		
		Utilities.removePreference("missing", context);
		Utilities.removePreference("service", context);
		Utilities.removePreference("noimage", context);		
	}
	
	@SuppressLint("NewApi")
	public static void createAndShowAlertDialog(Context context) {
		final AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setTitle("Do you want to reset flow?");
		final String menuSelection = Utilities.loadPreferences(builder.getContext(), Utilities.TAG_MENUSELECTION);
		builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
			@SuppressLint("NewApi")
			public void onClick(DialogInterface dialog, int id) {
				dialog.dismiss();
				Intent nextScreen = new Intent();
				if (menuSelection.equalsIgnoreCase("4") ||
						(menuSelection.equalsIgnoreCase("1") && Utilities.loadPreferences(builder.getContext(), "submenu").equalsIgnoreCase("toolbox")) ) {							
						 

				Utilities.resetCatalogueCountValues(builder.getContext());
				nextScreen = new Intent(builder.getContext(),
						EmsGroupsActivity.class);
				Utilities.savePreferences("submenu", "btnMenu3", builder.getContext());
				}
				if (menuSelection.equalsIgnoreCase("1") 
						&& Utilities.loadPreferences(builder.getContext(), "submenu").equalsIgnoreCase("btnMenu1")) {
					resetSiteRoutineCountValues(builder.getContext());
					nextScreen = new Intent(builder.getContext(),EmsSiteVisitItemsActivity.class);		        
				}
				if (menuSelection.equalsIgnoreCase("2")) {
            		Utilities.savePreferences("from camera back", "",
            				builder.getContext());//As back button is pressed, we should have "" for  this
            		Utilities.savePreferences(Utilities.TAG_REASONS_NUMBER,
            				"", builder.getContext());//As back button is pressed, we should have "" for  this
            		if (Utilities.loadPreferences(builder.getContext(), "nfc_feature").equalsIgnoreCase("false")) {
					nextScreen = new Intent(builder.getContext(),
							EmsIdentificationFormMgmtActivity.class);
            		}
            		else {
            			Utilities.savePreferences(Utilities.TAG_IDENTIFIED_REMOTE_UNIT,"", builder.getContext());
	    				Utilities.savePreferences(Utilities. TAG_IDENTIFIED_SUPERVISER_UNIT, "",
	    						 builder.getContext());
            			nextScreen = new Intent(builder.getContext(),
    							EmsNfcReaderActivity.class);
            		}
				}
				builder.getContext().startActivity(nextScreen);
			}
		});
		builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
			}
		});
		AlertDialog dialog = builder.create();
		dialog.show();
	}
	
	@SuppressLint("NewApi")
	public static void createAndShowAlertDialogForSiteVisitRoutine(Context context) {
		final AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setTitle("Choose Action");
		builder.setPositiveButton("Take picture", new DialogInterface.OnClickListener() {
			@SuppressLint("NewApi")
			public void onClick(DialogInterface dialog, int id) {
				dialog.dismiss();
				Intent nextScreen = new Intent();
				nextScreen = new Intent(builder.getContext(),
						EmsUploadImage.class);
				builder.getContext().startActivity(nextScreen);
			}
		});
		builder.setNegativeButton("Write Note", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.dismiss();
				Intent nextScreen = new Intent();
				nextScreen = new Intent(builder.getContext(),
						EmsServiceNotes.class);
				builder.getContext().startActivity(nextScreen);
			}
		});
		builder.setNeutralButton("Choose Part", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.dismiss();
				Utilities.savePreferences(Utilities.TAG_FLOW, "Parts", builder.getContext());
            	Intent i = new Intent(builder.getContext(), EmsGenericFormMgmtActivity.class);
            	builder.getContext().startActivity(i);
			}
		});
		AlertDialog dialog = builder.create();
		dialog.show();
	}
	
	public static JSONArray getItemsArray(Context context) {
		String items = Utilities.loadPreferences(context, Utilities.TAG_ITEMS);
		JSONArray itemsArray = new JSONArray();
		int itemsArrayIndex = 0;
		if (items.equalsIgnoreCase("")) {
			itemsArray = new JSONArray();
			itemsArrayIndex = 0;
		}
		else {
			try {
				itemsArray = new JSONArray(items);
				itemsArrayIndex = itemsArray.length();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		return itemsArray;
	}
    public static ProgressDialog progressDialogDisplay(Context context) {
   	 
   	 ProgressDialog pDialog = new ProgressDialog(context);
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.setMessage("Processing. Please wait");
			pDialog.show();
			return pDialog;
    }
    
    public  static void hideProgressDialog( ProgressDialog pDialog) {
		if (pDialog.isShowing())
			pDialog.hide();
	}
    
	public static  boolean setBackKeyValues(Context context) {
		Intent nextFlow = new Intent();
		if (Utilities.loadPreferences(context, Utilities.TAG_MENUSELECTION).equalsIgnoreCase("4") && 
			//Utilities.loadPreferences(context, "submenu").equalsIgnoreCase("toolbox")) {
				(Utilities.loadPreferences(context, "submenu").equalsIgnoreCase("toolbox") || 
						Utilities.loadPreferences(context, "submenu").equalsIgnoreCase("btnMenu3") ||
						Utilities.loadPreferences(context, "submenu").equalsIgnoreCase("create toolbox") ||
						Utilities.loadPreferences(context, "submenu").equalsIgnoreCase("create equipment") ||
						Utilities.loadPreferences(context, "submenu").equalsIgnoreCase("equipmentbox") )) {
			nextFlow = new Intent(context,
					EmsMenuActivity.class);
			context.startActivity(nextFlow);
			return true;
		}
		//changes to be done for machine management here related to back flow
		if (Utilities.loadPreferences(context, Utilities.TAG_MENUSELECTION).equalsIgnoreCase("2")) {
			if (Utilities.loadPreferences(context, "submenu").equalsIgnoreCase("")) {
				nextFlow = new Intent(context,
						EmsMenuActivity.class);				
			}
			else {
				Utilities.savePreferences("submenu", "",
					context);
				Utilities.savePreferences(Utilities.TAG_IDENTIFIED_REMOTE_UNIT, "",
						context);
				Utilities.savePreferences(Utilities. TAG_IDENTIFIED_SUPERVISER_UNIT, "",
						context);
				nextFlow = new Intent(context,
					EmsGroupsActivity.class);						
			}
				/*Utilities.savePreferences("submenu", "",
					context);
				Utilities.savePreferences(Utilities.TAG_IDENTIFIED_REMOTE_UNIT, "",
						context);
				Utilities.savePreferences(Utilities. TAG_IDENTIFIED_SUPERVISER_UNIT, "",
						context);
				nextFlow = new Intent(context,
					EmsGroupsActivity.class);		*/				
		
//			
			context.startActivity(nextFlow);
			return true;
		}
		else if (Utilities.loadPreferences(context, "submenu")
				.equalsIgnoreCase("btnMenu1") || 
				Utilities.loadPreferences(context, "submenu")
				.equalsIgnoreCase("btnMenu2") ||
				Utilities.loadPreferences(context, "submenu")
				.equalsIgnoreCase("btnMenu3") ||
				Utilities.loadPreferences(context, "submenu")
				.equalsIgnoreCase("btnMenu4") ) {
			Utilities.savePreferences("submenu", "",
					context);
			nextFlow = new Intent(context,
					EmsGroupsActivity.class);
			context.startActivity(nextFlow);
			return true;
			
		}
		if (Utilities.loadPreferences(context, "submenu")
				.equalsIgnoreCase("")) {
			nextFlow = new Intent(context,
					EmsMenuActivity.class);
			context.startActivity(nextFlow);
			return true;
		}
		return true;
		
	}
	
public static void navigateToListView(Context context) {
	JSONArray itemsArray = Utilities.getItemsArray(context);
	
	Utilities.saveSiteVisitDetails(context, itemsArray);
	EmsBaseAdapter.positionCount += 1;
	Intent intent = new Intent(context,EmsSiteVistRoutineItemsActivity.class);
	context.startActivity(intent);
}
public static void redirectMenuFlows(String menuSelection,Context context) {
	Intent nextFlow = new Intent();
	if (menuSelection.equalsIgnoreCase("2")) {
		nextFlow = new Intent(context,
				EmsGroupsActivity.class);
	}
	if (menuSelection.equalsIgnoreCase("4")){
		nextFlow = new Intent(context,
				EmsGroupsActivity.class);	
	}
	if (menuSelection.equalsIgnoreCase("1")){
		nextFlow = new Intent(context,
				EmsGroupsActivity.class);
		Utilities.savePreferences("submenu", "", context);
	}
	if (menuSelection.equalsIgnoreCase("2") || menuSelection.equalsIgnoreCase("4") || menuSelection.equalsIgnoreCase("1")){
		nextFlow.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(nextFlow);
	}
}

public static void makeJsonArryReq(String jsonUrl,Context context, String flow) {
//	final ProgressDialog pDialog = Utilities.progressDialogDisplay(context);
//	final VolleyCallback callBack = volleyCallBack;
	final String typeFlow = flow;
	final String url = jsonUrl;	
	final Context ctx = context;
	final String result = "";
	JsonArrayRequest req = new JsonArrayRequest(url,
			new Response.Listener<JSONArray>() {
				
				public void onResponse(JSONArray response) {
					volleyResult = "";
					StringBuffer sb = new StringBuffer();
					if (response != null && response.length() > 0) {
					System.out.println("response.." + response.length());
					volleyResult = response.toString();
					Intent nextScreen = new Intent();
					if (typeFlow.equalsIgnoreCase("1")) {
						nextScreen = new Intent(ctx,
							EmsIdentificationFormMgmtActivity.class);
					}
					else if (typeFlow.equalsIgnoreCase("createtoolbox")) {
					setCataLogueDetails("inventory.json",ctx);
					nextScreen = new Intent(ctx, EmsTextboxActivity.class);
					}
					else if (typeFlow.equalsIgnoreCase("createequipmentbox")) {
						setCataLogueDetails("inventory_equipment.json",ctx);
						nextScreen = new Intent(ctx, EmsTextboxActivity.class);
					}
					else {
						nextScreen = new Intent(ctx, EmsGroupsSplitActivity.class);
					}
					nextScreen.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
							| Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
					Utilities.savePreferences("volleyresponse", response.toString(),ctx);
					ctx.startActivity(nextScreen);	
					}
					else {
						Toast.makeText(ctx, "No evidences are seen. Create tools or equipments first",
								Toast.LENGTH_LONG).show();
					}
					//callBack.onSuccess(volleyResult);
					//Utilities.alertDialogDisplay(instructionstext1.getContext(), "output from jsp call", sb.toString());
				//	AlertCustomDialogClass alert = new AlertCustomDialogClass(ctx);
				//	Utilities.savePreferences("restHeaderValue", "Output from server",ctx);
				
				//	alert.show();

						
					
				//	Utilities.hideProgressDialog(pDialog);
				}
			}, new Response.ErrorListener() {
				
				public void onErrorResponse(VolleyError error) {
					//Utilities.alertDialogDisplay(instructionstext1.getContext(), "error in call", error.toString());
				//	Utilities.hideProgressDialog(pDialog);


					if (error instanceof NetworkError
							|| error instanceof ClientError
							|| error instanceof ServerError
							|| error instanceof AuthFailureError
							|| error instanceof ParseError) {
							/*Utilities.alertDialogDisplay(ctx, "Error",
								"Server-Side Error: Please Contact Your sys-admin");*/
						Toast.makeText(
								ctx,
								"Server-Side Error. Please contact System administrator"
										, Toast.LENGTH_LONG)
								.show();
						//	AlertCustomDialogClass alert = new AlertCustomDialogClass(activity);
						Utilities.savePreferences("restHeaderValue", "Error",ctx);
						Utilities.savePreferences("restValue", "Server-Side Error: Please Contact Your sys-admin",ctx);
					//	alert.show();
					}

					if (error instanceof NoConnectionError
							|| error instanceof TimeoutError) {
						Toast.makeText(
								ctx,
								"Internet connectivity Error. Please contact System administrator"
										, Toast.LENGTH_LONG)
								.show();
						//	AlertCustomDialogClass alert = new AlertCustomDialogClass(activity);
						Utilities.savePreferences("restHeaderValue", "Error",ctx);
						Utilities.savePreferences("restValue", "Please Check Internet Connectivity",ctx);
				//		alert.show();
						try {
							/*Toast.makeText(instructionstext1.getContext(),
									error.toString() + ". Retrying again",
									Toast.LENGTH_LONG).show();*/
							// cancelling current requests and then retrying
							AppController.getInstance().getRequestQueue()
									.cancelAll(tag_json_arry);
							Thread.sleep(10000);
							makeJsonArryReq(url,ctx, typeFlow);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

					// req.setRetryPolicy(new DefaultRetryPolicy(20 * 10000, 5,
					// DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
					// req.setTag(tag_json_arry);

				
				}
			});
	
	/*AppController.getInstance().addToRequestQueue(req,
			tag_json_obj);*/

	// Adding request to request queue
	req.setRetryPolicy(new DefaultRetryPolicy(20 * 10000, 5,
			DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
	req.setTag(tag_json_arry);
	
	AppController.getInstance().addToRequestQueue(req,
			tag_json_arry);
	

	// Cancelling request
	// ApplicationController.getInstance().getRequestQueue().cancelAll(tag_json_arry);
}
public static String appendParametersInUrl(String jsonUrl,String param1,String param2,String param3) {
	String url = jsonUrl + "?customerDbName="+param1+"&itemCollectionName="+param2;
	if (param3 != null && !param3.equalsIgnoreCase("")) {
		url += "&evidenceIdentifierValue="+param3;
	}
	return url;
}
public static void setCataLogueDetails(String jsonFileName,Context context) {
	// Need to change here
	int jsonGroupCount = 1;
	ArrayList jsonFileNames = new ArrayList();
	jsonFileNames.add(jsonFileName); //Changed on June 22 to support both toolbox/equipment
	int totalRecords = Utilities.getTotalNumberOfRecordsFromAssetJson(
			context, jsonGroupCount, jsonFileNames);
	Utilities.savePreferences(Utilities.TAG_CATALOGUETOTALRECORDS,
			String.valueOf(totalRecords), context);
}
//public static void zipFiles(String[] _files, File zipFile) {
	public static String zipFiles(ArrayList _files, String zipFileName) {
	try {
		BufferedInputStream origin = null;
		
		//FileOutputStream dest = new FileOutputStream(zipFile);
		FileOutputStream dest = new FileOutputStream(zipFileName);
		ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(
				dest));
		byte data[] = new byte[bufferWhileZipping];

		for (int i = 0; i < _files.size(); i++) {
			System.out.println("Compress Adding: " + _files.get(i).toString());
			FileInputStream fi = new FileInputStream(_files.get(i).toString());
			origin = new BufferedInputStream(fi, bufferWhileZipping);

			ZipEntry entry = new ZipEntry(_files.get(i).toString().substring(_files.get(i).toString().lastIndexOf("/") + 1));
			out.putNextEntry(entry);
			int count;

			while ((count = origin.read(data, 0, bufferWhileZipping)) != -1) {
				out.write(data, 0, count);
			}
			origin.close();
		}

		out.close();
	} catch (Exception e) {
		e.printStackTrace();
	}
	return zipFileName;
}
}
