package com.Ponnivi.ems.helper;

import android.content.Context;
import android.content.Intent;

import com.Ponnivi.ems.R;
import com.Ponnivi.ems.activity.EmsGroupsSplitActivity;
import com.Ponnivi.ems.activity.EmsCatalogueToolBoxActivity;
import com.Ponnivi.ems.activity.EmsMissingServiceNotes;
import com.Ponnivi.ems.activity.EmsServiceNotes;

public class SetNextFlow {
	Context context;
	public SetNextFlow(Context context) {
		this.context = context;
	}
	public void getSpecificFlow(int resId) {
		Intent nextFlow = new Intent();
		boolean nextFlowPresent = true;
		switch(resId) {
		case R.id.notok :
			nextFlow = new Intent(context, EmsMissingServiceNotes.class);
			break;
		case R.id.repair:
			nextFlow = new Intent(context, EmsServiceNotes.class);
			break;
		case R.id.ok:
			
			String currentRecNo = Utilities.loadPreferences(context,
					Utilities.TAG_CATALOGUERECORDNO);
			String specificTotalRecord = Utilities.loadPreferences(
					context, Utilities.TAG_CATALOGUESPECIFICTOTALRECORDS);
			String categoryChosen = Utilities.loadPreferences(
					context, Utilities.TAG_CHOSEN_CATEGORIES);
			if (currentRecNo != null && !currentRecNo.equalsIgnoreCase("")) {

			if (Integer.parseInt(currentRecNo) < Integer.parseInt(specificTotalRecord) - 1) { // go to next record of
															// the series
				int currentcNo = (Integer.parseInt(currentRecNo) + 1) % Integer.parseInt(specificTotalRecord);
				Utilities.savePreferences(Utilities.TAG_CATALOGUERECORDNO,
						String.valueOf(currentcNo), context);
				Utilities.prepareJson(context,"");
				Intent i = new Intent(context, EmsGroupsSplitActivity.class);
				// i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
				// Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
				//context.startActivity(i);
			} else if (Integer.parseInt(categoryChosen) < 4) {
				Utilities.prepareJson(context,"");
				nextFlow = new Intent(context,
						EmsCatalogueToolBoxActivity.class);
				// nextFlow.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
				// Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
				//context.startActivity(nextFlow);
			} else {
				Utilities.writeDataJsonToCloud(context, null);
				Utilities.alertDialogDisplay(context, "status", "File is sent");
				nextFlowPresent = false;
			}
			}
		}
		if (nextFlowPresent) {
			context.startActivity(nextFlow);
		}
		}
		

}
