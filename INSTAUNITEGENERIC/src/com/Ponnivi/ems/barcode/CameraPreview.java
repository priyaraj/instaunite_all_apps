package com.Ponnivi.ems.barcode;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.PreviewCallback;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.Ponnivi.ems.R;
import com.Ponnivi.ems.activity.EmsUploadImage;
import com.Ponnivi.ems.helper.Utilities;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.LuminanceSource;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.NotFoundException;
import com.google.zxing.Result;
import com.google.zxing.client.android.PlanarYUVLuminanceSource;
import com.google.zxing.common.HybridBinarizer;

public class CameraPreview extends SurfaceView implements SurfaceHolder.Callback {
    private SurfaceHolder mHolder;
    private Camera mCamera;
    private static final String TAG = "camera";   
    private int mWidth, mHeight;
    private Context mContext;
    private MultiFormatReader mMultiFormatReader;
    private AlertDialog mDialog;
    private int mLeft, mTop, mAreaWidth, mAreaHeight;
    String barcodeResultPrefernence = null;
    public CameraPreview(Context context, Camera camera) {
        super(context);
        mCamera = camera;
        mContext = context;
        mHolder = getHolder();
        mHolder.addCallback(this);
        // deprecated setting, but required on Android versions prior to 3.0
        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        Parameters params = mCamera.getParameters();
        
        mWidth = 640;
        mHeight = 480;
      //  mHeight = 800;
        
        params.setPreviewSize(mWidth, mHeight); 
        mCamera.setParameters(params);
        
        mMultiFormatReader = new MultiFormatReader();
        
        mDialog =  new AlertDialog.Builder(mContext).create();
		barcodeResultPrefernence = getResources()
				.getString(R.string.barcoderesult);
    }

    public void surfaceCreated(SurfaceHolder holder) {
        try {
            mCamera.setPreviewDisplay(holder);
            mCamera.startPreview();
        } catch (IOException e) {
        	Utilities.alertDialogDisplay(mContext, "Error setting camera preview", e.getMessage());
            Log.d(TAG, "Error setting camera preview: " + e.getMessage());
        }
    }

    
	public void surfaceDestroyed(SurfaceHolder holder) {

    }

    
	public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {

        if (mHolder.getSurface() == null){
          return;
        }

        try {
            mCamera.stopPreview();
            
        } catch (Exception e){

        }

        try {
           /* mCamera.setPreviewCallback(mPreviewCallback);
            mCamera.setPreviewDisplay(mHolder);
            mCamera.startPreview();*/
        	mCamera.takePicture(null, null,mPicture);
            

        } catch (Exception e){
            Log.d(TAG, "Error starting camera preview: " + e.getMessage());
        }
    }
    
    public void setCamera(Camera camera) {
    	mCamera = camera;
    }
    
    public void onPause() {
    	if (mCamera != null) {
    		mCamera.setPreviewCallback(null);
    		mCamera.stopPreview();
    	}
    }
    
    private Camera.PreviewCallback mPreviewCallback = new PreviewCallback() {

        
        public void onPreviewFrame(byte[] data, Camera camera) {
            // TODO Auto-generated method stub
        	
        	if (mDialog.isShowing())
        		return;
        	
        	LuminanceSource source = new PlanarYUVLuminanceSource(data, mWidth, mHeight, mLeft, mTop, mAreaWidth, mAreaHeight, false);
            BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(
              source));
            Result result;
          
            try {
				result = mMultiFormatReader.decode(bitmap, null);
				if (result != null) {
					mDialog.setTitle("Result");
					mDialog.setMessage(result.getText());
					mDialog.show();
					Utilities.savePreferences(
							barcodeResultPrefernence,
							result.getText().toString(),
							mContext);	
				}
			} catch (NotFoundException e) {
				// TODO Auto-generated catch block
				Utilities.alertDialogDisplay(mContext, "Error in getting barcode", e.getMessage() +"\nPlease try valid image");
				//Here we are taking a bar code placed in assets folder
				//Need to do
				String barcode = Utilities.loadFromAsset(mContext,"x1.SVG");
				//
				Utilities.savePreferences(
						barcodeResultPrefernence,
						"Barcode not scanned properly",
						mContext);	
				e.printStackTrace();
			}
	    	   Intent i = new Intent(mContext,EmsUploadImage.class);
			   mContext.startActivity(i);
        }
    };
    
    PictureCallback mPicture = new PictureCallback() {
        public void onPictureTaken(byte[] data, Camera camera) {
            File pictureFile = Utilities.getOutputMediaFile();
            if (pictureFile == null) {
                return; 
            } 
            try { 
                FileOutputStream fos = new FileOutputStream(pictureFile);
                fos.write(data);
                fos.close();
            } catch (FileNotFoundException e) {
 
            } catch (IOException e) {
            } 
        } 
    };
    
    public void setArea(int left, int top, int areaWidth, int width) {
    	double ratio = width / mWidth;
    	mLeft = (int) (left / (ratio + 1));
    	mTop = (int) (top / (ratio + 1));
    	mAreaHeight = mAreaWidth = mWidth - mLeft * 2;
    }
    
}
