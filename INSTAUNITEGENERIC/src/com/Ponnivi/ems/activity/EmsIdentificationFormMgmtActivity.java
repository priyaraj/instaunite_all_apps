/**
 * Author: Priyaraj T.T.
 * * Copyright (c) 2000,2001,2002,2006-2008,2009  Ponnivi Internet Systems Pvt. Ltd.,
 * Please see the License.txt file for more information.*
 * All Rights Reserved.

 * 
 * <summary> EmsIdentificationFormMgmtActivity -- Required for Feedback on Machine Uptime . Currently for 2nd

 * */

package com.Ponnivi.ems.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Window;

import com.Ponnivi.ems.R;
import com.Ponnivi.ems.helper.Utilities;
import com.bugsense.trace.BugSenseHandler;
import com.splunk.mint.Mint;

@SuppressLint("NewApi")
public class EmsIdentificationFormMgmtActivity extends Activity {
	String userName, password, menuSelection;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.ems_identification_form_mgmt);
        //Added these lines for bugsense, need to check -- June 17
        BugSenseHandler.setup(this, "6cdb6fca");
        Mint.initAndStartSession(this, "6cdb6fca");
        //
		userName = Utilities.loadPreferences(this, getResources()
				.getString(R.string.email));
		password = Utilities.loadPreferences(this,
				Utilities.TAG_PASSWORD);
		menuSelection = Utilities.loadPreferences(this,
				Utilities.TAG_MENUSELECTION);
	}
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    if (keyCode == KeyEvent.KEYCODE_BACK) {
			if (menuSelection.equalsIgnoreCase("2")) {
				Utilities.setBackKeyValues(this);
				return true;
				}
			}
	        
	    return super.onKeyDown(keyCode, event);
	}

}