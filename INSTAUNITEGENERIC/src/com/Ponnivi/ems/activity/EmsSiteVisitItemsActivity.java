/**
 * Author: Priyaraj T.T.
 * * Copyright (c) 2000,2001,2002,2006-2008,2009  Ponnivi Internet Systems Pvt. Ltd.,
* Please see the License.txt file for more information.*
* All Rights Reserved.

* 
* <summary> EmsSiteVisitItemsActivity -- Telecom Tower Front page and from here it wil divert to EmsSiteVistRoutineItemsActivity on click of site visit buttons

 * */
package com.Ponnivi.ems.activity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.Ponnivi.ems.R;
import com.Ponnivi.ems.helper.AlertCustomDialogClass;
import com.Ponnivi.ems.helper.AppController;
import com.Ponnivi.ems.helper.Utilities;
import com.Ponnivi.ems.widget.fragment.RoutineListFragment;
import com.android.volley.AuthFailureError;
import com.android.volley.ClientError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.bugsense.trace.BugSenseHandler;
import com.splunk.mint.Mint;

public class EmsSiteVisitItemsActivity extends Activity implements OnClickListener {

    private RelativeLayout relativelayout1;
    
    private ScrollView mainscrollView;
    private LinearLayout mainLayout2;
    private RelativeLayout itemRelativeLayout;
    private TextView sitevisit1;
    private TextView sitevisit2;
    private TextView sitevisit3;
    private TextView sitevisit4;
    private TextView historytext1;
    private TextView instructionstext1;
    private TextView sitevisittext1;
    private TextView historytext2;
    private TextView historytext3;
    private TextView historytext4;
    private TextView instructionstext2;
    private TextView instructionstext3;
    private TextView instructionstext4;
    private TextView sitevisittext2;
    private TextView sitevisittext3;
    private TextView sitevisittext4;
    
    ProgressDialog pDialog;
	private String tag_json_obj = "jobj_req", tag_json_arry = "jarray_req";
	Context context = this;
	private Activity activity = new Activity();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.ems_site_visit_items);
        //Added these lines for bugsense, need to check -- June 17
        BugSenseHandler.setup(this, "6cdb6fca");
        Mint.initAndStartSession(this, "6cdb6fca");
        //
        activity = this;
        relativelayout1 = (RelativeLayout) findViewById(R.id.relativelayout1);
        
        mainscrollView = (ScrollView) findViewById(R.id.mainscrollView);
        mainLayout2 = (LinearLayout) findViewById(R.id.mainLayout2);
        itemRelativeLayout = (RelativeLayout) findViewById(R.id.itemRelativeLayout);
        sitevisit1 = (TextView) findViewById(R.id.sitevisit1);
        sitevisit2 = (TextView) findViewById(R.id.sitevisit2);
        sitevisit3 = (TextView) findViewById(R.id.sitevisit3);
        sitevisit4 = (TextView) findViewById(R.id.sitevisit4);
        
        findViewById(R.id.instructions1).setOnClickListener(this);
/*        findViewById(R.id.history1).setOnClickListener(this);
        findViewById(R.id.instructions1).setOnClickListener(this);
        

        findViewById(R.id.history2).setOnClickListener(this);
        findViewById(R.id.instructions2).setOnClickListener(this);
        
        findViewById(R.id.history3).setOnClickListener(this);
        findViewById(R.id.instructions3).setOnClickListener(this);
        
        findViewById(R.id.history4).setOnClickListener(this);
        findViewById(R.id.instructions4).setOnClickListener(this);*/
        
        findViewById(R.id.sitevisitbutton1).setOnClickListener(this);
        findViewById(R.id.sitevisitbutton2).setOnClickListener(this);
        findViewById(R.id.sitevisitbutton3).setOnClickListener(this);
        findViewById(R.id.sitevisitbutton4).setOnClickListener(this);
        
        historytext1 = (TextView) findViewById(R.id.historytext1);
        instructionstext1 = (TextView) findViewById(R.id.instructionstext1);
        sitevisittext1 = (TextView) findViewById(R.id.sitevisittext1);
        historytext2 = (TextView) findViewById(R.id.historytext2);
        historytext3 = (TextView) findViewById(R.id.historytext3);
        historytext4 = (TextView) findViewById(R.id.historytext4);
        instructionstext2 = (TextView) findViewById(R.id.instructionstext2);
        instructionstext3 = (TextView) findViewById(R.id.instructionstext3);
        instructionstext4 = (TextView) findViewById(R.id.instructionstext4);
        sitevisittext2 = (TextView) findViewById(R.id.sitevisittext2);
        sitevisittext3 = (TextView) findViewById(R.id.sitevisittext3);
        sitevisittext4 = (TextView) findViewById(R.id.sitevisittext4);
    }

    
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.history1:
                //TODO implement
                break;
            case R.id.instructions1:
                makeJsonArryReq(Utilities.getAdditionalInstructionsUrl,view.getContext());
                break;
            case R.id.sitevisitbutton1:  //For all sitevisitbuttons keeping the same data and later it may be modified -- Mar 27
            	Utilities.savePreferences(Utilities.TAG_SITE_VISIT_NAME, "sitevisit1", view.getContext());            	
            	break;
            case R.id.sitevisitbutton2:
            	Utilities.savePreferences(Utilities.TAG_SITE_VISIT_NAME, "sitevisit2", view.getContext());
            	break;
            case R.id.sitevisitbutton3:
            	Utilities.savePreferences(Utilities.TAG_SITE_VISIT_NAME, "sitevisit3", view.getContext());
            	break;
            case R.id.sitevisitbutton4:
            	Utilities.savePreferences(Utilities.TAG_SITE_VISIT_NAME, "sitevisit4", view.getContext());
            	break;
            case R.id.history2:
                //TODO implement
                break;
            case R.id.instructions2:
                //TODO implement
                break;            
            case R.id.history3:
                //TODO implement
                break;
            case R.id.instructions3:
                //TODO implement
                break;

            case R.id.history4:
                //TODO implement
                break;
            case R.id.instructions4:
                //TODO implement
                break;

        }
        switch(view.getId()) {
        	case R.id.sitevisitbutton1:
        	case R.id.sitevisitbutton2:
        	case R.id.sitevisitbutton3:
        	case R.id.sitevisitbutton4:
        		Intent intent = new Intent(view.getContext(),EmsSiteVistRoutineItemsActivity.class);
        		startActivity(intent);
        	break;
        }

    }
    
	// Using volley.jar optimization done on 1st sep --Priyaraj
    
    private void makeJsonArryReq(String jsonUrl,Context context) {
    	pDialog = Utilities.progressDialogDisplay(context);
		final String url = jsonUrl;
		final Context ctx = context;
		JsonArrayRequest req = new JsonArrayRequest(url,
				new Response.Listener<JSONArray>() {
					
					public void onResponse(JSONArray response) {

						StringBuffer sb = new StringBuffer();
						try {
						for (int i = 0; i < response.length(); i++) {							
								JSONObject jsonObject = response.getJSONObject(i);
								sb.append(jsonObject.getString("history") + "..");
								
						}
						//Utilities.alertDialogDisplay(instructionstext1.getContext(), "output from jsp call", sb.toString());
						AlertCustomDialogClass alert = new AlertCustomDialogClass(activity);
						Utilities.savePreferences("restHeaderValue", "Output from server",ctx);
						Utilities.savePreferences("restValue", sb.toString(),ctx);
						alert.show();
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							
						
						Utilities.hideProgressDialog(pDialog);
					}
				}, new Response.ErrorListener() {
					
					public void onErrorResponse(VolleyError error) {
						//Utilities.alertDialogDisplay(instructionstext1.getContext(), "error in call", error.toString());
						Utilities.hideProgressDialog(pDialog);


						if (error instanceof NetworkError
								|| error instanceof ClientError
								|| error instanceof ServerError
								|| error instanceof AuthFailureError
								|| error instanceof ParseError) {
/*							Utilities.alertDialogDisplay(instructionstext1.getContext(), "Error",
									"Server-Side Error: Please Contact Your sys-admin");
*/							AlertCustomDialogClass alert = new AlertCustomDialogClass(activity);
							Utilities.savePreferences("restHeaderValue", "Error",ctx);
							Utilities.savePreferences("restValue", "Server-Side Error: Please Contact Your sys-admin",ctx);
							alert.show();
						}

						if (error instanceof NoConnectionError
								|| error instanceof TimeoutError) {
/*							Utilities.alertDialogDisplay(instructionstext1.getContext(), "Error",
									"Please Check Internet Connectivity");
*/							AlertCustomDialogClass alert = new AlertCustomDialogClass(activity);
							Utilities.savePreferences("restHeaderValue", "Error",ctx);
							Utilities.savePreferences("restValue", "Please Check Internet Connectivity",ctx);
							alert.show();
							try {
								Toast.makeText(instructionstext1.getContext(),
										error.toString() + ". Retrying again",
										Toast.LENGTH_LONG).show();
								// cancelling current requests and then retrying
								AppController.getInstance().getRequestQueue()
										.cancelAll(tag_json_arry);
								Thread.sleep(10000);
								makeJsonArryReq(url,instructionstext1.getContext());
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}

						// req.setRetryPolicy(new DefaultRetryPolicy(20 * 10000, 5,
						// DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
						// req.setTag(tag_json_arry);

					
					}
				});
		
		/*AppController.getInstance().addToRequestQueue(req,
				tag_json_obj);*/

		// Adding request to request queue
		req.setRetryPolicy(new DefaultRetryPolicy(20 * 10000, 5,
				DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
		req.setTag(tag_json_arry);
		
		AppController.getInstance().addToRequestQueue(req,
				tag_json_arry);
		

		// Cancelling request
		// ApplicationController.getInstance().getRequestQueue().cancelAll(tag_json_arry);
	}
    
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    if (keyCode == KeyEvent.KEYCODE_BACK) {
	    	Intent nextFlow = new Intent(this, EmsGroupsActivity.class);
	    	Utilities.savePreferences("submenu", "", context);
			startActivity(nextFlow);
	        return true;
	    }

	    return super.onKeyDown(keyCode, event);
	}
	
}