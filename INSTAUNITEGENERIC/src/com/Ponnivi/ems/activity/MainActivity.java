/**
 * Author: Priyaraj T.T.
 * * Copyright (c) 2000,2001,2002,2006-2008,2009  Ponnivi Internet Systems Pvt. Ltd.,
* Please see the License.txt file for more information.*
* All Rights Reserved.

* 
* <summary> MainActivity -- Splashscreen

 * */
package com.Ponnivi.ems.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.Window;
import android.widget.TextView;

import com.Ponnivi.ems.R;
import com.Ponnivi.ems.helper.Utilities;
import com.bugsense.trace.BugSenseHandler;
import com.splunk.mint.Mint;


public class MainActivity extends Activity {

	String splashTimeOutPreference = null;
	//private PushIOManager mPushIOManager;
	final Context context = this;
	// Splash screen timer
	private static int SPLASH_TIME_OUT = 0;
	TextView textViewOwn, textView2;
	Handler handler=  new Handler();
	Runnable myRunnable;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.screen1);
        //Added these lines for bugsense, need to check -- June 17
        BugSenseHandler.setup(this, "6cdb6fca");
        Mint.initAndStartSession(this, "6cdb6fca");
        //
        //Utilities.removeAllPreferences(this);
        
		splashTimeOutPreference = context.getResources()
					.getString(R.string.splashtimeout);
		SPLASH_TIME_OUT = Integer.parseInt(splashTimeOutPreference);
		
	    myRunnable = new Runnable() {
	    public void run() {
	    	if (!Utilities.loadPreferences(context, "shouldResume").equalsIgnoreCase("true")) {
	    		Utilities.removeAllPreferences(context);
			Intent i = new Intent(MainActivity.this,EmsMenuActivity.class);
			startActivity(i);
			finish();
	    	}
	    }
	    };
	    handler.postDelayed(myRunnable,SPLASH_TIME_OUT);        
       
/*		new Handler().postDelayed(new Runnable() {

			public void run() {


				Intent i = new Intent(MainActivity.this,EmsMenuActivity.class);
				startActivity(i);
				finish();
			}
		}, SPLASH_TIME_OUT);*/
    }	
    
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
//	    if (keyCode == KeyEvent.KEYCODE_BACK) {
		if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
	    	Utilities.savePreferences("shouldResume", "true", context);
	    	Intent startMain = new Intent(Intent.ACTION_MAIN);
	    	startMain.addCategory(Intent.CATEGORY_HOME);
	    	startMain.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	    	startActivity(startMain);
	    	//finish();
	    	// handler.removeCallbacks(myRunnable);
	    	return true;
	    }
	    return super.onKeyDown(keyCode, event);
	}    
	@Override
	public void onResume() {
		super.onResume();
		if (Utilities.loadPreferences(this, "shouldResume").equalsIgnoreCase("true")) {
			myRunnable = new Runnable() {
			    public void run() {
			Utilities.removeAllPreferences(context);
			Intent i = new Intent(MainActivity.this,EmsMenuActivity.class);
			startActivity(i);
			//finish();
			    }
		};
	    handler.postDelayed(myRunnable,SPLASH_TIME_OUT);  
		}
		
	}
	@Override
	public void onPause() {
		super.onPause();
	//	Utilities.alertDialogDisplay(this, "resumed back", "resumed back");
		
	}
}
