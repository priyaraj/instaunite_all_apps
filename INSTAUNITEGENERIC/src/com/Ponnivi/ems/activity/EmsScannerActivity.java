/**
 * Author: Priyaraj T.T.
 * * Copyright (c) 2000,2001,2002,2006-2008,2009  Ponnivi Internet Systems Pvt. Ltd.,
* Please see the License.txt file for more information.*
* All Rights Reserved.

* 
* <summary> EmsBarcodeReader -- Required for barcode

 * */
package com.Ponnivi.ems.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.Ponnivi.ems.R;
import com.Ponnivi.ems.helper.Utilities;
import com.bugsense.trace.BugSenseHandler;
import com.google.zxing.Result;
import com.google.zxing.client.android.CaptureActivity;
import com.splunk.mint.Mint;


public class EmsScannerActivity extends CaptureActivity implements OnClickListener 
{
    /** Called when the activity is first created. */
	TextView TxtViewCredCode;
	String barcodePrefernence = null;
	private Button next;
	String barcodeResult = "";
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.scan);
        //Added these lines for bugsense, need to check -- June 17
        BugSenseHandler.setup(this, "6cdb6fca");
        Mint.initAndStartSession(this, "6cdb6fca");
        //
        next = (Button) findViewById(R.id.next);
        next.setOnClickListener(this);
		barcodePrefernence = getResources()
				.getString(R.string.barcoderesult);
   //     TxtViewCredCode = (TextView)findViewById(R.id.TxtViewCredCode);
   //     TxtViewCredCode.setText(" ");
    }
    
    @Override 
    public void handleDecode(Result rawResult, Bitmap barcode) 
    {
    //	TxtViewCredCode.setText(rawResult.getText());
    	if (rawResult.getText().equals("")) {
    		Utilities.alertDialogDisplay(this, "scanned qrcode value", "scanned qrcode value is empty now");
    	}
    	else {
    		barcodeResult = rawResult.getText();
    	}
    	//Toast.makeText(this.getApplicationContext(), "Scanned code " + rawResult.getText(), Toast.LENGTH_LONG);
    	//Toast.makeText(this.getApplicationContext(), "Scanned code " + rawResult.getText(), Toast.LENGTH_LONG); 
    //	Utilities.alertDialogDisplay(this, "scanned qrcode value", "scanned qrcode value="+rawResult.getText());
		Utilities.savePreferences(
				barcodePrefernence,
				barcodeResult,
				getApplicationContext());	

    }

	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.next:
				Intent i = new Intent(getApplicationContext(),
						EmsUploadImage.class);
				startActivity(i);
				break;
		}
		
	}
}