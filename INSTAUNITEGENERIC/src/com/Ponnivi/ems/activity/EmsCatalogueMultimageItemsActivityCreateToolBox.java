package com.Ponnivi.ems.activity;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.Ponnivi.ems.R;
import com.Ponnivi.ems.helper.ActivitySwipeDetector;
import com.Ponnivi.ems.helper.SwipeInterface;
import com.Ponnivi.ems.helper.Utilities;
import com.Ponnivi.ems.widget.fragment.TopHeaderFragment;
import com.bugsense.trace.BugSenseHandler;
import com.splunk.mint.Mint;

@SuppressLint("NewApi")
public class EmsCatalogueMultimageItemsActivityCreateToolBox extends Activity implements
		OnClickListener, SwipeInterface {

	private RelativeLayout relativelayout1;
	private TextView articlename1;
	private TextView articlename2;
	private TextView articlename3;
	private TextView articlename4;
	private ImageView productimage1;
	private ImageView productimage2;
	private ImageView productimage3;
	private ImageView productimage4;
	private RelativeLayout relativelayout2;
	private LinearLayout linearlayout1;
	private LinearLayout linearlayout2;
	private RelativeLayout relativelayout3;
	private LinearLayout linearlayout3;
	private LinearLayout linearlayout4;
	private String catalogueSpecificTotalRecords, catalogueTotalRecords;
	private String noOfImagesPerScreen;
	private JSONArray catalogueSpecificRecords;
	private Button ok4, ok1,ok2,ok3;
	private TextView instructions;
	int imageExist = 0;
	JSONObject jsonObject;
	int quotientResults;
	public static int index = 0;
	int recordIndex = 0;
	int remainingCatalogueSpecificTotalRecords;
	RelativeLayout itemRelativeLayout;
	LinearLayout mainLayout2;
	public static int okCount = 0;

	public static int noImageCount = 0;
	public static int itemsScanned = 0;
	public static int itemsNotScanned = 0;
	public static int swipedCount = 0;
	public int totalSwipedCounts = 0;
	boolean newItemAddRequested = false;
	String items;
	JSONArray itemsArray;
	int itemsArrayIndex = 0;
	ScrollView mainscrollView;
	Fragment multiimageheaderfragment;
	public int screenX = 0;
	public ArrayList itemArrayList = new ArrayList();
	public ArrayList totalitemArrayList = new ArrayList();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.ems_catalogue_multimage_items_create_toolbox);
        //Added these lines for bugsense, need to check -- June 17
        BugSenseHandler.setup(this, "6cdb6fca");
        Mint.initAndStartSession(this, "6cdb6fca");
        //
		multiimageheaderfragment = (TopHeaderFragment) getFragmentManager()
				.findFragmentById(R.id.multiimageheaderfragment);
		relativelayout1 = (RelativeLayout)findViewById(R.id.relativelayout1);
		mainscrollView = (ScrollView)findViewById(R.id.mainscrollView);
		mainLayout2 = (LinearLayout) findViewById(R.id.mainLayout2);
		itemRelativeLayout = (RelativeLayout) findViewById(R.id.itemRelativeLayout);
		articlename1 = (TextView) findViewById(R.id.articlename1);
		articlename2 = (TextView) findViewById(R.id.articlename2);
		articlename3 = (TextView) findViewById(R.id.articlename3);
		articlename4 = (TextView) findViewById(R.id.articlename4);
		productimage1 = (ImageView) findViewById(R.id.productimage1);
		productimage2 = (ImageView) findViewById(R.id.productimage2);
		productimage3 = (ImageView) findViewById(R.id.productimage3);
		productimage4 = (ImageView) findViewById(R.id.productimage4);
		relativelayout2 = (RelativeLayout) findViewById(R.id.relativelayout2);
		linearlayout1 = (LinearLayout) findViewById(R.id.linearlayout1);
		instructions = (TextView) findViewById(R.id.instructions);
		ok1 = (Button) findViewById(R.id.ok1);
		ok2 = (Button) findViewById(R.id.ok2);
		ok3 = (Button) findViewById(R.id.ok3);
		ok4 = (Button) findViewById(R.id.ok4);

		
		findViewById(R.id.ok1).setOnClickListener(this);
		linearlayout2 = (LinearLayout) findViewById(R.id.linearlayout2);
		findViewById(R.id.ok2).setOnClickListener(this);
		relativelayout3 = (RelativeLayout) findViewById(R.id.relativelayout3);
		linearlayout3 = (LinearLayout) findViewById(R.id.linearlayout3);
		findViewById(R.id.ok3).setOnClickListener(this);
		linearlayout4 = (LinearLayout) findViewById(R.id.linearlayout4);
		findViewById(R.id.ok4).setOnClickListener(this);

		catalogueSpecificTotalRecords = Utilities.loadPreferences(this,
				Utilities.TAG_CATALOGUESPECIFICTOTALRECORDS);
		catalogueTotalRecords = Utilities.loadPreferences(this,
				Utilities.TAG_CATALOGUETOTALRECORDS);
		
		items = Utilities.loadPreferences(this, Utilities.TAG_ITEMS);
		if (itemsScanned > 0) {
/*        	((TextView) multiimageheaderfragment.getView().findViewById(
    				R.id.round)).setVisibility(View.VISIBLE);*/
		((TextView) multiimageheaderfragment.getView().findViewById(
				R.id.cataloguerecordno)).setText(String
				.valueOf(itemsScanned) + "/" + catalogueTotalRecords);
		}
		if (items.equalsIgnoreCase("")) {
			itemsArray = new JSONArray();
			itemsArrayIndex = 0;
		}
		else {
			try {
				itemsArray = new JSONArray(items);
				itemsArrayIndex = itemsArray.length();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		if (!catalogueSpecificTotalRecords.equalsIgnoreCase("") && Integer.parseInt(catalogueSpecificTotalRecords) > 0) {
			
			noOfImagesPerScreen = Utilities.loadPreferences(this,
					Utilities.NO_OF_IMAGES_PER_SCREEN);
			totalSwipedCounts = Integer.parseInt(catalogueTotalRecords) / Integer.parseInt(noOfImagesPerScreen);
			try {
				catalogueSpecificRecords = new JSONArray(
						Utilities.loadPreferences(this,
								Utilities.TAG_CATALOGUESPECIFICRECORDS));
				
				recordIndex = swipedCount
						* Integer.parseInt(noOfImagesPerScreen);
				if (swipedCount < totalSwipedCounts) {
					
					jsonObject = catalogueSpecificRecords
							.getJSONObject(recordIndex + 0);
					
					setFieldValues(jsonObject, articlename1, productimage1, ok1, linearlayout1);
					jsonObject = catalogueSpecificRecords
							.getJSONObject(recordIndex + 1);
					setFieldValues(jsonObject, articlename2, productimage2, ok2, linearlayout2);
					jsonObject = catalogueSpecificRecords
							.getJSONObject(recordIndex + 2);
					setFieldValues(jsonObject, articlename3, productimage3, ok3, linearlayout3);
					jsonObject = catalogueSpecificRecords
							.getJSONObject(recordIndex + 3);
					setFieldValues(jsonObject, articlename4, productimage4, ok4, linearlayout4);
				}

				
				else {
					remainingCatalogueSpecificTotalRecords = 0;
					int result = Integer
							.parseInt(catalogueSpecificTotalRecords)
							% Integer.parseInt(noOfImagesPerScreen);
					if (index > 0) {
						index = index * Integer.parseInt(noOfImagesPerScreen);
					}
					//result = recordIndex ;
					switch (result) {
					case 0 :
						articlename1.setVisibility(View.INVISIBLE);
						articlename2.setVisibility(View.INVISIBLE);
						
						articlename3.setVisibility(View.INVISIBLE);
						
						articlename4.setVisibility(View.INVISIBLE);
						productimage2.setVisibility(View.INVISIBLE);
						productimage3.setVisibility(View.INVISIBLE);
						productimage4.setVisibility(View.INVISIBLE);
						//1st item as + item
						articlename1.setText("Add Tools");
						productimage1.setVisibility(View.VISIBLE);
						productimage1.setTag(R.drawable.additem);
						productimage1.setImageResource(R.drawable.additem);
					//	newItemAddRequested = true;
						productimage1.setOnClickListener(this);
						linearlayout1.setVisibility(View.INVISIBLE);
						linearlayout2.setVisibility(View.INVISIBLE);
						linearlayout3.setVisibility(View.INVISIBLE);
						linearlayout4.setVisibility(View.INVISIBLE);
						ok1.setVisibility(View.INVISIBLE);
						ok2.setVisibility(View.INVISIBLE);
						ok3.setVisibility(View.INVISIBLE);
						ok4.setVisibility(View.INVISIBLE);
						break;

					case 1:
						System.out
								.println("num is not perfectly divisble by 4 and remainder 1");
						jsonObject = catalogueSpecificRecords
								.getJSONObject(recordIndex + 0);
						setFieldValues(jsonObject, articlename1, productimage1, ok1, linearlayout1);
						articlename2.setVisibility(View.INVISIBLE);
						
						articlename3.setVisibility(View.INVISIBLE);
						productimage3.setVisibility(View.INVISIBLE);
						articlename4.setVisibility(View.INVISIBLE);
						productimage4.setVisibility(View.INVISIBLE);
						//2nd item as + item
						articlename2.setText("Add Tools");
						productimage2.setVisibility(View.VISIBLE);
						productimage2.setTag(R.drawable.additem);
						productimage2.setImageResource(R.drawable.additem);
					//	newItemAddRequested = true;
						productimage2.setOnClickListener(this);
						linearlayout2.setVisibility(View.INVISIBLE);
						linearlayout3.setVisibility(View.INVISIBLE);
						linearlayout4.setVisibility(View.INVISIBLE);
						ok2.setVisibility(View.INVISIBLE);
						ok3.setVisibility(View.INVISIBLE);
						ok4.setVisibility(View.INVISIBLE);

						break;
					case 2:
						System.out
								.println("num is not perfectly divisble by 4 and remainder 2");
						jsonObject = catalogueSpecificRecords
								.getJSONObject(recordIndex + 0);
						setFieldValues(jsonObject, articlename1, productimage1, ok1, linearlayout1);
						jsonObject = catalogueSpecificRecords
								.getJSONObject(recordIndex + 1);
						setFieldValues(jsonObject, articlename2, productimage2, ok2, linearlayout2);
						articlename3.setVisibility(View.INVISIBLE);
						
						articlename4.setVisibility(View.INVISIBLE);
						productimage4.setVisibility(View.INVISIBLE);
						//3rd item as + item
						articlename3.setText("Add Tools");
						productimage3.setVisibility(View.VISIBLE);
						productimage3.setTag(R.drawable.additem);
						productimage3.setImageResource(R.drawable.additem);
					//	newItemAddRequested = true;
						productimage3.setOnClickListener(this);
						linearlayout3.setVisibility(View.INVISIBLE);
						linearlayout4.setVisibility(View.INVISIBLE);
						ok3.setVisibility(View.INVISIBLE);
						ok4.setVisibility(View.INVISIBLE);
						break;
					case 3:
						System.out
								.println("num is not perfectly divisble by 4 and remainder 3");

						jsonObject = catalogueSpecificRecords
								.getJSONObject(recordIndex + 0);
						setFieldValues(jsonObject, articlename1, productimage1, ok1, linearlayout1);
						jsonObject = catalogueSpecificRecords
								.getJSONObject(recordIndex + 1);
						setFieldValues(jsonObject, articlename2, productimage2, ok2, linearlayout2);
						jsonObject = catalogueSpecificRecords
								.getJSONObject(recordIndex + 2);
						setFieldValues(jsonObject, articlename3, productimage3, ok3, linearlayout3);
						articlename4.setText("Add Tools");
						ok4.setVisibility(View.INVISIBLE);
						productimage4.setTag(R.drawable.additem);
						productimage4.setImageResource(R.drawable.additem);
						
					//	newItemAddRequested = true;
						productimage4.setOnClickListener(this);
						linearlayout4.setVisibility(View.INVISIBLE);
						break;
					}
				}

				ActivitySwipeDetector swipe = new ActivitySwipeDetector(this);
				mainLayout2.setOnTouchListener(swipe);
				relativelayout1.setOnTouchListener(swipe);
				mainscrollView.setOnTouchListener(swipe);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else {
			instructions.setText("");
			articlename1.setVisibility(View.INVISIBLE);
			articlename2.setVisibility(View.INVISIBLE);
			
			articlename3.setVisibility(View.INVISIBLE);
			
			articlename4.setVisibility(View.INVISIBLE);
			productimage2.setVisibility(View.INVISIBLE);
			productimage3.setVisibility(View.INVISIBLE);
			productimage4.setVisibility(View.INVISIBLE);
			//1st item as + item
			articlename1.setText("Add Tools");
			productimage1.setVisibility(View.VISIBLE);
			productimage1.setTag(R.drawable.additem);
			productimage1.setImageResource(R.drawable.additem);
			
		//	newItemAddRequested = true;
			productimage1.setOnClickListener(this);
			linearlayout1.setVisibility(View.INVISIBLE);
			linearlayout2.setVisibility(View.INVISIBLE);
			linearlayout3.setVisibility(View.INVISIBLE);
			linearlayout4.setVisibility(View.INVISIBLE);
			ok1.setVisibility(View.INVISIBLE);
			ok2.setVisibility(View.INVISIBLE);
			ok3.setVisibility(View.INVISIBLE);
			ok4.setVisibility(View.INVISIBLE);
			
		}

	}

	public void onClick(View view) {
		Intent nextFlow = new Intent();
		switch (view.getId()) {
		case R.id.ok1:
			try {
				jsonObject = catalogueSpecificRecords
				.getJSONObject(recordIndex + 0);
				makeInvisible(productimage1, ok1, articlename1,linearlayout1);
				setFlows(view.getContext(), jsonObject, (recordIndex + 0));
				//upload image to server
				Utilities.savePreferences("imageUpload", "yes", view.getContext());
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			break;			
		case R.id.ok2:
			try {
				jsonObject = catalogueSpecificRecords
				.getJSONObject(recordIndex + 1);
				makeInvisible(productimage2, ok2, articlename2,linearlayout2);
				setFlows(view.getContext(), jsonObject, (recordIndex + 1));
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			break;
		case R.id.ok3:
			try {
				jsonObject = catalogueSpecificRecords
				.getJSONObject(recordIndex + 2);
				makeInvisible(productimage3, ok3, articlename3,linearlayout3);
				setFlows(view.getContext(), jsonObject, (recordIndex + 2));
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			break;

		case R.id.ok4:
			try {
				jsonObject = catalogueSpecificRecords
				.getJSONObject(recordIndex + 3);
				makeInvisible(productimage4, ok4, articlename4,linearlayout4);
				setFlows(view.getContext(), jsonObject, (recordIndex + 3));
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			break;

		case R.id.productimage4:
		//	productimage4.setTag(R.drawable.additem);
			openNewItemOrCameraScreen(productimage4);			
			break;
		case R.id.productimage3:
		//	productimage3.setTag(R.drawable.additem);
			openNewItemOrCameraScreen(productimage3);			
			break;
		case R.id.productimage2:
		//	productimage2.setTag(R.drawable.additem);
			openNewItemOrCameraScreen(productimage2);
			break;
		case R.id.productimage1:
		//	productimage1.setTag(R.drawable.additem);
			openNewItemOrCameraScreen(productimage1);
			break;
			
		
/*		case R.id.productimage1:
		case R.id.productimage2:
		case R.id.productimage3:
			if (noImageCount > 0) {
				Utilities.savePreferences(Utilities.TAG_CATALOGUETOCAMERA,
						Utilities.TAG_CATALOGUETOCAMERA, this);

				nextFlow = new Intent(this, EmsUploadImage.class);
				startActivity(nextFlow);
			}
			break;*/

		}

		Utilities.savePreferences("ok", String.valueOf(okCount), this);
		
		

	}
	private void openNewItemOrCameraScreen(ImageView imageView) {
		Intent nextFlow = new Intent();
		Object tag = imageView.getTag();
		int id = tag == null ? -1 : Integer.parseInt(tag.toString());
		if (id == R.drawable.additem) {
	
		Utilities.savePreferences(Utilities.TAG_CATALOGUETOCAMERA,
				"insert item", this);
		nextFlow = new Intent(this,
				EmsCatalogueInsertItemActivity.class);
	} else {
		if (noImageCount > 0) {
			Utilities.savePreferences(Utilities.TAG_CATALOGUETOCAMERA,
					Utilities.TAG_CATALOGUETOCAMERA, this);

			nextFlow = new Intent(this, EmsUploadImage.class);
		}
	}
	startActivity(nextFlow);
	}
	private void setFieldValues(JSONObject jsonObject, TextView txtView,
			ImageView productImageView,Button ok,LinearLayout linearLayout) {
		try {
			txtView.setText(jsonObject.getString(Utilities.TAG_ARTICLENAME));
			if (jsonObject.getString(Utilities.TAG_SCANNEDSTATUS).equalsIgnoreCase("unscanned")) {
			itemArrayList.add(jsonObject);
			

			imageExist = Utilities.getDynamicImage(this, "pic_100_"
					+ jsonObject.getString(Utilities.TAG_ARTICLENO),
					productImageView, "pic_100");
			if (imageExist == 0) { // image is not there and hence we
									// need to do click event to go to
									// camera
				
				noImageCount += 1;
				Utilities.savePreferences("newarticleno", jsonObject.getString(Utilities.TAG_ARTICLENO), this);
				Utilities
				.savePreferences("noimage", String.valueOf(noImageCount), this);
				productImageView.setOnClickListener(this);
			}
			}
			else {
/*					int jsonPosition = Integer.parseInt(jsonObject.getString("index"));
					if (jsonPosition <= (swipedCount
							* Integer.parseInt(noOfImagesPerScreen)))*/
						
				    setInvisibleItems(productImageView, ok, txtView,linearLayout);
				}
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void bottom2top(View v) {
		// TODO Auto-generated method stub

	}
	
	
	public void left2right(View v) {
		if (swipedCount > 0) {
			swipedCount -= 1;
			Utilities.savePreferences(Utilities.TAG_CATALOGUESWIPEDCOUNT, String.valueOf(swipedCount), v.getContext());
			Intent nextFlow = new Intent(v.getContext(),
					EmsCatalogueMultimageItemsActivityCreateToolBox.class);
		startActivity(nextFlow);
		}
	}

	public void right2left(View v) {

		String categoryChosen = Utilities.loadPreferences(v.getContext(),
				Utilities.TAG_CHOSEN_CATEGORIES);
		swipedCount += 1;
		
		Intent nextFlow = new Intent();

		Utilities.savePreferences(Utilities.TAG_CATALOGUESWIPEDCOUNT, String.valueOf(swipedCount), v.getContext());

		
		if (swipedCount <= totalSwipedCounts ) {			
			nextFlow = new Intent(v.getContext(),
					EmsCatalogueMultimageItemsActivityCreateToolBox.class);
		} else {
			nextFlow = new Intent(v.getContext(), EmsSummaryActivity.class);
		}

		startActivity(nextFlow);

	}

	public void top2bottom(View v) {
		// TODO Auto-generated method stub

	}


	public void setFlows(Context context, JSONObject jsonObject, int recordIndex) {

				JSONArray newJsonArray = new JSONArray();

				try {
					catalogueSpecificRecords = new JSONArray(
							Utilities.loadPreferences(context,
									Utilities.TAG_CATALOGUESPECIFICRECORDS));

					itemsArray = Utilities.getItemsArray(context);
					for (int i = 0; i < catalogueSpecificRecords.length(); i++) {
						
						if (i == recordIndex) {
							String scanned = jsonObject.getString(Utilities.TAG_SCANNEDSTATUS).replaceAll("(.*)unscanned(.*)",
							         "scanned" );
							jsonObject.put(Utilities.TAG_SCANNEDSTATUS, scanned);
							newJsonArray.put(jsonObject);					
						}
						else {
							newJsonArray.put(catalogueSpecificRecords.get(i));
						}				
					}

					
					Utilities.savePreferences(
							Utilities.TAG_CATALOGUESPECIFICRECORDS,
							newJsonArray.toString(), context);
					Utilities.savePreferences(Utilities.TAG_CATALOGUESCANNEDITEMCOUNT, String.valueOf(itemsScanned), context);
					Utilities.saveCatalogueDetails(jsonObject, context, itemsArray, "no", "no");
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	}



	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    if (keyCode == KeyEvent.KEYCODE_BACK) {
	    	Utilities.createAndShowAlertDialog(this);
	        return true;
	    }

	    return super.onKeyDown(keyCode, event);
	}
	
	
	private void makeInvisible(ImageView image, Button ok, TextView articleName,LinearLayout linearLayout) {
		itemsScanned += 1;
		setInvisibleItems(image,ok,articleName,linearLayout);
	}
	
	private void setInvisibleItems(ImageView image, Button ok, TextView articleName,LinearLayout linearLayout) {
		image.setVisibility(View.INVISIBLE);
		ok.setVisibility(View.INVISIBLE);
		articleName.setTextColor(getResources().getColor(R.color.orange_color)); //color change from green to dark_green done on June 19 as per Ram's info for better visibility
		linearLayout.setVisibility(View.INVISIBLE);
		((TextView) multiimageheaderfragment.getView().findViewById(
				R.id.cataloguerecordno)).setText(String
				.valueOf(itemsScanned) + "/" + catalogueTotalRecords);
	}
	

}
