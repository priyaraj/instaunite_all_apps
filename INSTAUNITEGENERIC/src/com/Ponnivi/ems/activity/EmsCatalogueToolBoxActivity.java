/**
 * Author: Priyaraj T.T.
 * * Copyright (c) 2000,2001,2002,2006-2008,2009  Ponnivi Internet Systems Pvt. Ltd.,
* Please see the License.txt file for more information.*
* All Rights Reserved.

* 
* <summary> EmsBarcodeReader -- Required for barcode

 * */
 
package com.Ponnivi.ems.activity;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import com.Ponnivi.ems.R;
import com.Ponnivi.ems.helper.Utilities;



@SuppressLint("NewApi")
public class EmsCatalogueToolBoxActivity extends Activity implements  View.OnClickListener
 {

    Button btnMenu1, btnMenu2,btnMenu3,btnMenu4;
    Context context = this;
    String toolBoxArray;
    ArrayList<String> toolsArray = new ArrayList<String>();
    

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//check internet connection
	//	Utilities.checkInternetConnection(LoginActivity.this,getApplicationContext());
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		String toolBoxName = Utilities.loadPreferences(context, getResources().getString(R.string.toolbox));
		toolBoxArray = Utilities.loadPreferences(context,getResources().getString(R.string.toolbaxarray));
		setContentView(R.layout.cataloguetoolsscreen);
		
		Utilities.removePreference(Utilities.TAG_WHOLE_JSON, context);
		 Utilities.removePreference(Utilities.TAG_WHOLE_JSONArray,context);
		 //Utilities.RemovePreference(getResources().getString(R.string.toolbaxarray), context);
		Utilities.removePreference(Utilities.TAG_CATALOGUERECORDNO, context);
		btnMenu1 = (Button) findViewById(R.id.btnMenu1);
		btnMenu2 = (Button) findViewById(R.id.btnMenu2);
		btnMenu3 = (Button) findViewById(R.id.btnMenu3);
		btnMenu4 = (Button) findViewById(R.id.btnMenu4);
		
/*		if (toolBoxArray != null && !toolBoxArray.equalsIgnoreCase("")) {
			//toolsArray.add(toolBoxArray);
			toolsArray.add(toolBoxArray);
		if (toolBoxArray.indexOf(getResources().getString(R.string.Power_Tools)) > 0) {
			btnMenu1.setEnabled(false);
		}
		if (toolBoxArray.indexOf(getResources().getString(R.string.Tool_Sets)) > 0) {
			btnMenu2.setEnabled(false);
		}
		if (toolBoxArray.indexOf(getResources().getString(R.string.Screwdrivers_and_Spanners)) > 0) {
			btnMenu3.setEnabled(false);
		}
		if (toolBoxArray.indexOf(getResources().getString(R.string.Miscellenous)) > 0) {
			btnMenu4.setEnabled(false);
		}
		}
		else {
			toolsArray.add(toolBoxArray);
		}*/
		btnMenu1.setOnClickListener(this);
		btnMenu2.setOnClickListener(this);
		btnMenu3.setOnClickListener(this);
		btnMenu4.setOnClickListener(this);
	}

	public void onClick(View v) {
		Intent nextScreen = new Intent();
		String toolBoxName = "";
		switch(v.getId()) {  // As of now all are redirecting to login activity
		case R.id.btnMenu1 :
			toolBoxName = getResources().getString(R.string.Power_Tools);
			break;
		case R.id.btnMenu2 :
			toolBoxName = getResources().getString(R.string.Tool_Sets);
			break;
		case R.id.btnMenu3 :
			toolBoxName = getResources().getString(R.string.Screwdrivers_and_Spanners);
			break;
		case R.id.btnMenu4 :
			toolBoxName = getResources().getString(R.string.Miscellenous);
			break;		
			
		}
		toolsArray.add(toolBoxName);
		Utilities.savePreferences(getResources().getString(R.string.toolbaxarray), toolsArray.toString(), context);
		Utilities.savePreferences(getResources().getString(R.string.toolbox), toolBoxName, context);
		String categoryChosen = Utilities.loadPreferences(context, Utilities.TAG_CHOSEN_CATEGORIES);


		if (categoryChosen != null && !categoryChosen.equalsIgnoreCase("")) {
			categoryChosen = String.valueOf(Integer.parseInt(categoryChosen) + 1);
		}
		else {
			categoryChosen = "1";
		}
		Utilities.savePreferences(Utilities.TAG_CHOSEN_CATEGORIES, categoryChosen, context);
		int totalRecords = Utilities.getTotalNumberOfRecordsFromAssetJson(context);
		Utilities.savePreferences(Utilities.TAG_CATALOGUETOTALRECORDS,
				String.valueOf(totalRecords), context);
		nextScreen = new Intent(this,EmsGroupsSplitActivity.class);
			startActivity(nextScreen);
		}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    if (keyCode == KeyEvent.KEYCODE_BACK) {
	        finish();
	        return true;
	    }

	    return super.onKeyDown(keyCode, event);
	}
		
	}


	


