package com.Ponnivi.ems.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Window;

import com.Ponnivi.ems.R;
import com.bugsense.trace.BugSenseHandler;
import com.splunk.mint.Mint;

@SuppressLint("NewApi")
public class EmsTextboxActivity extends Activity  {

    private Fragment textboxgroups;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.inputtextbox);
        //Added these lines for bugsense, need to check -- June 17
        BugSenseHandler.setup(this, "6cdb6fca");
        Mint.initAndStartSession(this, "6cdb6fca");
        //
        textboxgroups = getFragmentManager().findFragmentById(R.id.textboxgroups);
    }
    
    public boolean onKeyDown(int keyCode, KeyEvent event) {
	    if (keyCode == KeyEvent.KEYCODE_BACK) {
	    	Intent nextFlow = new Intent(this,
					EmsGroupsActivity.class);
/*			Utilities.savePreferences("submenu", "",
					context);*/
			startActivity(nextFlow);
	    }
	    return super.onKeyDown(keyCode, event);
	}   

}