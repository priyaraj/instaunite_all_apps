/**
 * Author: Priyaraj T.T.

 * * Copyright (c) 2000,2001,2002,2006-2008,2009  Ponnivi Internet Systems Pvt. Ltd.,
* Please see the License.txt file for more information.*
* All Rights Reserved.

* 
* <summary> EmsSiteVistRoutineItemsActivity -- 

 * */
package com.Ponnivi.ems.activity;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.Ponnivi.ems.R;
import com.Ponnivi.ems.adapter.EmsBaseAdapter;
import com.Ponnivi.ems.helper.Utilities;
import com.Ponnivi.ems.widget.fragment.RoutineListFragment;
import com.Ponnivi.ems.widget.fragment.TopHeaderFragment;
import com.bugsense.trace.BugSenseHandler;
import com.splunk.mint.Mint;

@SuppressLint("NewApi")
public class EmsSiteVistRoutineItemsActivity extends Activity implements OnClickListener {

    private RelativeLayout relativelayout1;
    
    private ListView list;
    private RelativeLayout relativelayout2;
    private LinearLayout linearlayout1;
    ArrayList routinesList;
    Fragment sitevisitroutineheaderfragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.ems_site_vist_routine_items);
        //Added these lines for bugsense, need to check -- June 17
        BugSenseHandler.setup(this, "6cdb6fca");
        Mint.initAndStartSession(this, "6cdb6fca");
        //
        sitevisitroutineheaderfragment = (TopHeaderFragment) getFragmentManager()
				.findFragmentById(R.id.sitevisitroutineheaderfragment);
        RoutineListFragment routineListFragment = new RoutineListFragment();
        getFragmentManager().beginTransaction().add(R.id.list_container, routineListFragment).commit();
        if (EmsBaseAdapter.positionCount > 0) {
/*        	((TextView) sitevisitroutineheaderfragment.getView().findViewById(
    				R.id.round)).setVisibility(View.VISIBLE);*/
        ((TextView) sitevisitroutineheaderfragment.getView().findViewById(
				R.id.cataloguerecordno)).setText(String
				.valueOf(EmsBaseAdapter.positionCount) + "/" + Utilities.loadPreferences(this, Utilities.TAG_CATALOGUETOTALRECORDS));
        }
       
        findViewById(R.id.notok).setOnClickListener(this);
        findViewById(R.id.prev).setOnClickListener(this);
        if (!Utilities.loadPreferences(this, Utilities.TAG_CATALOGUETOTALRECORDS).equalsIgnoreCase("")) {
        if (EmsBaseAdapter.positionCount == Integer.parseInt(Utilities.loadPreferences(this, Utilities.TAG_CATALOGUETOTALRECORDS))) {  //when finished with last item, show the summary icon
        	findViewById(R.id.ok).setEnabled(true);
        	 findViewById(R.id.ok).setOnClickListener(this);
        }
        }
    }

   
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ok:
            	Intent nextFlow = new Intent(view.getContext(), EmsSummaryActivity.class);
            	startActivity(nextFlow);
                break;
            case R.id.notok:
                //TODO implement
                break;
            case R.id.prev:
            	Utilities.createAndShowAlertDialog(this);
                break;
        }
    }
    
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    if (keyCode == KeyEvent.KEYCODE_BACK) {
	    	Utilities.createAndShowAlertDialog(this);
	        return true;
	    }

	    return super.onKeyDown(keyCode, event);
	}
}