package com.Ponnivi.ems.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Window;

import com.Ponnivi.ems.R;
import com.Ponnivi.ems.helper.Utilities;
import com.bugsense.trace.BugSenseHandler;
import com.splunk.mint.Mint;

public class EmsGenericFormMgmtActivity extends Activity  {

    private String menuSelection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.ems_generic_form_mgmt);   
        //Added these lines for bugsense, need to check -- June 17
        BugSenseHandler.setup(this, "6cdb6fca");
        Mint.initAndStartSession(this, "6cdb6fca");
        //
        menuSelection = Utilities.loadPreferences(this, Utilities.TAG_MENUSELECTION);
    }
    
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    if (keyCode == KeyEvent.KEYCODE_BACK) {
	    	Intent nextFlow = new Intent();
	    	if (menuSelection.equalsIgnoreCase("2")) {	    		
	    			if (Utilities.loadPreferences(this, "nfc_feature").equalsIgnoreCase("false")) {
	    				nextFlow = new Intent(this, EmsIdentificationFormMgmtActivity.class);
	    	    		startActivity(nextFlow);		
					}
	    			else {
	    				Utilities.savePreferences(Utilities.TAG_IDENTIFIED_REMOTE_UNIT,"",this);
	    				Utilities.savePreferences(Utilities. TAG_IDENTIFIED_SUPERVISER_UNIT, "",
	    						this);
	    		nextFlow = new Intent(this, EmsNfcReaderActivity.class);
	    		startActivity(nextFlow);
	    			}
	    	}
	    	if (menuSelection.equalsIgnoreCase("1")) {
	    		finish();
	    	}
			
	        return true;
	    }

	    return super.onKeyDown(keyCode, event);
	}

}