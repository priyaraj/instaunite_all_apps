/**
 * Author: Priyaraj T.T.
 * * Copyright (c) 2000,2001,2002,2006-2008,2009  Ponnivi Internet Systems Pvt. Ltd.,
* Please see the License.txt file for more information.*
* All Rights Reserved.

* 
* <summary> EmsBarcodeReader -- Required for barcode

 * */
package com.Ponnivi.ems.activity;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.widget.Button;

import com.Ponnivi.ems.R;
import com.Ponnivi.ems.helper.Utilities;
import com.bugsense.trace.BugSenseHandler;
import com.splunk.mint.Mint;



@SuppressLint("NewApi")
public class EmsMenuActivity extends Activity implements  View.OnClickListener
 {
	private static final String TAG = EmsMenuActivity.class.getSimpleName();


	
    Button btnMenu1, btnMenu2,btnMenu3,btnMenu4,btnMenu5,btnMenuRemoteEvidence;
    
	
	

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//check internet connection
	//	Utilities.checkInternetConnection(LoginActivity.this,getApplicationContext());
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);

		setContentView(R.layout.menuscreen);
        //Added these lines for bugsense, need to check -- June 17
        BugSenseHandler.setup(this, "6cdb6fca");
        Mint.initAndStartSession(this, "6cdb6fca");
        //
		Utilities.removeAllPreferences(this);
		//Utilities.removePreference(Utilities.TAG_CHOSEN_CATEGORIES, this);  // As of now this is moved here in order to get send status
		btnMenu1 = (Button) findViewById(R.id.btnMenu1);
		btnMenu2 = (Button) findViewById(R.id.btnMenu2);
		btnMenu3 = (Button) findViewById(R.id.btnMenu3);
		btnMenu4 = (Button) findViewById(R.id.btnMenu4);
		btnMenu5 = (Button) findViewById(R.id.btnMenu5);
		btnMenuRemoteEvidence = (Button) findViewById(R.id.btnMenuRemoteEvidence);
		btnMenu1.setOnClickListener(this);
		btnMenu2.setOnClickListener(this);
		btnMenu3.setOnClickListener(this);
		btnMenu4.setOnClickListener(this);
		btnMenu5.setOnClickListener(this);
		btnMenuRemoteEvidence.setEnabled(false);

		
		ArrayList<JSONObject> groupList = new ArrayList<JSONObject>();
		JSONObject obj;
		JSONArray m_jArry;
		try {
			obj = new JSONObject(Utilities.loadFromAsset(this,
					"ConfigApp_JSON.json"));
			if (obj != null) {
				m_jArry = obj.getJSONArray("config");
				for (int i = 0; i < m_jArry.length(); i++) {
					JSONObject innerJson = m_jArry.getJSONObject(i);
					Utilities.savePreferences(
							Utilities.TAG_IMAGES_REQUIREMENT,
							innerJson.getString("Images"), this);
					Utilities.savePreferences(
							Utilities.NO_OF_IMAGES_PER_SCREEN,
							innerJson.getString(Utilities.NO_OF_IMAGES_PER_SCREEN), this);
					//Getting customer_id,username and password here as per discussion on 23RD June
					Utilities.savePreferences(
							Utilities.TAG_CUSTOMER_ID,
							innerJson.getString(Utilities.TAG_CUSTOMER_ID), this);
					Utilities.savePreferences(
							Utilities.TAG_USERNAME,
							innerJson.getString(Utilities.TAG_USERNAME), this);
					Utilities.savePreferences(
							Utilities.TAG_PASSWORD,
							innerJson.getString(Utilities.TAG_PASSWORD), this);
					Utilities.savePreferences(Utilities.TAG_DBNAME,
							innerJson.getString(Utilities.TAG_DBNAME), getApplicationContext());
					//
					Utilities.savePreferences(Utilities.TAG_SERVERURL,
							innerJson.getString(Utilities.TAG_SERVERURL), getApplicationContext());
					Utilities.savePreferences(Utilities.TAG_CUSTOMERSPECIFIC,
							innerJson.getString(Utilities.TAG_CUSTOMERSPECIFIC), getApplicationContext());
				}
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			Utilities.alertDialogDisplay(this, "jsonexception",
					e.getMessage());
		}
	}

	public void onClick(View v) {
		Intent nextScreen = new Intent();
		String selection = null;
		switch(v.getId()) {  // As of now all are redirecting to login activity
		case R.id.btnMenu1 :
			selection = "1";
			break;
		case R.id.btnMenu2 :
			selection = "2";
			break;
		case R.id.btnMenu3 :
			selection = "3";
			break;
		case R.id.btnMenu4 :
			selection = "4";
			break;
		case R.id.btnMenu5 :
			selection = "5";
			break;
		case R.id.btnMenuRemoteEvidence :
			selection = "remoteevidence";
			break;
			
			
		}
		Utilities.savePreferences(Utilities.TAG_MENUSELECTION,
				selection, getApplicationContext());
		//If username and password from config file is not blank, we will have login flow or else it is bypassed -- June 24
		if (!Utilities.loadPreferences(this, Utilities.TAG_USERNAME).equalsIgnoreCase("") &&
				!Utilities.loadPreferences(this, Utilities.TAG_PASSWORD).equalsIgnoreCase("")	) {		
			nextScreen = new Intent(this,LoginActivity.class);
			startActivity(nextScreen);
		}
		else {
		Utilities.redirectMenuFlows(selection, this);
		}
		}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    if (keyCode == KeyEvent.KEYCODE_BACK) {
	    	Intent nextFlow = new Intent(this, MainActivity.class);
			startActivity(nextFlow);
	        return true;
	    }

	    return super.onKeyDown(keyCode, event);
	}

/*	public boolean onTouch(View v, MotionEvent event) {
			Button b = new Button(this);
		switch(v.getId()) {
		case R.id.btnMenu1 :
				b = btnMenu1;
		break;
		case R.id.btnMenu2 :
			b = btnMenu2;
			break;
		case R.id.btnMenu3 :
			b = btnMenu3;
			break;
		case R.id.btnMenu4 :
			b = btnMenu4;
			break;
		case R.id.btnMenu5 :
			b = btnMenu5;
			break;
		}
			if(event.getAction() == MotionEvent.ACTION_UP) {
            	b.setBackgroundColor(R.drawable.orangeband);
            } else if(event.getAction() == MotionEvent.ACTION_DOWN) {
            	b.setBackgroundColor(R.drawable.orangeband);
            }
		
		return false;
	}*/
		
	}
	


