package com.Ponnivi.ems.activity;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.Ponnivi.ems.R;
import com.Ponnivi.ems.helper.Utilities;
import com.bugsense.trace.BugSenseHandler;
import com.splunk.mint.Mint;

public class EmsCatalogueInsertItemActivity extends Activity implements OnClickListener, OnItemSelectedListener {

    
    private RelativeLayout relativeLayout;
    private TextView artNo;
    private TextView articleName;
    private TextView price;
    private Spinner categoryDropdown;
    String items;
	JSONArray itemsArray;
	int itemsArrayIndex = 0;
	String chosenCategory;
	String subMenuOption;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.ems_catalogue_insert_item);
        //Added these lines for bugsense, need to check -- June 17
        BugSenseHandler.setup(this, "6cdb6fca");
        Mint.initAndStartSession(this, "6cdb6fca");
        //
        items = Utilities.loadPreferences(this, Utilities.TAG_ITEMS);
		if (items.equalsIgnoreCase("")) {
			itemsArray = new JSONArray();
			itemsArrayIndex = 0;
		}
		else {
			try {
				itemsArray = new JSONArray(items);
				itemsArrayIndex = itemsArray.length();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
       
        relativeLayout = (RelativeLayout) findViewById(R.id.relativeLayout);
        artNo = (TextView) findViewById(R.id.artNo);
        articleName = (TextView) findViewById(R.id.articleName);
        findViewById(R.id.imgBtnCapturePhoto).setOnClickListener(this);
        price = (TextView) findViewById(R.id.price);
        categoryDropdown = (Spinner) findViewById(R.id.categoryDropdown);
    	//Get the type from json depends upon toolbox/equipment
		ArrayList<String> typeList = new ArrayList<String>();
		JSONObject obj;
		JSONArray m_jArry;
		subMenuOption = Utilities.loadPreferences(this, "submenu");
		String jsonToPick = "";
		if (subMenuOption.equalsIgnoreCase("create toolbox") || subMenuOption.equalsIgnoreCase("toolbox")) {
			jsonToPick = "categorytypes.json";
		}
		else {
			jsonToPick = "equipmenttypes.json";
		}
		try {
			obj = new JSONObject(Utilities.loadFromAsset(this,
					jsonToPick));
			if (obj != null) {
				m_jArry = obj.getJSONArray("type");
				for (int i = 0; i < m_jArry.length(); i++) {
					JSONObject json = m_jArry.getJSONObject(i);
					typeList.add(json.getString("Option"));
				}
				/*Utilities.savePreferences(
						Utilities.TAG_CATALOGUESPECIFICTOTALRECORDS,
						String.valueOf(m_jArry.length()), getActivity());*/
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			Utilities.alertDialogDisplay(this, "jsonexception",
					e.getMessage());
		}
		ArrayAdapter<String> categoryAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, typeList);
		categoryAdapter.setDropDownViewResource(android.R.layout.simple_list_item_checked);
		categoryDropdown.setAdapter(categoryAdapter);
		
		categoryDropdown.setOnItemSelectedListener(this);
    }

    private EditText getArticleNo(){
        return (EditText) findViewById(R.id.articleNo);
    }

    private EditText getEditArticleName(){
        return (EditText) findViewById(R.id.editArticleName);
    }

    private EditText getEditPrice(){
        return (EditText) findViewById(R.id.editPrice);
    }
    
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgBtnCapturePhoto:
                //TODO implement
            	JSONObject newJsonObject = new JSONObject();
            	try {
            		boolean isValueExists = false;
            		isValueExists = Utilities.mandatoryChecks("ArticleNo", getArticleNo().getText().toString(),
            				view.getContext(), "Missing ArticleNo", "Missing ArticleNo");
            		if (isValueExists) {
            			isValueExists = Utilities.mandatoryChecks("ArticleName", getEditArticleName().getText().toString(),
                				view.getContext(), "Missing ArticleName", "Missing ArticleName");
            			if (isValueExists) {
            				isValueExists = Utilities.mandatoryChecks("Price", getEditPrice().getText().toString(),
                    				view.getContext(), "Missing Price", "Missing Price");
            			}
            		}
            	if (isValueExists) {
            		Utilities.savePreferences("miss_or_service",
    						"", this); //As item is inserted, we should check where it is added only, hence we should not have miss_or_service and for that making it as "" -- June 12
            		newJsonObject.put(Utilities.TAG_ARTICLENO, getArticleNo().getText().toString());
            		newJsonObject.put(Utilities.TAG_ARTICLENAME, getEditArticleName().getText().toString());
            		newJsonObject.put(Utilities.TAG_PRICE,getEditPrice().getText().toString());
            		newJsonObject.put(Utilities.TAG_INCHARGE,Utilities.loadPreferences(this, Utilities.TAG_SE_ID));
            		newJsonObject.put(Utilities.TAG_SCANNEDSTATUS,"unscanned");
            		newJsonObject.put(Utilities.TAG_CATEGORY,chosenCategory);

        			//Utilities.saveCatalogueDetails(newJsonObject,view.getContext(),"newItem");
        			Utilities.saveCatalogueDetails(newJsonObject, view.getContext(), itemsArray, "yes", "no");
        			Utilities.savePreferences("newarticleno", getArticleNo().getText().toString(), this);
                    Intent i = new Intent(this,
                    			EmsUploadImage.class);
                    	startActivity(i);
            	}

        		
    			} catch (JSONException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}
        			
            	
            	
            	

                break;
        }
    }

	public void onItemSelected(AdapterView<?> view, View arg1, int position,
			long arg3) {
		chosenCategory = String.valueOf(view.getItemAtPosition(position)); 
				//Utilities.savePreferences(Utilities.TAG_IDENTIFIED_REMOTE_UNIT, String.valueOf(view.getItemAtPosition(position)), this);
		
	}

	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub
		
	}
	 
	@Override
	 public boolean onKeyDown(int keyCode, KeyEvent event) {
	     if (keyCode == KeyEvent.KEYCODE_BACK) {
	      Utilities.createAndShowAlertDialog(this);
	         return true;
	     }

	     return super.onKeyDown(keyCode, event);
	 }
}