package com.Ponnivi.ems.activity;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Base64;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.Ponnivi.ems.R;
import com.Ponnivi.ems.helper.Utilities;
import com.bugsense.trace.BugSenseHandler;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.splunk.mint.Mint;

public class EmsSummaryActivity extends Activity implements OnClickListener {

	private RelativeLayout relativelayout1;
	ProgressDialog prgDialog;
	Bitmap bitmap;
	String imageFileName = "",encodedString ="",imagePath="";
	private TextView totalScannedItems, totalScannedItemsResult;
	private TextView totalMissingItems, totalMissingItemsResult;
	private TextView totalRepairedItems, totalRepairedItemsResult;
	private TextView totalItemsWithNoImages, totalItemsWithNoImagesResult,
			txtView5, txtView5Result, txtView6, txtView6Result, txtView7,
			txtView7Result;
	String menuSelection = "";
	String actions = "",customerId = "",evidenceIdentifier="";
	Button submit;
	RequestParams params = new RequestParams();
	Activity context;
	int imageCount = 0;
	File imageDirectory = null;
	ArrayList imageFilesToZip = new ArrayList();
	boolean imageFileUploadedCompleted = false;
	String items = null; //Required to get whether item scanned/missed/serviced
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.ems_catalogue_summary);
        //Added these lines for bugsense, need to check -- June 17
        BugSenseHandler.setup(this, "6cdb6fca");
        Mint.initAndStartSession(this, "6cdb6fca");
        //
        context = this;
		menuSelection = Utilities.loadPreferences(this,
				Utilities.TAG_MENUSELECTION);
		customerId = Utilities.loadPreferences(this,
				Utilities.TAG_CUSTOMER_ID);
		evidenceIdentifier = Utilities.loadPreferences(this,
				Utilities.TAG_EVIDENCE_IDENTIFIER);

		relativelayout1 = (RelativeLayout) findViewById(R.id.relativelayout1);
		totalScannedItems = (TextView) findViewById(R.id.totalScannedItems);
		totalMissingItems = (TextView) findViewById(R.id.totalMissingItems);
		totalRepairedItems = (TextView) findViewById(R.id.totalRepairedItems);
		totalItemsWithNoImages = (TextView) findViewById(R.id.totalItemsWithNoImages);

		totalScannedItemsResult = (TextView) findViewById(R.id.totalScannedItemsResult);
		totalMissingItemsResult = (TextView) findViewById(R.id.totalMissingItemsResult);
		totalRepairedItemsResult = (TextView) findViewById(R.id.totalRepairedItemsResult);
		totalItemsWithNoImagesResult = (TextView) findViewById(R.id.totalItemsWithNoImagesResult);
		txtView5 = (TextView) findViewById(R.id.txtView5);
		txtView5Result = (TextView) findViewById(R.id.txtView5Result);
		txtView6 = (TextView) findViewById(R.id.txtView6);
		txtView6Result = (TextView) findViewById(R.id.txtView6Result);
		txtView7 = (TextView) findViewById(R.id.txtView7);
		txtView7Result = (TextView) findViewById(R.id.txtView7Result);
		submit = (Button)findViewById(R.id.submit);
/*		if (menuSelection.equalsIgnoreCase("2")) {
			MarginLayoutParams params = (MarginLayoutParams) submit.getLayoutParams();
			params.topMargin = 200;
			submit.setLayoutParams(params);
		}   Need to do . As multiple actions are coming, this is taken care, but yet not completed - Apr 24*/  
		displayStaticValues(menuSelection);
		displayDynamicValues(menuSelection, this);
		items = Utilities.loadPreferences(context,
				Utilities.TAG_ITEMS);
		imageDirectory = Utilities.getDir(customerId, evidenceIdentifier);
		File[] files = imageDirectory.listFiles();
		if (files != null && files.length > 0) {
		for (File CurFile : files) {
		      System.out.println("filename.." + CurFile.getName());
		      if (CurFile.getName().endsWith(".jpg")) {
		      imagePath = imageDirectory + "/" + CurFile.getName();
		      //Adding the zip file logic			      
		      //imageFilesToZip.add(imageDirectory + "/" + CurFile.getName());
		      imageFilesToZip.add(CurFile.getName());
		      //imageCount += 1;
		      }
		      }
		  if (imageFilesToZip != null  && imageFilesToZip.size() > 0) {
	      //params.put("filename", imageFilesToZip.get(imageCount).toString());
	      //encodeImagetoString();  
			  //imageFileUploadedCompleted = true;
			  findViewById(R.id.submit).setOnClickListener(this);
		  }
		  else {
			  imageFileUploadedCompleted = true; //To continue json upload, may be some refinements required -- July 30
		  }
			  
		  
		}  //files != null
		//else {
			if (!items.equalsIgnoreCase("")) {
				findViewById(R.id.submit).setOnClickListener(this);
			}
			else {
				Toast.makeText(
						getApplicationContext(),
						"No tools/equipments added/modified. Redirecting to the main screen.",
								Toast.LENGTH_LONG)
						.show();
				submit.setVisibility(View.GONE);
				Utilities.resetCatalogueCountValues(this);
				Intent nextFlow = new Intent(this, EmsMenuActivity.class);
				startActivity(nextFlow);
			}
			
		//}
		
		
	}

	private void displayStaticValues(String menuSelection) {
		if ( ( menuSelection.equalsIgnoreCase("4") || menuSelection.equalsIgnoreCase("1") ) 
				&& 	
				(Utilities.loadPreferences(this, "submenu").equalsIgnoreCase("toolbox") ||
				  Utilities.loadPreferences(this, "submenu").equalsIgnoreCase("equipmentbox")) 
						)  {
			totalScannedItems.setText("Total Scanned Items : ");			
			
			totalMissingItems.setText("Total Missing Items : ");

			totalRepairedItems.setText("Total Items to be repaired : ");

			totalItemsWithNoImages.setText("Total Items with no image : ");

			txtView5.setText("Total Unscanned Items : ");

			txtView6.setText("New Items Added: ");
		}
		if ( ( menuSelection.equalsIgnoreCase("4") || menuSelection.equalsIgnoreCase("1") ) 
				&& 	
				(Utilities.loadPreferences(this, "submenu").equalsIgnoreCase("create toolbox") 
				|| Utilities.loadPreferences(this, "submenu").equalsIgnoreCase("Create Equipment") ) ) {
			totalScannedItems.setText("Total Scanned Items : ");			
			
			totalMissingItems.setText("Total Items with no image : ");

			totalRepairedItems.setText("Total Unscanned Items : ");

			totalItemsWithNoImages.setText("New Items Added: ");
		}
		if (menuSelection.equalsIgnoreCase("1") && 	
				Utilities.loadPreferences(this, "submenu").equalsIgnoreCase("btnMenu1")) {
			// totalScannedItems.setText("Tower : ");
			totalScannedItems.setText("Items checked : ");
			totalMissingItems.setText("Total Missing Items : ");

			totalRepairedItems.setText("Total Items to be repaired : ");

			totalItemsWithNoImages.setText("Total images : ");
			

				txtView5.setText("Tower : ");


			// txtView6.setText("Images: ");
		}
		if (menuSelection.equalsIgnoreCase("2")) {

			totalScannedItems.setText("Remote Unit : ");
			totalMissingItems.setText("Machine :");
			totalRepairedItems.setText("Superviser :");
			totalItemsWithNoImages.setText("Reasons : ");
			txtView5.setText("Machine Downtime : ");
			txtView6.setText("Actions Taken : ");
			
		}

	}

	private void displayDynamicValues(String menuSelection, Context context) {
		//4
		if ( ( menuSelection.equalsIgnoreCase("4") || menuSelection.equalsIgnoreCase("1") ) && 
				Utilities.loadPreferences(context,"submenu").equalsIgnoreCase("toolbox") ||
				Utilities.loadPreferences(context, "submenu").equalsIgnoreCase("equipmentbox"))   {
			int scannedItemCount = 0;
			int unScannedItemCount = 0;
			int totalRecords = 0;

			if (Utilities.loadPreferences(this,
					Utilities.TAG_CATALOGUESCANNEDITEMCOUNT).equalsIgnoreCase(
					"")) {
				totalScannedItemsResult.setText("0");
			} else {
				totalScannedItemsResult.setText(Utilities.loadPreferences(this,
						Utilities.TAG_CATALOGUESCANNEDITEMCOUNT));
				scannedItemCount = Integer.parseInt(totalScannedItemsResult
						.getText().toString());
			}
			totalRecords = Integer.parseInt(Utilities.loadPreferences(this,
					Utilities.TAG_CATALOGUETOTALRECORDS));
			unScannedItemCount = totalRecords - scannedItemCount;

			txtView5Result.setText(String.valueOf(unScannedItemCount));
			if (Utilities.loadPreferences(this, Utilities.TAG_ADDEDCOUNT)
					.equalsIgnoreCase("")) {
				txtView6Result.setText("0");
			} else {
				txtView6Result.setText(Utilities.loadPreferences(this,
						Utilities.TAG_ADDEDCOUNT));
			}

			if (Utilities.loadPreferences(this, "noimage").equalsIgnoreCase("")) {
				totalItemsWithNoImagesResult.setText("0");
			} else {
				totalItemsWithNoImagesResult.setText(Utilities.loadPreferences(
						this, "noimage"));
			}

		}
		
		if (menuSelection.equalsIgnoreCase("4") || (menuSelection.equalsIgnoreCase("1") && 
				( Utilities.loadPreferences(context,"submenu").equalsIgnoreCase("create toolbox")
					|| Utilities.loadPreferences(context,"submenu").equalsIgnoreCase("create equipment") ) ) ) {
			int scannedItemCount = 0;
			int unScannedItemCount = 0;
			int totalRecords = 0;

			if (Utilities.loadPreferences(this,
					Utilities.TAG_CATALOGUESCANNEDITEMCOUNT).equalsIgnoreCase(
					"")) {
				totalScannedItemsResult.setText("0");
			} else {
				totalScannedItemsResult.setText(Utilities.loadPreferences(this,
						Utilities.TAG_CATALOGUESCANNEDITEMCOUNT));
				scannedItemCount = Integer.parseInt(totalScannedItemsResult
						.getText().toString());
			}
			totalRecords = Integer.parseInt(Utilities.loadPreferences(this,
					Utilities.TAG_CATALOGUETOTALRECORDS));
			unScannedItemCount = totalRecords - scannedItemCount;

			totalRepairedItemsResult.setText(String.valueOf(unScannedItemCount));
			if (Utilities.loadPreferences(this, Utilities.TAG_ADDEDCOUNT)
					.equalsIgnoreCase("")) {
				totalItemsWithNoImagesResult.setText("0");
			} else {
				totalItemsWithNoImagesResult.setText(Utilities.loadPreferences(this,
						Utilities.TAG_ADDEDCOUNT));
			}

			if (Utilities.loadPreferences(this, "noimage").equalsIgnoreCase("")) {
				totalMissingItemsResult.setText("0");
			} else {
				totalMissingItemsResult.setText(Utilities.loadPreferences(
						this, "noimage"));
			}
			
			
			

		}
		//4 end
		if (menuSelection.equalsIgnoreCase("1") && 
				Utilities.loadPreferences(context,"submenu").equalsIgnoreCase("btnMenu1")) {
			totalScannedItemsResult.setText(Utilities.loadPreferences(context,
					Utilities.TAG_CATALOGUETOTALRECORDS));
			txtView5Result.setText(Utilities.loadPreferences(context,
					Utilities.TAG_SITE_VISIT_NAME));
		}
		//1 or 4
		if (menuSelection.equalsIgnoreCase("1") || ( menuSelection.equalsIgnoreCase("4")
				&& Utilities.loadPreferences(context,"submenu").equalsIgnoreCase("toolbox")) ) {
		if (Utilities.loadPreferences(this, "missing").equalsIgnoreCase("")) {
			totalMissingItemsResult.setText("0");
		} else {
			totalMissingItemsResult.setText(Utilities.loadPreferences(this,
					"missing"));
		}
		if (Utilities.loadPreferences(this, "service").equalsIgnoreCase("")) {
			totalRepairedItemsResult.setText("0");
		} else {
			totalRepairedItemsResult.setText(Utilities.loadPreferences(this,
					"service"));
		}
		if (Utilities.loadPreferences(this, "noimage").equalsIgnoreCase("")) {
			totalItemsWithNoImagesResult.setText("0");
		} else {
			totalItemsWithNoImagesResult.setText(Utilities.loadPreferences(
					this, "noimage"));
		}
		}
		
		//2 begin
		if (menuSelection.equalsIgnoreCase("2")) {

			totalScannedItemsResult.setText(Utilities.loadPreferences(this,
					Utilities.TAG_IDENTIFIED_REMOTE_UNIT));
			totalMissingItemsResult.setText(Utilities.loadPreferences(this,
					Utilities.TAG_IDENTIFIED_MACHINE_UNIT));
			totalRepairedItemsResult.setText(Utilities.loadPreferences(this,
					Utilities.TAG_IDENTIFIED_SUPERVISER_UNIT));
			totalItemsWithNoImagesResult.setText(Utilities.loadPreferences(
					this, Utilities.TAG_REASONS));
			txtView5Result.setText(Utilities.loadPreferences(this,
					Utilities.TAG_MACHINE_DOWNTIME));
			actions = Utilities.getActionNames(this);
			txtView6Result.setText(actions);
			Utilities.addedActionsList = new ArrayList(); //After completing one cycle, need to make static value to new
			prepareItemsJson();

		} // 2 end

	}
	
	private void prepareItemsJson() {
		try {
			JSONObject specific_details = new JSONObject();

			specific_details.put(Utilities.TAG_A, "null");
			specific_details.put(Utilities.TAG_B, "null");
			specific_details.put(Utilities.TAG_C, "null");
			specific_details.put(Utilities.TAG_D, "null");

			JSONObject evidences = new JSONObject();

			evidences.put(Utilities.TAG_EVIDENCE_1,
					"image_location_will_be_modified_later"); // instead of
																// image
																// location
																// directory,
																// currently
																// we are
																// having
																// articleno,
																// latger it
																// will be
																// modified
																// -- Mar 26

			evidences.put(Utilities.TAG_EVIDENCE_2,
					Utilities.loadPreferences(this, Utilities.TAG_REASONS));
			evidences.put(Utilities.TAG_EVIDENCE_3, Utilities
					.loadPreferences(this, Utilities.TAG_MACHINE_DOWNTIME));

			evidences.put(Utilities.TAG_EVIDENCE_4, actions);
			evidences.put(Utilities.TAG_EVIDENCE_5, "null");
			evidences.put(Utilities.TAG_EVIDENCE_6, "null");
			evidences.put(Utilities.TAG_EVIDENCE_7, "null");
			evidences.put(Utilities.TAG_EVIDENCE_8, "null");
			evidences.put(Utilities.TAG_EVIDENCE_9, "null");
			evidences.put(Utilities.TAG_EVIDENCE_10, "null");

			JSONObject evidence_qualifiers = new JSONObject();

			if (Utilities.loadPreferences(this, Utilities.TAG_REASONS)
					.equalsIgnoreCase("unknown machine issue")) {
				evidence_qualifiers.put(Utilities.TAG_QUALIFIER_1,
						Utilities.loadPreferences(this,
								Utilities.TAG_REASONS));
			} else {
				evidence_qualifiers.put(Utilities.TAG_QUALIFIER_1, "null");
			}
			evidence_qualifiers.put(Utilities.TAG_QUALIFIER_2, "null");

			evidence_qualifiers.put(Utilities.TAG_QUALIFIER_3, "null");
			evidence_qualifiers.put(Utilities.TAG_QUALIFIER_4, "null");
			evidence_qualifiers.put(Utilities.TAG_QUALIFIER_5, "null");
			evidence_qualifiers.put(Utilities.TAG_QUALIFIER_6, "null");
			evidence_qualifiers.put(Utilities.TAG_QUALIFIER_7, "null");
			evidence_qualifiers.put(Utilities.TAG_QUALIFIER_8, "null");
			evidence_qualifiers.put(Utilities.TAG_QUALIFIER_9, "null");
			evidence_qualifiers.put(Utilities.TAG_QUALIFIER_10, "null");

			JSONObject itemsJsonObject = new JSONObject();
			itemsJsonObject.put(Utilities.TAG_SPECIFIC_DETAILS,
					specific_details);
			itemsJsonObject.put(Utilities.TAG_EVIDENCES, evidences);
			itemsJsonObject.put(Utilities.TAG_EVIDENCE_QUALIFIERS,
					evidence_qualifiers);
			JSONArray jsonArray = new JSONArray();
			jsonArray.put(jsonArray.length(), itemsJsonObject);
			Utilities.savePreferences(Utilities.TAG_ITEMS,
					jsonArray.toString(), this);
			String evidence_dentifier = totalScannedItemsResult.getText()
					.toString()
					+ "_"
					+ totalMissingItemsResult.getText().toString();
			Utilities.savePreferences(Utilities.TAG_EVIDENCE_IDENTIFIER,
					evidence_dentifier, this);
			// Utilities.prepareMachineUptimeJson(this);
		} catch (JSONException ex) {
			ex.printStackTrace();
		}
	}

	public void onClick(View view) {
		switch (view.getId()) {			
		case R.id.submit:
			
			boolean internetConnected = Utilities.checkInternetConnection(this,context);  
			if (internetConnected) {
			if (menuSelection.equalsIgnoreCase("4")) {
				
			//	else if (imageFileUploadedCompleted){
				
				if (!items.equalsIgnoreCase("")) {  //When there are items, we need to upload meta and data json
				if (Utilities.loadPreferences(this, "submenu").equalsIgnoreCase("create toolbox") ||
						Utilities.loadPreferences(this, "submenu").equalsIgnoreCase("toolbox")	) { 
				Utilities.writeMetaJsonToCloud(this,
						"metajson_rotatingassets_" + customerId + ".json");
				Utilities.writeDataJsonToCloud(this,
						"datajson_rotatingassets_" + customerId + ".json");				
				}				
				else {
					Utilities.writeMetaJsonToCloud(this,
							"metajson_rotatingassets_equipments_" + customerId + ".json");
					Utilities.writeDataJsonToCloud(this,
							"datajson_rotatingassets_equipments_" + customerId + ".json");				
				}
				
				Utilities.resetCatalogueCountValues(this);
				}
				
				if (imageFilesToZip != null  && imageFilesToZip.size() > 0) {
					
			      params.put("filename", imageFilesToZip.get(imageCount).toString());
			      encodeImagetoString();    
				  }
				
			}  //menuselection 
			if (menuSelection.equalsIgnoreCase("1")) {
				Utilities.writeMetaJsonToCloud(this,
						"metajson_telecom_towers_" + customerId + ".json");
				Utilities.writeDataJsonToCloud(this,
						"datajson_telecom_towers_" + customerId + ".json");
				Utilities.resetSiteRoutineCountValues(this);
			}
			if (menuSelection.equalsIgnoreCase("2")) {

				Utilities.writeMetaJsonToCloud(this,
						"metajson_machineuptime_" + customerId + ".json");
				Utilities.writeDataJsonToCloud(this,
						"datajson_machineuptime_" + customerId + ".json");
			//	if (Utilities.loadPreferences(
				//		this, "datajsonupload").equalsIgnoreCase("success")) {
				if (imageFilesToZip != null  && imageFilesToZip.size() > 0 &&
						Utilities.loadPreferences(
								this, Utilities.TAG_REASONS).equalsIgnoreCase("Unknown Machine Issue")
						) {
				      params.put("filename", imageFilesToZip.get(imageCount).toString());
				      encodeImagetoString();    
					  }
				else {
					Intent nextFlow = new Intent(context, EmsMenuActivity.class);
					context.startActivity(nextFlow);
				}
				//}
			}
			Utilities.resetCatalogueCountValues(this);
			//encodeImagetoString();
			
			
			break;
			}
		}
	}

	
	//As back key is pressed, need to ask the user to reset  the flow or not -- June 19
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    if (keyCode == KeyEvent.KEYCODE_BACK) {
	    	Utilities.createAndShowAlertDialog(this);
	        return true;
	    }

	    return super.onKeyDown(keyCode, event);
	}
	
	// AsyncTask - To convert Image to String
	public void encodeImagetoString() {
		prgDialog = new ProgressDialog(this,R.style.Theme_MyDialog);
//		prgDialog.show();
		// Set Cancelable as False
		prgDialog.setCancelable(false);
		imagePath = imageDirectory + "/" + imageFilesToZip.get(imageCount).toString();
		//imageCount += 1;
	
		
		new AsyncTask<Void, Void, String>() {

			protected void onPreExecute() {

			};

			@SuppressLint("NewApi")
			@Override
			protected String doInBackground(Void... params) {
				BitmapFactory.Options options = null;
				options = new BitmapFactory.Options();
				options.inSampleSize = 3;
				
				bitmap = BitmapFactory.decodeFile(imagePath,
						options);
				ByteArrayOutputStream stream = new ByteArrayOutputStream();
				// Must compress the Image to reduce image size to make upload easy
				bitmap.compress(Bitmap.CompressFormat.JPEG, 30, stream); 
				byte[] byte_arr = stream.toByteArray();
				// Encode Image to String
				
				encodedString = Base64.encodeToString(byte_arr, 0);
				return "";
			}

			@Override
			protected void onPostExecute(String msg) {
				
				// Put converted Image string into Async Http Post param
				//if (imageCount <= imageFilesToZip.size()) {		
					if (imageCount < imageFilesToZip.size()) {
				//	prgDialog.setMessage("Image upload process initiated");
					imageCount += 1;
				params.put("image", encodedString);
				//params.put("imageCount", imageCount);
				// Trigger Image upload
				triggerImageUpload();
				}
/*				else {
					if (items.equalsIgnoreCase("")) { //As there is no need to upload data json, we can go to main screen
						Intent nextFlow = new Intent(context, EmsMenuActivity.class);
						context.startActivity(nextFlow);
					}
					else {  //let us check this flow
						imageFileUploadedCompleted = true;
						
							if (Utilities.loadPreferences(context, "submenu").equalsIgnoreCase("create toolbox") ||
									Utilities.loadPreferences(context, "submenu").equalsIgnoreCase("toolbox")	) { 
							Utilities.writeMetaJsonToCloud(context,
									"metajson_rotatingassets_" + customerId + ".json");
							Utilities.writeDataJsonToCloud(context,
									"datajson_rotatingassets_" + customerId + ".json");				
							}				
							else {
								Utilities.writeMetaJsonToCloud(context,
										"metajson_rotatingassets_equipments_" + customerId + ".json");
								Utilities.writeDataJsonToCloud(context,
										"datajson_rotatingassets_equipments_" + customerId + ".json");				
							}
							
							Utilities.resetCatalogueCountValues(context);
							
					}
				}*/
			}
		}.execute(null, null, null);
	}
	//}
	
	public void triggerImageUpload() {
		makeHTTPCall();
	}

	// http://192.168.2.4:9000/imgupload/upload_image.php
	// http://192.168.2.4:9999/ImageUploadWebApp/uploadimg.jsp
	// Make Http call to upload Image to Java server
	public void makeHTTPCall() {
		prgDialog = new ProgressDialog(this,R.style.Theme_MyDialog);
		prgDialog.show();
		// Set Cancelable as False
		prgDialog.setCancelable(false);
		prgDialog.setMessage("Captured Data uploading to server");		
		AsyncHttpClient client = new AsyncHttpClient();
		client.post(Utilities.getImageUploadToServerUrl(context),
		//client.post("http://192.168.137.1:8080/formbasedreadandwriteroutineexamples/uploadimg.jsp",
				params, new AsyncHttpResponseHandler() {
					// When the response returned by REST has Http
					// response code '200'
					@Override
					public void onSuccess(String response) {
						// Hide Progress Dialog
						prgDialog.hide();
						//Utilities.alertDialogDisplay(context, response, response);
						if (imageCount < imageFilesToZip.size()) {							
							params.put("filename", imageFilesToZip.get(imageCount).toString());
							encodeImagetoString();
						}
						else {
						//	if (items.equalsIgnoreCase("")) { //As there is no need to upload data json, we can go to main screen
							Toast.makeText(
									getApplicationContext(),
									"Captured Data has been uploaded successfully"
											, Toast.LENGTH_LONG)
									.show();
								Intent nextFlow = new Intent(context, EmsMenuActivity.class);
								context.startActivity(nextFlow);
						//	}
/*							else {  //let us check this flow
								imageFileUploadedCompleted = true;
								
									if (Utilities.loadPreferences(context, "submenu").equalsIgnoreCase("create toolbox") ||
											Utilities.loadPreferences(context, "submenu").equalsIgnoreCase("toolbox")	) { 
									Utilities.writeMetaJsonToCloud(context,
											"metajson_rotatingassets_" + customerId + ".json");
									Utilities.writeDataJsonToCloud(context,
											"datajson_rotatingassets_" + customerId + ".json");				
									}				
									else {
										Utilities.writeMetaJsonToCloud(context,
												"metajson_rotatingassets_equipments_" + customerId + ".json");
										Utilities.writeDataJsonToCloud(context,
												"datajson_rotatingassets_equipments_" + customerId + ".json");				
									}
									
									Utilities.resetCatalogueCountValues(context);
									
							}*/
						}
						
					}

					// When the response returned by REST has Http
					// response code other than '200' such as '404',
					// '500' or '403' etc
					@Override
					public void onFailure(int statusCode, Throwable error,
							String content) {
						// Hide Progress Dialog
						prgDialog.hide();
						/*Toast.makeText(
								getApplicationContext(),
								"statusCode..." + statusCode
										, Toast.LENGTH_LONG)
								.show();*/
						// When Http response code is '404'
						if (statusCode == 404) {
							Toast.makeText(getApplicationContext(),
									"Requested resource not found",
									Toast.LENGTH_LONG).show();
							//Utilities.alertDialogDisplay(context, "Requested resource not found", "Requested resource not found. Please try again");
						}
						// When Http response code is '500'
						else if (statusCode == 500) {
							Toast.makeText(getApplicationContext(),
									"Something went wrong at server end",
									Toast.LENGTH_LONG).show();
							//Utilities.alertDialogDisplay(context, "Something went wrong at server end", "Something went wrong at server end. Please try again");
						}
						// When Http response code other than 404, 500
						else {
							Toast.makeText(
									getApplicationContext(),
									"Error Occured \n Device not connected to Internet.Please try again later. "
											, Toast.LENGTH_LONG)
									.show();
							//Utilities.alertDialogDisplay(context, "Error occured", "Error Occured \n Device not connected to Internet. Please try again");
							
						}
						if (Utilities.loadPreferences(context, Utilities.TAG_MENUSELECTION).equalsIgnoreCase("4") && 
								//Utilities.loadPreferences(context, "submenu").equalsIgnoreCase("toolbox")) {
									(Utilities.loadPreferences(context, "submenu").equalsIgnoreCase("toolbox") || 
											Utilities.loadPreferences(context, "submenu").equalsIgnoreCase("btnMenu3") ||
											Utilities.loadPreferences(context, "submenu").equalsIgnoreCase("create toolbox") ||
											Utilities.loadPreferences(context, "submenu").equalsIgnoreCase("create equipment") ||
											Utilities.loadPreferences(context, "submenu").equalsIgnoreCase("equipmentbox"))) {
							Toast.makeText(
									getApplicationContext(),
									"Captured Data has not ploaded successfully. Please contact System administrator"
											, Toast.LENGTH_LONG)
									.show();
								Intent nextFlow = new Intent(context,
										EmsMenuActivity.class);
								context.startActivity(nextFlow);
							}
					}
					
				});
	}
	
	/*private void doFileUpload(){
        HttpURLConnection conn = null;
        DataOutputStream dos = null;
        DataInputStream inStream = null;
        String lineEnd = "rn";
        String twoHyphens = "--";
        String boundary =  "*****";
        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1*1024*1024;
        String responseFromServer = "";
        String urlString = "http://your_website.com/upload_audio_test/upload_audio.php";
        try
        {
         //------------------ CLIENT REQUEST
        FileInputStream fileInputStream = new FileInputStream(new File(selectedPath) );
         // open a URL connection to the Servlet
         URL url = new URL(urlString);
         // Open a HTTP connection to the URL
         conn = (HttpURLConnection) url.openConnection();
         // Allow Inputs
         conn.setDoInput(true);
         // Allow Outputs
         conn.setDoOutput(true);
         // Don't use a cached copy.
         conn.setUseCaches(false);
         // Use a post method.
         conn.setRequestMethod("POST");
         conn.setRequestProperty("Connection", "Keep-Alive");
         conn.setRequestProperty("Content-Type", "multipart/form-data;boundary="+boundary);
         dos = new DataOutputStream( conn.getOutputStream() );
         dos.writeBytes(twoHyphens + boundary + lineEnd);
         dos.writeBytes("Content-Disposition: form-data; name="uploadedfile";filename="" + selectedPath + """ + lineEnd);
         dos.writeBytes(lineEnd);
         // create a buffer of maximum size
         bytesAvailable = fileInputStream.available();
         bufferSize = Math.min(bytesAvailable, maxBufferSize);
         buffer = new byte[bufferSize];
         // read file and write it into form...
         bytesRead = fileInputStream.read(buffer, 0, bufferSize);
         while (bytesRead > 0)
         {
          dos.write(buffer, 0, bufferSize);
          bytesAvailable = fileInputStream.available();
          bufferSize = Math.min(bytesAvailable, maxBufferSize);
          bytesRead = fileInputStream.read(buffer, 0, bufferSize);
         }
         // send multipart form data necesssary after file data...
         dos.writeBytes(lineEnd);
         dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
         // close streams
         
         System.out.println("File is written");
         fileInputStream.close();
         dos.flush();
         dos.close();
        }
        catch (MalformedURLException ex)
        {
            // Log.e("Debug", "error: " + ex.getMessage(), ex);
        	 System.out.println("Debug error: " + ex.getMessage());
        }
        catch (IOException ioe)
        {
        	System.out.println("Debugerror: " + ioe.getMessage(), ex);
        }
        //------------------ read the SERVER RESPONSE
        try {
              inStream = new DataInputStream ( conn.getInputStream() );
              String str;

              while (( str = inStream.readLine()) != null)
              {
                   
                   System.out.println("Server Response "+str);
              }
              inStream.close();

        }
        catch (IOException ioex){
        	System.out.println("Debug error: " + ioex.getMessage());
        }
      }*/
}
