/**
 * Author: Priyaraj T.T.
 * * Copyright (c) 2000,2001,2002,2006-2008,2009  Ponnivi Internet Systems Pvt. Ltd.,
* Please see the License.txt file for more information.*
* All Rights Reserved.

* 
* <summary> LoginActivity -- Take username and password as credentials, validates and redirects the screen

 * */
package com.Ponnivi.ems.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import com.Ponnivi.ems.R;
import com.Ponnivi.ems.helper.Utilities;
import com.bugsense.trace.BugSenseHandler;
import com.splunk.mint.Mint;

@SuppressLint("NewApi")
public class LogoutActivity extends Activity  {
	private static final String TAG = LogoutActivity.class.getSimpleName();


	Button imgBtnLogin;
	Context context = this;
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		this.requestWindowFeature(Window.FEATURE_NO_TITLE);

		setContentView(R.layout.logout);
        //Added these lines for bugsense, need to check -- June 17
        BugSenseHandler.setup(this, "6cdb6fca");
        Mint.initAndStartSession(this, "6cdb6fca");
        //
		Utilities.removeAllPreferences(this);
		imgBtnLogin = (Button) findViewById(R.id.imgBtnLogin);

		imgBtnLogin.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				 Intent i = new Intent(context,LoginActivity.class);
				 startActivity(i);

			}
		});
	}

}
