/**
 * Author: Priyaraj T.T.
 * * Copyright (c) 2000,2001,2002,2006-2008,2009  Ponnivi Internet Systems Pvt. Ltd.,
 * Please see the License.txt file for more information.*
 * All Rights Reserved.

 * 
 * <summary> EmsUploadImage -- Required for uploading image 

 * */
package com.Ponnivi.ems.activity;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.ErrorCallback;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.ShutterCallback;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Surface;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.Ponnivi.ems.R;
import com.Ponnivi.ems.adapter.EmsBaseAdapter;
import com.Ponnivi.ems.helper.Preview;
import com.Ponnivi.ems.helper.Utilities;
import com.Ponnivi.ems.widget.CenterLockHorizontalScrollview;
import com.bugsense.trace.BugSenseHandler;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.splunk.mint.Mint;

@SuppressLint("NewApi")
public class EmsUploadImage extends Activity implements OnClickListener {
	// public class EmsUploadImage extends Activity implements OnClickListener {

	// ImageView image;
	ProgressDialog prgDialog;
	Activity context;
	Preview preview;
	Camera camera;
	// Button exitButton;
	ImageView fotoButton;
	ImageView takeMore, cancel;
	LinearLayout progressLayout;
	String imagePathPreference, menuSelection;
	CenterLockHorizontalScrollview centerLockHorizontalScrollview;
	//static int photoCount = 0;
	int photoCount = 0;
	File videoDirectory = null;
	File image = null;
	String multiImageRequirement = "";
	RequestParams params = new RequestParams();
	String imageFileName = "",encodedString ="",imagePath="",customerId="",evidenceIdentifier="";
	Bitmap bitmap;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.ems_upload_image);
        //Added these lines for bugsense, need to check -- June 17
        BugSenseHandler.setup(this, "6cdb6fca");
        Mint.initAndStartSession(this, "6cdb6fca");
        //
		context = this;
		multiImageRequirement = Utilities.loadPreferences(context,
				Utilities.TAG_IMAGES_REQUIREMENT);
		customerId = Utilities.loadPreferences(context,
				Utilities.TAG_CUSTOMER_ID);
		evidenceIdentifier = Utilities.loadPreferences(context,
				Utilities.TAG_EVIDENCE_IDENTIFIER);
		fotoButton = (ImageView) findViewById(R.id.imageView_foto);

		takeMore = (ImageView) findViewById(R.id.takeMore);
		cancel = (ImageView) findViewById(R.id.cancel);

		progressLayout = (LinearLayout) findViewById(R.id.progress_layout);
		imagePathPreference = getResources().getString(R.string.imagePath);
		preview = new Preview(this,
				(SurfaceView) findViewById(R.id.KutCameraFragment));
		FrameLayout frame = (FrameLayout) findViewById(R.id.preview);
		frame.addView(preview);
		preview.setKeepScreenOn(true);

		takeMore.setOnClickListener(this);
		cancel.setOnClickListener(this);
		cancel.setEnabled(false);

		menuSelection = Utilities.loadPreferences(context,
				Utilities.TAG_MENUSELECTION);
		videoDirectory = Utilities.getDir(customerId, evidenceIdentifier);

		fotoButton.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				Intent nextScreen = new Intent();
				try {
					Utilities.savePreferences("takemore", "", context);
					takeFocusedPicture();

				} catch (Exception e) {

				}

				// exitButton.setClickable(false);
				if (Utilities.loadPreferences(context,"takemore").equals(""))
					fotoButton.setClickable(false);
				else
					takeMore.setClickable(false);
				progressLayout.setVisibility(View.VISIBLE);
			}
		});
	}

	@Override
	protected void onResume() {
		super.onResume();
		// TODO Auto-generated method stub
		try {
			if (camera == null) {
				camera = Camera.open();
				camera.startPreview();
				camera.setErrorCallback(new ErrorCallback() {
					public void onError(int error, Camera mcamera) {
						camera.setPreviewCallback(null);
						camera.release();
						camera = Camera.open();
						Log.d("Camera died", "error camera");

					}
				});
			}
			if (camera != null) {
				// Here need to check that in genemotion
				if (Build.VERSION.SDK_INT >= 14)
					setCameraDisplayOrientation(context,
							CameraInfo.CAMERA_FACING_BACK, camera);
				preview.setCamera(camera);
			}
		} catch (Exception ex) {
			Utilities.alertDialogDisplay(context,
					"could not connect to camera",
					"could not connect to camera");
		}
	}

	private void setCameraDisplayOrientation(Activity activity, int cameraId,
			android.hardware.Camera camera) {
		android.hardware.Camera.CameraInfo info = new android.hardware.Camera.CameraInfo();
		android.hardware.Camera.getCameraInfo(cameraId, info);
		int rotation = activity.getWindowManager().getDefaultDisplay()
				.getRotation();
		int degrees = 0;
		switch (rotation) {
		case Surface.ROTATION_0:
			degrees = 0;
			break;
		case Surface.ROTATION_90:
			degrees = 90;
			break;
		case Surface.ROTATION_180:
			degrees = 180;
			break;
		case Surface.ROTATION_270:
			degrees = 270;
			break;
		}

		int result;
		if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
			result = (info.orientation + degrees) % 360;
			result = (360 - result) % 360; // compensate the mirror
		} else { // back-facing
			result = (info.orientation - degrees + 360) % 360;
		}
		camera.setDisplayOrientation(result);
	}

	Camera.AutoFocusCallback mAutoFocusCallback = new Camera.AutoFocusCallback() {

		public void onAutoFocus(boolean success, Camera camera) {

			try {
				camera.takePicture(mShutterCallback, null, jpegCallback);
			} catch (Exception e) {

			}

		}
	};

	Camera.ShutterCallback mShutterCallback = new ShutterCallback() {

		public void onShutter() {
			// TODO Auto-generated method stub
			Log.d("onShutter", "onShutter");

		}
	};

	public void takeFocusedPicture() {
		Log.d("takeFocusedPicture", "takeFocusedPicture");
		camera.autoFocus(mAutoFocusCallback);

	}

	PictureCallback rawCallback = new PictureCallback() {
		public void onPictureTaken(byte[] data, Camera camera) {
			Log.d("onPictureTaken - raw", "onPictureTaken - raw");
		}
	};

	PictureCallback jpegCallback = new PictureCallback() {
		@SuppressWarnings("deprecation")
		public void onPictureTaken(byte[] data, Camera camera) {

			FileOutputStream outStream = null;
			Calendar c = Calendar.getInstance();
			// File videoDirectory = new File(path);
		//	File videoDirectory = Utilities.getDir(customerId, evidenceIdentifier);

			if (!videoDirectory.exists()) {
				videoDirectory.mkdirs();
			}

			try {
				// Write to SD Card
				
				photoCount += 1;
				if (menuSelection.equalsIgnoreCase("2")) {
					//photoCount += 1;
					imageFileName = customerId + "_" + evidenceIdentifier + "_" + "pic_" + "unknowwn_issue"
							+ photoCount + ".jpg";
					
					image = new File(videoDirectory, imageFileName);
				}
				if (menuSelection.equalsIgnoreCase("1"))  {
					if  (Utilities.loadPreferences(context,"submenu").equalsIgnoreCase("toolbox")) {
					//photoCount += 1;
						imageFileName = customerId + "_" + evidenceIdentifier + "_" + "pic_100" + "_" + Utilities.loadPreferences(context, "newarticleno") + "_" + photoCount +  ".jpg";
						image = new File(videoDirectory, imageFileName);
						Utilities.savePreferences("imageUpload", "yes", context);
					}
					else {				
					//photoCount += 1;
						imageFileName = customerId + "_" + evidenceIdentifier + "_" + "pic_" + "telecom_asset_" + EmsBaseAdapter.positionCount + "_"
								+ photoCount + ".jpg";
					image = new File(videoDirectory, imageFileName);
					}
				}
				if (menuSelection.equalsIgnoreCase("4")) {
					//photoCount += 1;
					if (multiImageRequirement
							.equalsIgnoreCase("MultiplePerScreen")) {
						imageFileName = customerId + "_" + evidenceIdentifier + "_" + "pic_100" + "_" + Utilities.loadPreferences(context, "newarticleno") + "_" + photoCount +  ".jpg";
						image = new File(videoDirectory, imageFileName);
						Utilities.savePreferences("imageUpload", "yes", context);

					} else {
						String articleNo = Utilities.loadPreferences(context,
								Utilities.TAG_CATALOGUERECORDNO);
						image = new File(videoDirectory, "pic_" + articleNo
								+ ".jpg");
					}
				}
				Uri uriSavedImage = Uri.fromFile(image);
				Utilities.savePreferences(imagePathPreference,
						uriSavedImage.getPath(), getApplicationContext());
				outStream = new FileOutputStream(image);
				outStream.write(data);
				//outStream.close();
				//Resizing here before writing -- July 31
				// Here we Resize the Image ...
				BitmapFactory.Options options = null;
				options = new BitmapFactory.Options();
				options.inSampleSize = 3;
				
				
				bitmap = BitmapFactory.decodeFile(videoDirectory + "/" + imageFileName,
						options);   
		        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		        bitmap.compress(Bitmap.CompressFormat.JPEG, 50,
		                byteArrayOutputStream); // bm is the bitmap object
		        byte[] bsResized = byteArrayOutputStream.toByteArray();
				//
				//outStream.write(data);
		        outStream = new FileOutputStream(image);  // Again taking
		        outStream.write(bsResized);  //Writing resized data
				outStream.close();
				params.put("filename", imageFileName);
				imagePath = videoDirectory + "/" + imageFileName;
				
				String fromScreen = Utilities.loadPreferences(context,
						Utilities.TAG_CATALOGUETOCAMERA);
				Intent nextScreen = new Intent();
				if ((menuSelection.equalsIgnoreCase("2"))
						|| (menuSelection.equalsIgnoreCase("4"))
						|| (menuSelection.equalsIgnoreCase("1"))) {
					if (multiImageRequirement.equalsIgnoreCase("MultiplePerScreen")) {
						// But this process can be completed if we have cloud setup
						// and then only can show json from it. as of now alert is
						// there
						if (Utilities.loadPreferences(context, "takemore").equalsIgnoreCase("")) {
								if (menuSelection.equalsIgnoreCase("1") && Utilities.loadPreferences(context, "submenu").equalsIgnoreCase("btnMenu1")) {
									Utilities.navigateToListView(context);
								}
								if ((menuSelection.equalsIgnoreCase("2") && Utilities.loadPreferences(context, "submenu").equalsIgnoreCase("btnMenu1")) || Utilities.loadPreferences(context, "submenu").equalsIgnoreCase("btnMenu3")) {
									navigateBackToReasonsScreen(); // This way we can move back to Reason screen -- June 19
								}
							    if (menuSelection.equalsIgnoreCase("4") && 
							    	( Utilities.loadPreferences(context, "submenu").equalsIgnoreCase("toolbox") ||  
							    	  Utilities.loadPreferences(context, "submenu").equalsIgnoreCase("create toolbox") ||
							    	  Utilities.loadPreferences(context, "submenu").equalsIgnoreCase("create equipment") ||
							    	  Utilities.loadPreferences(context, "submenu").equalsIgnoreCase("equipmentbox") )) {
							    	if (fromScreen.equalsIgnoreCase("insert item")) {
										createAndShowAlertDialog();
									} 
							    	else {
							    	finish();
							    	}
							    }
							    if (menuSelection.equalsIgnoreCase("1") && Utilities.loadPreferences(context, "submenu").equalsIgnoreCase("toolbox")) {
							    	if (fromScreen.equalsIgnoreCase("insert item")) {
										createAndShowAlertDialog();
									} 
							    	else {
							    	finish();
							    	}
							    }						}
							else {
								//Utilities.alertDialogDisplay(context, "Picture Status", "Picture is taken. Preparing to take next picture");
								createAndShowAlertDialogForTakingMorePicture();
								/* Commented on Jan 18
								//  takeMore.setClickable(true);
								//cancel.setEnabled(true);
								//camera.startPreview();
								//progressLayout.setVisibility(View.GONE);*/
							}

						} else {
							finish();
						}
					}

				 else {
					nextScreen = new Intent(context, EmsServiceNotes.class);
					startActivity(nextScreen);
				}
			}
			
				//Commenting out image upload to server but instead it will done finally during submit
			//	encodeImagetoString();
				
				//Upload the image to server as well --June 1
				/*Map headerData = new HashMap();
				headerData.put("Content-type", "multipart/form-data");
				
				//MultipartRequest request = new MultipartRequest(uploadUrl, jsonFileName, Response.class, headerData, new Response.Listener(), null);
				MultipartRequest request = new MultipartRequest(context,Utilities.uploadImageToServerUrl, image, Response.class, headerData, new Response.Listener<String>() {

					public void onResponse(String response) {
						System.out.println("response.." + response);
						
					}
					
				}, new Response.ErrorListener(){


					public void onErrorResponse(VolleyError error) {
						System.out.println("errorResponse.." + error);
						if (error instanceof NetworkError
								|| error instanceof ClientError
								|| error instanceof ServerError
								|| error instanceof AuthFailureError
								|| error instanceof ParseError) {
							System.out.println("errorResponse123.." + error);
						}
						
					}
				}
			        );
				request.setRetryPolicy(new DefaultRetryPolicy(20 * 10000, 5,
						DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
				request.setTag("multipart upload");
				
				AppController.getInstance().addToRequestQueue(request,
						"multipart upload");*/

			 catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {

			}


		}
	};

	public static Bitmap rotate(Bitmap source, float angle) {
		Matrix matrix = new Matrix();
		matrix.postRotate(angle);
		return Bitmap.createBitmap(source, 0, 0, source.getWidth(),
				source.getHeight(), matrix, false);
	}

	public void onClick(View v) { 
		switch(v.getId()) 
		{ 
			case R.id.takeMore :
				cancel.setEnabled(true);
				try { 
					Utilities.savePreferences("takemore", "takemore", context);
					takeFocusedPicture();
					takeMore.setClickable(false);
					}	catch (Exception e) {
						e.printStackTrace();
					}
				break;
				
			case R.id.cancel :
				  
				  File file = null; 
				  //new File(videoDirectory + "pic_" + "unknowwn_issue" +
					//	  photoCount + ".jpg"); 
				  if (menuSelection.equalsIgnoreCase("2")) {				  
					  file = new File(videoDirectory + customerId + evidenceIdentifier + "pic_" + "unknowwn_issue" +
				  photoCount + ".jpg"); 
				  }
				  if (menuSelection.equalsIgnoreCase("1")) {				  
					  file = new File(videoDirectory, customerId + evidenceIdentifier + "pic_" + "telecom_asset"
								+ photoCount + ".jpg");
				  }
				  if (menuSelection.equalsIgnoreCase("4")) {				  
					  file = new File(videoDirectory, customerId + evidenceIdentifier + "pic_100" + "_" + Utilities.loadPreferences(context, "newarticleno") + "_" + photoCount +  ".jpg");
				  }
				  file.delete(); 
				  if(file.exists())
				  { 
					  try {
						  file.getCanonicalFile().delete(); 
						} catch (IOException e) {
							 e.printStackTrace(); 
					} if(file.exists()){
						getApplicationContext().deleteFile(file.getName()); } } 
				  Utilities.alertDialogDisplay(context, "Picture deleted", "Picture  " + file + " is deleted");
				  cancel.setEnabled(true);
				 break; 
				
	  
	  }
	  
	    }
	  
	  

	private void createAndShowAlertDialog() {
		final AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Add More Items");
		builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.dismiss();
				Intent nextScreen = new Intent();
				nextScreen = new Intent(context,
						EmsCatalogueInsertItemActivity.class);
				startActivity(nextScreen);
			}
		});
		builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				Intent nextScreen = new Intent();
				String categoryChosen = Utilities.loadPreferences(context,
						Utilities.TAG_CHOSEN_CATEGORIES);
				nextScreen = new Intent(context, EmsSummaryActivity.class);
				startActivity(nextScreen);
			}
		});
		AlertDialog dialog = builder.create();
		dialog.show();
	}
	
	private void createAndShowAlertDialogForTakingMorePicture() {
		final AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Take More Pictures");
		builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.dismiss();
				takeMore.setClickable(true);
				cancel.setEnabled(true);
				camera.startPreview();
				progressLayout.setVisibility(View.GONE);
			}
		});
		builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				if (menuSelection.equalsIgnoreCase("1") && Utilities.loadPreferences(context, "submenu").equalsIgnoreCase("btnMenu1")) {
					Utilities.navigateToListView(context);
				}
				else if (menuSelection.equalsIgnoreCase("2")) {
					navigateBackToReasonsScreen();
				}
				else if (menuSelection.equalsIgnoreCase("4")) {
					if (Utilities.loadPreferences(context,
						Utilities.TAG_CATALOGUETOCAMERA).equalsIgnoreCase("insert item")) {
					createAndShowAlertDialog();
				}
				else {
					finish();
				}
			}
			}
		});
		AlertDialog dialog = builder.create();
		dialog.show();
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    if (keyCode == KeyEvent.KEYCODE_BACK) {
	    	Utilities.createAndShowAlertDialog(this);
	        return true;
	    }

	    return super.onKeyDown(keyCode, event);
	}
	
	private void navigateBackToReasonsScreen() {
		Utilities.savePreferences(Utilities.TAG_FLOW, "Reasons",
				context);
		Utilities.savePreferences("from camera back", "from camera back",
				context);
		Intent nextFlow = new Intent(context, EmsGenericFormMgmtActivity.class);
		startActivity(nextFlow);
	}
	

		
	

}
