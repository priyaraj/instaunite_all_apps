/**
 * Author: Priyaraj T.T.
 * * Copyright (c) 2000,2001,2002,2006-2008,2009  Ponnivi Internet Systems Pvt. Ltd.,
* Please see the License.txt file for more information.*
* All Rights Reserved.

* 
* <summary> EmsNfcReaderActivity -- The activity lists the records of any detected NDEF message and displays some toast messages for various events.

 * */

package com.Ponnivi.ems.activity;

import org.ndeftools.Message;
import org.ndeftools.MimeRecord;
import org.ndeftools.Record;
import org.ndeftools.externaltype.ExternalTypeRecord;
import org.ndeftools.util.activity.NfcReaderActivity;
import org.ndeftools.wellknown.TextRecord;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.Ponnivi.ems.R;
import com.Ponnivi.ems.adapter.NdefRecordAdapter;
import com.Ponnivi.ems.helper.Utilities;

public class EmsNfcReaderActivity extends NfcReaderActivity implements OnClickListener {

	private static final String TAG = EmsNfcReaderActivity.class.getName();
	
	protected Message message;
	
	TextView scancard;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.nfcreader);
		scancard = (TextView)findViewById(R.id.scancard);
		if (Utilities.loadPreferences(this, Utilities.TAG_IDENTIFIED_REMOTE_UNIT).equalsIgnoreCase("")) {
			scancard.setText("Scan Machine");
		}
		else {
			scancard.setText("Scan Supervisor");
		}
		// lets start detecting NDEF message using foreground mode
		setDetecting(true);
		
		findViewById(R.id.btnSubmit).setOnClickListener(this);
        findViewById(R.id.prev).setOnClickListener(this);
	}
	
	public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnSubmit:
            	if (Utilities.loadPreferences(this, Utilities.TAG_IDENTIFIED_SUPERVISER_UNIT).equalsIgnoreCase("")) {
                	Intent i = new Intent(this, EmsNfcReaderActivity.class);
                	startActivity(i);
            	}
            	else {
            	Utilities.savePreferences(Utilities.TAG_FLOW, "Reasons", this);
            	Intent i = new Intent(this, EmsGenericFormMgmtActivity.class);
            	startActivity(i);
            	}
                break;
            case R.id.prev:
            	Utilities.setBackKeyValues(this);
                break;
        }
    }
	
	/**
	 * An NDEF message was read and parsed. This method prints its contents to log and then shows its contents in the GUI.
	 * 
	 * @param message the message
	 */
	
	@Override
	public void readNdefMessage(Message message) {
/*		if(message.size() > 1) {
	        toast(getString(R.string.readMultipleRecordNDEFMessage));
		} else {
	        toast(getString(R.string.readSingleRecordNDEFMessage));
		}	*/	
		
		this.message = message;
		
		// process message
		
		// show in log
		// iterate through all records in message
		Log.d(TAG, "Found " + message.size() + " NDEF records");

		for(int k = 0; k < message.size(); k++) {
			Record record = message.get(k);
			
			Log.d(TAG, "Record " + k + " type " + record.getClass().getSimpleName());
			
			// your own code here, for example:
			if(record instanceof MimeRecord) {
				// ..
			} else if(record instanceof ExternalTypeRecord) {
				// ..
			} else if(record instanceof TextRecord) {
				// ..
			} else { // more else
				// ..
			}
		}
		
		// show in gui
		showList();
	}
	

	/**
	 * An empty NDEF message was read.
	 * 
	 */
	
	@Override
	protected void readEmptyNdefMessage() {
		 toast(getString(R.string.readEmptyMessage));
		 
		 clearList();
	}

	/**
	 * 
	 * Something was read via NFC, but it was not an NDEF message. 
	 * 
	 * Handling this situation is out of scope of this project.
	 * 
	 */
	
	@Override
	protected void readNonNdefMessage() {
		toast(getString(R.string.readNonNDEFMessage));
		
		hideList();
	}

   /**
     * 
     * NFC feature was found and is currently enabled
     * 
     */
	
	@Override
	protected void onNfcStateEnabled() {
		toast(getString(R.string.nfcAvailableEnabled));
	}

    /**
     * 
     * NFC feature was found but is currently disabled
     * 
     */
	
	@Override
	protected void onNfcStateDisabled() {
		toast(getString(R.string.nfcAvailableDisabled));
	}

	/**
     * 
     * NFC setting changed since last check. For example, the user enabled NFC in the wireless settings.
     * 
     */
	
	@Override
	protected void onNfcStateChange(boolean enabled) {
		if(enabled) {
			toast(getString(R.string.nfcAvailableEnabled));
		} else {
			toast(getString(R.string.nfcAvailableDisabled));
		}
	}

	/**
	 * 
	 * This device does not have NFC hardware
	 * 
	 */
	
	@Override
	protected void onNfcFeatureNotFound() {
	//	toast(getString(R.string.noNfcMessage));
		Intent nextScreen = new Intent(this,
				EmsIdentificationFormMgmtActivity.class);
		Utilities.savePreferences("nfc_feature", "false", this);
		startActivity(nextScreen);
	}

	/**
	 * 
	 * Show NDEF records in the list
	 * 
	 */
	
	private void showList() {
		// display the message
		// show in gui
		ArrayAdapter<? extends Object> adapter = new NdefRecordAdapter(this, message);
		ListView listView = (ListView) findViewById(R.id.recordListView);
		listView.setAdapter(adapter);
		listView.setVisibility(View.VISIBLE);
		//Utilities.alertDialogDisplay(this, "identified units", message.toString());
	}
	
	/**
	 * 
	 * Hide the NDEF records list.
	 * 
	 */
	
	public void hideList() {
		ListView listView = (ListView) findViewById(R.id.recordListView);
		listView.setVisibility(View.GONE);
	}
	
	/**
	 * 
	 * Clear NDEF records from list
	 * 
	 */
	
	private void clearList() {
		ListView listView = (ListView) findViewById(R.id.recordListView);
		listView.setAdapter(null);
		listView.setVisibility(View.VISIBLE);
	}

	public void toast(String message) {
		Toast toast = Toast.makeText(this, message, Toast.LENGTH_LONG);
		toast.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL, 0, 0);
		toast.show();
	}

	@Override
	protected void onTagLost() {
		toast(getString(R.string.tagLost));
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	     if (keyCode == KeyEvent.KEYCODE_BACK) { 
		 	Utilities.setBackKeyValues(this);
	     }

	     return super.onKeyDown(keyCode, event);
	 }
	     
}
