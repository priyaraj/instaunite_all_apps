/**
 * Author: Priyaraj T.T.
 * * Copyright (c) 2000,2001,2002,2006-2008,2009  Ponnivi Internet Systems Pvt. Ltd.,
* Please see the License.txt file for more information.*
* All Rights Reserved.

* 
* <summary> EmsGroupsActivity -- Required for questionaire type of forms. Currently for 3rd

 * */
package com.Ponnivi.ems.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Window;

import com.Ponnivi.ems.R;
import com.Ponnivi.ems.helper.Utilities;
import com.bugsense.trace.BugSenseHandler;
import com.splunk.mint.Mint;

@SuppressLint("NewApi")
public class EmsGroupsActivity extends Activity  {

    private Fragment questionerheaderfragment;
    private Fragment questionergroups;
    String userName, password, menuSelection;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.groups);
        //Added these lines for bugsense, need to check -- June 17
        /*BugSenseHandler.setup(this, "6cdb6fca");
        Mint.initAndStartSession(this, "6cdb6fca");*/
        //
		userName = Utilities.loadPreferences(this, getResources()
				.getString(R.string.email));
		password = Utilities.loadPreferences(this,
				Utilities.TAG_PASSWORD);
		menuSelection = Utilities.loadPreferences(this,
				Utilities.TAG_MENUSELECTION);
		Utilities.removePreference(
				getResources().getString(R.string.toolbaxarray), getApplicationContext());
		Utilities.removePreference(Utilities.TAG_CATALOGUERECORDNO, getApplicationContext());
//
        questionerheaderfragment = getFragmentManager().findFragmentById(R.id.questionerheaderfragment);
        questionergroups = getFragmentManager().findFragmentById(R.id.questionergroups);
    }
    
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    if (keyCode == KeyEvent.KEYCODE_BACK) {
	    	Utilities.resetCatalogueCountValues(this);
	    	//finish();
	    	Utilities.setBackKeyValues(this);
	    }
	    return super.onKeyDown(keyCode, event);
	}
}
			
/*			if (userName.equalsIgnoreCase("TT1@g.com")
					&& password.equalsIgnoreCase("Aaa555")
					|| menuSelection.equalsIgnoreCase("1")) {
				Utilities.setBackKeyValues(this);
				return true;
				
			}
			if (userName.equalsIgnoreCase("MC1@g.com")
					&& password.equalsIgnoreCase("bbb444")
					|| menuSelection.equalsIgnoreCase("2")) {
				Utilities.setBackKeyValues(this);
				return true;
				}
			}
		if ((userName.equalsIgnoreCase("AC1@g.com") && password
				.equalsIgnoreCase("ddd222"))
				|| menuSelection.equalsIgnoreCase("4")) {
			Utilities.setBackKeyValues(this);
			return true;
		}
		if ((userName.equalsIgnoreCase("FS1@g.com") && password
				.equalsIgnoreCase("Ccc333"))
				|| menuSelection.equalsIgnoreCase("3")) {
			Utilities.setBackKeyValues(this);
			return true;
			
		}*/
			
/*	    	Intent nextFlow = new Intent(this, EmsMenuActivity.class);
			startActivity(nextFlow);
	        return true;
*/	    

	    
	
	


