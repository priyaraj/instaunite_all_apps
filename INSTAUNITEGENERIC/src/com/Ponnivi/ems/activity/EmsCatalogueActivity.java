/**
 * Author: Priyaraj T.T.
 * * Copyright (c) 2000,2001,2002,2006-2008,2009  Ponnivi Internet Systems Pvt. Ltd.,
* Please see the License.txt file for more information.*
* All Rights Reserved.

* 
* <summary> EmsBarcodeReader -- Required for barcode

 * */
package com.Ponnivi.ems.activity;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.Window;
import android.widget.TextView;

import com.Ponnivi.ems.R;
import com.Ponnivi.ems.adapter.EmsCatalogueListAdapter;
import com.Ponnivi.ems.helper.Utilities;
import com.Ponnivi.ems.widget.CenterLockHorizontalScrollview;

@SuppressLint("NewApi")
public class EmsCatalogueActivity extends Activity {
	CenterLockHorizontalScrollview centerLockHorizontalScrollview;
	int pageNewsCount;
	Fragment catalogueheaderfragment;
	Context context = this;
	JSONArray catalogueDetailsJson;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.ems_catalogue_mgmt);
		catalogueheaderfragment = getFragmentManager().findFragmentById(R.id.catalogueheaderfragment);
		String toolBoxName = Utilities.loadPreferences(context,getResources().getString(R.string.toolbox));
		String relevantJsonFileName = "";
		String wholeJson = Utilities.loadPreferences(context, Utilities.TAG_WHOLE_JSON);

		if (toolBoxName.equalsIgnoreCase(getResources().getString(R.string.Power_Tools))) {
			relevantJsonFileName = "powertools.json";
		}
		if (toolBoxName.equalsIgnoreCase(getResources().getString(R.string.Tool_Sets))) {
			relevantJsonFileName = "multipleitems.json";
		}
		if (toolBoxName.equalsIgnoreCase(getResources().getString(R.string.Screwdrivers_and_Spanners))) {
			relevantJsonFileName = "spanner.json";
		}
		if (toolBoxName.equalsIgnoreCase(getResources().getString(R.string.Miscellenous))) {
			relevantJsonFileName = "othertools.json";
		}
		//Get the total number of records in all the four jsons
		
		ArrayList<JSONObject> catalogueList = new ArrayList<JSONObject>();
		JSONObject obj;
		JSONArray m_jArry;
		try {
			obj = new JSONObject(Utilities.loadFromAsset(this,
					relevantJsonFileName));
			if (obj != null) {
			m_jArry = obj.getJSONArray(Utilities.TAG_CATALOGUE);
			for (int i = 0; i < m_jArry.length(); i++) {
				catalogueList.add(m_jArry.getJSONObject(i));
			}
			Utilities.savePreferences(Utilities.TAG_CATALOGUESPECIFICTOTALRECORDS, String.valueOf(m_jArry.length()), context);
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			Utilities.alertDialogDisplay(context, "jsonexception", e.getMessage());
		}
		centerLockHorizontalScrollview = (CenterLockHorizontalScrollview) findViewById(R.id.scrollView);
		EmsCatalogueListAdapter catalogueItemAdapter = new EmsCatalogueListAdapter(
				this, R.layout.ems_catalogue_items, catalogueList,centerLockHorizontalScrollview,catalogueheaderfragment);
		String nextRecord = Utilities.loadPreferences(context, Utilities.TAG_CATALOGUERECORDNO);
		if (nextRecord != null && !nextRecord.equalsIgnoreCase("")) {
			pageNewsCount = Integer.parseInt(nextRecord);
		}
		else {
			pageNewsCount = centerLockHorizontalScrollview.getCurrentPageIndex();
		}
		centerLockHorizontalScrollview.setCatalogueAdapter(catalogueItemAdapter,
				pageNewsCount, "");

	}

}