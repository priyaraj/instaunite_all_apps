/**
 * Author: Priyaraj T.T.
 * * Copyright (c) 2000,2001,2002,2006-2008,2009  Ponnivi Internet Systems Pvt. Ltd.,
* Please see the License.txt file for more information.*
* All Rights Reserved.

* 
* <summary> LoginActivity -- Take username and password as credentials, validates and redirects the screen

 * */
package com.Ponnivi.ems.activity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.Ponnivi.ems.R;
import com.Ponnivi.ems.helper.Utilities;
import com.bugsense.trace.BugSenseHandler;
import com.splunk.mint.Mint;

@SuppressLint("NewApi")
public class LoginActivity extends Activity implements View.OnClickListener {
	private static final String TAG = LoginActivity.class.getSimpleName();

	EditText userName;
	EditText password;
	Button imgBtnLogin,imgBtnCancel;
	String userEmail = null;
	String emailIdPrefernence = null;
	String userCategoryPrefernence = null;
	String userPassword = null;	
	Context context = this;
	InputMethodManager imm;
	String dbName = null; //To get the dbName from userdetails.json, if not logged in but coming via top menu button, ponnivi_test will be dbname -- June 23

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		this.requestWindowFeature(Window.FEATURE_NO_TITLE);

		setContentView(R.layout.login);
        //Added these lines for bugsense, need to check -- June 17
        BugSenseHandler.setup(this, "6cdb6fca");
        Mint.initAndStartSession(this, "6cdb6fca");
        //
		emailIdPrefernence = getResources().getString(R.string.email);
		userCategoryPrefernence = getResources().getString(
				R.string.userCategory);
		// slide_me = new SimpleSideDrawer(this);
		// btnSlide = (Button) findViewById(R.id.BtnSlide);
		userName = (EditText) findViewById(R.id.userName);
		password = (EditText) findViewById(R.id.password);
		imgBtnLogin = (Button) findViewById(R.id.imgBtnLogin);
		imgBtnCancel = (Button) findViewById(R.id.imgBtnCancel);
		Utilities.removePreference(Utilities.TAG_CHOSEN_CATEGORIES, context);
		Utilities.removePreference(Utilities.TAG_WHOLE_JSON, context);
		Utilities.removePreference(Utilities.TAG_WHOLE_JSONArray, context);
		Utilities.removePreference(
				getResources().getString(R.string.toolbaxarray), context);
		imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		Utilities.requestFocus(userName, imm);

		Configuration config = getResources().getConfiguration();
		int screenSize = config.screenLayout & config.SCREENLAYOUT_SIZE_MASK;
		int screenWidth = this.getWindowManager().getDefaultDisplay()
				.getWidth();
		int screenHeight = this.getWindowManager().getDefaultDisplay()
				.getHeight();
		

		password.setOnKeyListener(new OnKeyListener() {
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				// If the event is a key-down event on the "enter" button
				if ((event.getAction() == KeyEvent.ACTION_DOWN)
						&& (keyCode == KeyEvent.KEYCODE_ENTER)) {
					validateCredentials();
					return true;
				}
				return false;
			}

		});

		imgBtnLogin.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				validateCredentials();
				// Intent i = new
				// Intent(context,EmsCatalogueToolBoxActivity.class);
				// context.startActivity(i);

			}
		});
		imgBtnCancel.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				 Intent i = new Intent(context,LogoutActivity.class);
				 context.startActivity(i);
				 finish();
			}
		});
		


	}

	private void validateCredentials() {

		userEmail = userName.getText().toString();
		userPassword = password.getText().toString();

		boolean isEmailValueExists = false;
		isEmailValueExists = Utilities.mandatoryChecks("User Name", userEmail,
				context, "Missing userName", "Missing userName");
		// Validating email format front end itself
		if (isEmailValueExists) {
			isEmailValueExists = Utilities.isEmailValid(userEmail, context,
					"Invalid email id format", "Invalid email id format");

			if (isEmailValueExists) {
				isEmailValueExists = Utilities.mandatoryChecks("Password",
						userPassword, context, "Missing password",
						"Missing password");
				if (isEmailValueExists) {

					JSONObject obj;
					JSONArray m_jArry;
					boolean userExists = false;
					String userCategory = null;
					//We are validating username and password from config json irrespective of details in userdetails.json -- June 24
					if (userEmail.equalsIgnoreCase(Utilities.loadPreferences(context, Utilities.TAG_USERNAME))
							&& userPassword.equalsIgnoreCase(Utilities.loadPreferences(context, Utilities.TAG_PASSWORD))) {
						userExists = true;
					}
					/*obj = new JSONObject(Utilities.loadFromAsset(context,
							"userdetails.json"));
					m_jArry = obj.getJSONArray(Utilities.TAG_Login);

					for (int i = 0; i < m_jArry.length(); i++) {

						JSONObject jo_inside = m_jArry.getJSONObject(i);
						String userName = jo_inside
								.getString(Utilities.TAG_USERNAME);
						String password = jo_inside
								.getString(Utilities.TAG_PASSWORD);
						userCategory = jo_inside
								.getString(Utilities.TAG_UserCategory);
						dbName = jo_inside
								.getString(Utilities.TAG_DBNAME);
						if (userEmail.equalsIgnoreCase(userName)
								&& userPassword.equalsIgnoreCase(password)) {
							userExists = true;
							break;
						}

					}*/
					if (userExists) {
						// Utilities.alertDialogDisplay(this,
						// "Valid credentials",
						//All details are captured only from config_app but not from userdetails -- June 24
/*							Utilities.savePreferences(emailIdPrefernence,
								userEmail, getApplicationContext());
						Utilities.savePreferences(Utilities.TAG_PASSWORD,
								userPassword, getApplicationContext());
						Utilities.savePreferences(userCategoryPrefernence,
								userCategory, getApplicationContext());
						Utilities.savePreferences("dbName",
								dbName, getApplicationContext());
*/							
						
						//Depends upon menuSelection redirecting the users instead with login details. All users available in userdetails.json are valid users
						String menuSelection = Utilities.loadPreferences(context, Utilities.TAG_MENUSELECTION);
						Utilities.redirectMenuFlows(menuSelection, this);
						
						/*if (userEmail.equalsIgnoreCase("AC1@g.com")
								&& userPassword.equalsIgnoreCase("ddd222")) {
							i = new Intent(this,
									EmsGroupsActivity.class);
						}

						if (userEmail.equalsIgnoreCase("FS1@g.com")
								&& userPassword.equalsIgnoreCase("Ccc333")) {
							
							i = new Intent(this, EmsGroupsActivity.class);
						}
						if (userEmail.equalsIgnoreCase("MC1@g.com")
								&& userPassword.equalsIgnoreCase("bbb444")) {
							Utilities.savePreferences("submenu", "btnMenu1",
									this);
							i = new Intent(this, EmsIdentificationFormMgmtActivity.class);
						}
						if (userEmail.equalsIgnoreCase("SR1@g.com")
								&& userPassword.equalsIgnoreCase("eee111")) {
							Utilities.savePreferences("submenu", "btnMenu1",
									this);
							i = new Intent(this, EmsSiteVisitItemsActivity.class);
						}*/
						//Intent i = new Intent();
						//startActivity(i);
					} else {
						Utilities
								.alertDialogDisplay(this, "Invalid User",
										"Wrong credentials. Please login with valid credentials");
						userName.setText("");
						password.setText("");
						Utilities.requestFocus(userName, imm);
					}

				} // if (isEmailValueExists

			}
		}

	} // validatecredentials

	public void onClick(View arg0) {
		// TODO Auto-generated method stub

	}

}
