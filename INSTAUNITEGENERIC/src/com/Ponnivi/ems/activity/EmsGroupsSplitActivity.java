/**
 * Author: Priyaraj T.T.
 * * Copyright (c) 2000,2001,2002,2006-2008,2009  Ponnivi Internet Systems Pvt. Ltd.,
* Please see the License.txt file for more information.*
* All Rights Reserved.

* 
* <summary> EmsBarcodeReader -- Required for barcode

 * */
package com.Ponnivi.ems.activity;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Window;
import android.widget.TextView;

import com.Ponnivi.ems.R;
import com.Ponnivi.ems.adapter.EmsCatalogueListAdapter;
import com.Ponnivi.ems.adapter.EmsCatalogueMultiImageListAdapter;
import com.Ponnivi.ems.adapter.EmsQuestionaireListAdapter;
import com.Ponnivi.ems.helper.Utilities;
import com.Ponnivi.ems.widget.CenterLockHorizontalScrollview;
import com.bugsense.trace.BugSenseHandler;
import com.splunk.mint.Mint;

@SuppressLint("NewApi")
public class EmsGroupsSplitActivity extends Activity {
	CenterLockHorizontalScrollview centerLockHorizontalScrollview;
	int pageNewsCount;
	Fragment groupheaderfragment;
	Context context = this;
	JSONArray catalogueDetailsJson;
	String userName, password, jsonArraySplitter,menuSelection,multiImageRequirement,subMenuOption;
	EmsCatalogueListAdapter catalogueItemAdapter = null;
	EmsCatalogueMultiImageListAdapter catalogueMultiImageItemAdapter = null;
	EmsQuestionaireListAdapter questionerItemAdapter = null;


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.ems_group_mgmt);
        //Added these lines for bugsense, need to check -- June 17
        BugSenseHandler.setup(this, "6cdb6fca");
        Mint.initAndStartSession(this, "6cdb6fca");
        //
		groupheaderfragment = getFragmentManager().findFragmentById(
				R.id.groupheaderfragment);

		String relevantJsonFileName = "";
		String wholeJson = Utilities.loadPreferences(context,
				Utilities.TAG_WHOLE_JSON);
		userName = Utilities.loadPreferences(this,
				getResources().getString(R.string.email));
		password = Utilities.loadPreferences(this, Utilities.TAG_PASSWORD);
		menuSelection = Utilities.loadPreferences(this, Utilities.TAG_MENUSELECTION);
		subMenuOption = Utilities.loadPreferences(this, "submenu");
		if (menuSelection.equalsIgnoreCase("4") || menuSelection.equalsIgnoreCase("1")) {
			//From telecom assets submenu also we are coming to this screen -- Apr 23
			if (subMenuOption.equalsIgnoreCase("create toolbox")) {
				relevantJsonFileName = "inventory.json";
			}

			else {
				relevantJsonFileName = "inventory_equipment.json";
			}
			((TextView) groupheaderfragment.getView().findViewById(
					R.id.screenName)).setText("INVENTORY");
		}
		if (menuSelection.equalsIgnoreCase("3")) {
		String groupName = Utilities.loadPreferences(context, getResources()
				.getString(R.string.toolbox));

		if (groupName.equalsIgnoreCase(getResources().getString(
				R.string.Power_Tools))) {
			relevantJsonFileName = "powertools.json";
		}
		if (groupName.equalsIgnoreCase(getResources().getString(
				R.string.Tool_Sets))) {
			relevantJsonFileName = "multipleitems.json";
		}
		if (groupName.equalsIgnoreCase(getResources().getString(
				R.string.Screwdrivers_and_Spanners))) {
			relevantJsonFileName = "spanner.json";
		}
		if (groupName.equalsIgnoreCase(getResources().getString(
				R.string.Miscellenous))) {
			relevantJsonFileName = "othertools.json";
		}
		if (groupName.equalsIgnoreCase("Complaints")) {
			relevantJsonFileName = "complaints.json";
		}
		if (groupName.equalsIgnoreCase("Occupancy")) {
			relevantJsonFileName = "occupancy_utilisation.json";
		}
		if (groupName.equalsIgnoreCase("Others")) {
			relevantJsonFileName = "others.json";
		}
		if (groupName.equalsIgnoreCase("Safeguarding")) {
			relevantJsonFileName = "safeguarding.json";
		}
		if (groupName.equalsIgnoreCase("Staffing")) {
			relevantJsonFileName = "staffing.json";
		}
		((TextView) groupheaderfragment.getView().findViewById(
				R.id.screenName)).setText(groupName);
		}

		// Get the total number of records in all the four jsons
		if (userName.equalsIgnoreCase("FS1@g.com")
				&& password.equalsIgnoreCase("Ccc333") || menuSelection.equalsIgnoreCase("3")) {
			jsonArraySplitter = Utilities.TAG_QUESTIONBANK;
		}
		
		/* As we are taking from mongodb collection, this process is changed -- July 3
		 * if ( ( userName.equalsIgnoreCase("AC1@g.com") && password.equalsIgnoreCase("ddd222") || menuSelection.equalsIgnoreCase("4"))  ||
			  (userName.equalsIgnoreCase("TT1@g.com") && password.equalsIgnoreCase("Aaa555") || menuSelection.equalsIgnoreCase("1")) )	
		{
			jsonArraySplitter = Utilities.TAG_CATALOGUE;
		}*/
		
	

		ArrayList<JSONObject> groupList = new ArrayList<JSONObject>();
		if (menuSelection.equalsIgnoreCase("4") || menuSelection.equalsIgnoreCase("1")) {
			if (subMenuOption.equalsIgnoreCase("toolbox") || subMenuOption.equalsIgnoreCase("equipmentbox")){
				relevantJsonFileName = Utilities.loadPreferences(this, "volleyresponse");
				try {
					JSONArray checkItemsJsonArray = new JSONArray(relevantJsonFileName);
					for (int i = 0; i < checkItemsJsonArray.length(); i++) {
						JSONObject itemObject = checkItemsJsonArray.getJSONObject(i);
						System.out.println(itemObject.getString("B") + "... " + itemObject.getString("Evidence_1"));
						groupList.add(itemObject);
					}
					Utilities.savePreferences(
							Utilities.TAG_CATALOGUESPECIFICTOTALRECORDS,
							String.valueOf(checkItemsJsonArray.length()), context);
					Utilities.savePreferences(
							Utilities.TAG_CATALOGUETOTALRECORDS,
							String.valueOf(checkItemsJsonArray.length()), context);
					Utilities.savePreferences(
							Utilities.TAG_CATALOGUESPECIFICRECORDS,
							checkItemsJsonArray.toString(), context);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			else {
				jsonArraySplitter = Utilities.TAG_CATALOGUE;
				groupList = Utilities.loadJsonValues(this,jsonArraySplitter,relevantJsonFileName);
			}
		}
		else {
			
		groupList = Utilities.loadJsonValues(this,jsonArraySplitter,relevantJsonFileName);
		}

		centerLockHorizontalScrollview = (CenterLockHorizontalScrollview) findViewById(R.id.scrollView);
		if (userName.equalsIgnoreCase("FS1@g.com")
				&& password.equalsIgnoreCase("Ccc333") || menuSelection.equalsIgnoreCase("3")) {
			questionerItemAdapter = new EmsQuestionaireListAdapter(
					this, R.layout.ems_questionaire_items, groupList,
					centerLockHorizontalScrollview, groupheaderfragment);
		}
		else {
			multiImageRequirement = Utilities.loadPreferences(context, Utilities.TAG_IMAGES_REQUIREMENT);
			if (multiImageRequirement.equalsIgnoreCase("MultiplePerScreen")) {
				//Need to see whether is this check inventory or create toolbox
				Intent nextFlow;
				if (subMenuOption.equalsIgnoreCase("create toolbox") || subMenuOption.equalsIgnoreCase("create equipment")) {
					nextFlow = new Intent(context,
							EmsCatalogueMultimageItemsActivityCreateToolBox.class);
				}
				else {
				nextFlow = new Intent(context,
						EmsCatalogueMultimageItemsActivity.class);
				}
				context.startActivity(nextFlow);
				
				
			}
			else {
		catalogueItemAdapter = new EmsCatalogueListAdapter(
				this, R.layout.ems_catalogue_items, groupList,
				centerLockHorizontalScrollview, groupheaderfragment);
			}
		}
		String nextRecord = Utilities.loadPreferences(context,
				Utilities.TAG_CATALOGUERECORDNO);
		if (nextRecord != null && !nextRecord.equalsIgnoreCase("")) {
			pageNewsCount = Integer.parseInt(nextRecord);
		} else {
			pageNewsCount = centerLockHorizontalScrollview
					.getCurrentPageIndex();
		}
		if (userName.equalsIgnoreCase("FS1@g.com")
				&& password.equalsIgnoreCase("Ccc333") || menuSelection.equalsIgnoreCase("3")) {
		centerLockHorizontalScrollview.setQuestionAdapter(questionerItemAdapter,
				pageNewsCount, "");
		}
		else {
			if (multiImageRequirement.equalsIgnoreCase("MultiplePerScreen")) {
				//pageNewsCount += 3;
			//	centerLockHorizontalScrollview.setCatalogueMultiImageAdapter(catalogueMultiImageItemAdapter, pageNewsCount, "");
			}
			centerLockHorizontalScrollview.setCatalogueAdapter(catalogueItemAdapter, pageNewsCount, "");
		}

	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    if (keyCode == KeyEvent.KEYCODE_BACK) {
	    	Utilities.resetCatalogueCountValues(this);
	    	Intent nextFlow = new Intent(this, EmsMenuActivity.class);
			startActivity(nextFlow);
	        return true;
	    }

	    return super.onKeyDown(keyCode, event);
	}

}